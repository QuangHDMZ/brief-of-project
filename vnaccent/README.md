# Woka vietnamese ecomerce accent

Woka vietnamese ecomerce accent (Data train cho TMDT)
1. https://drive.google.com/file/d/1ypvEoGRNWrNLmW246RtBm9iMyKXm_2BP/view
2. https://dumps.wikimedia.org/viwiki/latest/viwiki-latest-pages-articles.xml.bz2
3. https://github.com/VNOpenAI/vn-accent

# Data 
`Toàn bộ` 192.168.10.178 /data_science/data_science/data/v1/accent_vi

`Train` 192.168.10.178 /data_science/data_science/data/v1/accent_vi/train
# Train

git clone https://github.com/VNOpenAI/vn-accent

bash train_transformer_evolved.sh


file vocab tokenizer.h5 là được lấy từ git https://github.com/vudaoanhtuan/vietnamese-tone-prediction 

Tách final.tone và final.notone thành train và valid tùy vào tỉ lệ mọi người muốn hiện tại của mình là 80 train và 20 val 

bash train_transformer_evolved.sh /data_science/data_science/data/v1/final_models/vn_accent/tokenizer.h5 train_path /data_science/data_science/data/v1/accent_vi/train/ val_path /data_science/data_science/data/v1/accent_vi/val/ restore_file /data_science/data_science/data/v1/final_models/vn_accent/epoch_03.h5 cuda True


vocab_path là đường dẫn tới file chứa voc đã build sẵn

train_path là đường dẫn tới folder train trong đó phải có 1 file là train.tone và 1 file là train.notone

val_path là đường dẫn tới folder val trong đó phải có 1 file là val.tone và 1 file là val.notone

restore_file là đường dẫn đến file weight cần train tiếp 

cuda True là dùng GPU còn không có GPU thì ko cần gọi cuda

# Model 
`Đã train` 192.168.10.178 /data_science/data_science/data/v1/final_models/vn_accent
# Test
src/wkvnaccent/test.py
# Translate
src/wkvnaccent/translate.py

#Loc Note

model này trước là được train trên tập final.tone có 1tr row thì được acc là 88% trên real input

xong đó thì được train tiếp với data real input kw.sql 

data trong kw.sql phải được preprocess bỏ các outline chỉ lấy tiếng việt và tích hợp các preprocess cũ

sau khi train thì được model final là 94% acc trên real input

