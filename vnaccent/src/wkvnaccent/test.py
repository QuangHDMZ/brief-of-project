import os
import json
import argparse
import time
import torch

from models.model_factory import get_model
from utils import translate, evaluate_model


vocab_path = "/content/drive/MyDrive/vn-accent/data/tokenizer.h5"
weight_file = "/content/drive/MyDrive/vn-accent/experiments/transformer_evolved/weights/epoch_02.h5"
model_name = "transformer_evolved"
batch_size = 128
cuda = "False"
config_file = "model_config.json"


print("Load tokenizer")

tokenizer = torch.load(vocab_path)
print(tokenizer)
src_tokenizer = tokenizer['notone']
trg_tokenizer = tokenizer['tone']
src_pad_token = 0
trg_pad_token = 0
print("Init model")
with open(config_file) as f:
    config = json.load(f)

if model_name in config:
    model_param = config[model_name]
else:
    raise Exception("Invalid model name")

model_param['src_vocab_size'] = len(src_tokenizer.word_index) + 1
model_param['trg_vocab_size'] = len(trg_tokenizer.word_index) + 1

model = get_model(model_param)
device = torch.device('cuda' if torch.cuda.is_available() and cuda else 'cpu')
#device = torch.device('cpu')
print("Using", device.type)
if device.type == 'cuda':
    model = model.cuda()

if os.path.isfile(weight_file):
    print("Load model")
    state = torch.load(weight_file, map_location='cpu')
    if isinstance(state, dict):
        model.load_state_dict(state['model'])
    else:
        model.load_state_dict(state)
else:
    raise Exception("Invalid weight path")


sents = input("String: ")
start = time.time()
res, props = translate(model, sents, src_tokenizer, trg_tokenizer,
                       use_mask=model_param["use_mask"], device=device)

end = time.time()
print(end - start)
print(res)
print(props)
