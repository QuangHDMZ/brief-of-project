import os
import json
import argparse
import pandas as pd
import torch
from jiwer import wer

import torch.multiprocessing as mp
from models.model_factory import get_model
from utils import translate


def get_arg():
    parser = argparse.ArgumentParser()
    parser.add_argument('vocab_path')
    # parser.add_argument('test_path')
    parser.add_argument('weight_file')
    # parser.add_argument('--output_file', default='output.txt')
    parser.add_argument('--config_file', default='model_config.json')
    parser.add_argument('--model_name', default='big_evolved')
    parser.add_argument('--batch_size', type=int, default=32)
    parser.add_argument('--cuda', action='store_true', default=False)

    args = parser.parse_args()

    return args


if __name__ == '__main__':
    args = get_arg()

    # Load tokenizer
    print("Load tokenizer")
    tokenizer = torch.load(args.vocab_path)
    src_tokenizer = tokenizer['notone']
    trg_tokenizer = tokenizer['tone']
    src_pad_token = 0
    trg_pad_token = 0

    # Load model
    print("Init model")
    with open(args.config_file) as f:
        config = json.load(f)

    if args.model_name in config:
        model_param = config[args.model_name]
    else:
        raise Exception("Invalid model name")

    model_param['src_vocab_size'] = len(src_tokenizer.word_index) + 1
    model_param['trg_vocab_size'] = len(trg_tokenizer.word_index) + 1

    model = get_model(model_param)
    device = torch.device(
        'cuda' if torch.cuda.is_available() and args.cuda else 'cpu')
    print("Using", device.type)
    if device.type == 'cuda':
        model = model.cuda()

    if os.path.isfile(args.weight_file):
        print("Load model")
        state = torch.load(args.weight_file)
        if isinstance(state, dict):
            model.load_state_dict(state['model'])
        else:
            model.load_state_dict(state)
    else:
        raise Exception("Invalid weight path")

    dem_true = 0
    dem_false = 0
    print("load xong model")
    data_vi = pd.read_csv(
        "/content/drive/MyDrive/vn-accent/real_data/test1.notone")
    #data_vi = data_vi[:50000]
    data_X = pd.read_csv(
        "/content/drive/MyDrive/vn-accent/real_data/test1.tone")
    #data_X = data_X[:50000]
    print("qua roi hen qua 1")
    list_input = []
    list_output = []
    list_wer = []
    list_false = []
    list_src = []
    futures = []

    for index in data_vi.index:
        temp = data_vi.loc[index]['text_vi']
        temp_x = data_X.loc[index]['text_vi']
        print(index)
        sents = temp
        # sents = input("String: ")
        res = translate(model, sents, src_tokenizer, trg_tokenizer,
                        use_mask=model_param["use_mask"], device=device)

        if(wer(temp_x, res) == 0):
            dem_true = dem_true + 1
        else:
            dem_false = dem_false + 1
            list_false.append(res)
            list_wer.append(wer(temp_x, res))
            list_src.append(temp_x)
        list_output.append(res)
    print("qua roi hen qua 2")
    #Input = pd.DataFrame(list_input)
    Output = pd.DataFrame(list_output)
    Output_false = pd.DataFrame()
    Output_false = pd.DataFrame(list(zip(list_src, list_false, list_wer)),
                                columns=['Sents', 'Predict', 'Wer'])
    #Input.to_csv("Input.txt",header = None,index=None)
    Output.to_csv("Output_test_fado_final.txt", header=None, index=None)
    Output_false.to_csv("Output_false_test_fado_final.csv")
    print("So cau dung 100% ", dem_true)
    print("So cau chua dung 100%", dem_false)
