#!/bin/sh

ROOT_PATH=`pwd`
EXTENSION_PATH="${ROOT_PATH}/ext"
mkdir -p ${EXTENSION_PATH}
DATA_PATH="${ROOT_PATH}/data"

VENDOR_NAME="vendors"
PYTHON="${ROOT_PATH}/${VENDOR_NAME}/bin/python3"
PIP="${ROOT_PATH}/${VENDOR_NAME}/bin/pip3"

install_cuda() {
	echo "Build cuda"
	cd ${EXTENSION_PATH}
	wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
	sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
	wget https://developer.download.nvidia.com/compute/cuda/11.4.0/local_installers/cuda-repo-ubuntu2004-11-4-local_11.4.0-470.42.01-1_amd64.deb
	sudo dpkg -i cuda-repo-ubuntu2004-11-4-local_11.4.0-470.42.01-1_amd64.deb
	sudo apt-key add /var/cuda-repo-ubuntu2004-11-4-local/7fa2af80.pub
	sudo apt-get update
	sudo apt-get -y install cuda
}

install_python3_venv() {
    echo "Create venv fadoml"
    OS_TARGET=`uname -s`
	if [ "${OS_TARGET}" == "Linux" ]; then 
		sudo apt-get install libopenblas-dev libopenblas-pthread-dev libopenblas-openmp-dev libblas-dev
		sudo apt-get install -y nodejs
        sudo apt-get install -y libicu-dev
        sudo apt-get install -y libpython3-dev python3-dev cython3
		sudo apt-get install -y python3-venv
        sudo apt-get install -y python3-pip
		sudo apt install nvidia-cuda-toolkit
		sudo apt-get install -y libgl1-mesa-glx python3-opencv
		sudo apt-get install librocksdb-dev liblz4-dev libbz2-dev libsnappy-dev
		sudo apt-get install libmecab-dev
	fi

    cd ${ROOT_PATH}
    python3 -m venv ${VENDOR_NAME}
}

install_underthesea() {
    echo "Build underthesea"
	${PYTHON} -m pip install underthesea
    underthesea download-data VNTC
}

install_fadoml_pip() {
	echo "Install fado pip"

	mkdir -p ${ROOT_PATH}/tmp
	export TMPDIR="${ROOT_PATH}/tmp"

	OS_TARGET=`uname -s`
	if [ "${OS_TARGET}" == "Darwin" ]; then 
		brew install libomp
		export SKLEARN_NO_OPENMP=TRUE
	fi
	
	cd ${ROOT_PATH}
	${PYTHON} -m pip install wheel
	${PYTHON} -m pip install -r requirements.txt
}

install_fadoml() {
    echo "Building ML server"
	cd ${ROOT_PATH}
	${PYTHON} setup.py install
}

install_cococ() {
    echo "Build coccoc"
	cd ${EXTENSION_PATH}
	git clone --recurse https://github.com/miczone/coccoc-tokenizer.git
	mkdir ${EXTENSION_PATH}/coccoc-tokenizer/build
    cd ${EXTENSION_PATH}/coccoc-tokenizer/build
	cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_PYTHON=1 -DCMAKE_INSTALL_PREFIX:PATH=/usr .. 
	sudo make install -j 4
    sudo /sbin/ldconfig
    cd ${EXTENSION_PATH}/coccoc-tokenizer/python
    ${PYTHON} setup.py install
}

install_nmt_tokenizer() {
    echo "Build NMT Tokenizer"
	cd ${EXTENSION_PATH}
	git clone --recurse https://github.com/OpenNMT/Tokenizer.git
	mkdir -p ${EXTENSION_PATH}/Tokenizer/_build
	cd ${EXTENSION_PATH}/Tokenizer/_build
	cmake -DBUILD_SHARED_LIBS:BOOL=ON -DBUILD_TESTS:BOOL=OFF -DCMAKE_BUILD_TYPE:STRING=Release -DSPM_BUILD_TEST:BOOL=OFF ../
	make all -j 4
	sudo make all install
    sudo /sbin/ldconfig
	cd ${EXTENSION_PATH}/Tokenizer/bindings/python
	${PYTHON} setup.py install
}

install_nmt() {
	echo "Install openNMT"
	cd ${ROOT_PATH}/${VENDOR_NAME}
	git clone https://github.com/OpenNMT/OpenNMT-py.git
	cd OpenNMT-py
	${PYTHON} setup.py install
	${PIP} install -r requirements.opt.txt
}

install_httpx() {
	${PYTHON} -m pip install httpx
}

install_fastext() {
	echo "Build FatsText detect language"
	cd ${EXTENSION_PATH}
	git clone --recurse https://github.com/facebookresearch/fastText.git
	cd ${EXTENSION_PATH}/fastText
	${PYTHON} setup.py install
	wget "https://dl.fbaipublicfiles.com/fasttext/supervised-models/lid.176.bin"
	mv ${EXTENSION_PATH}/fastText/lid.176.bin ${ROOT_PATH}/data
}

install_vn_accent() {
	echo "Install vn-accent"
	cd ${ROOT_PATH}/${VENDOR_NAME}
	
	git clone https://github.com/VNOpenAI/vn-accent.git
	touch vn-accent/__init__.py
	sed -i 's/accent_utils/.accent_utils/g' vn-accent/utils.py	
	sed -i 's/models/.models/g' vn-accent/utils.py
	mv vn-accent vn_accent
	mv vn_accent lib/python*/site-packages
	cd ${ROOT_PATH}
}

install_nltk_data() {
	echo "Loading nltk data"
	${PYTHON} -m nltk.downloader -d "${ROOT_PATH}/${VENDOR_NAME}/nltk_data" averaged_perceptron_tagger stopwords punkt wordnet
}

install_bpemb_data() {
	rm -rf ${DATA_PATH}/tokenizer/*
	cd ${DATA_PATH}/tokenizer
	
	arr=( "vi" "ja" "zh" "en" "km" "ko" "de" )
	for (( i=0; i<${#arr[@]}; i++ ));
	do
		LANG_ROOT_PATH=${DATA_PATH}/tokenizer/${arr[$i]}
		rm -rf ${LANG_ROOT_PATH}
		mkdir -p ${LANG_ROOT_PATH}
		cd ${LANG_ROOT_PATH}
		wget "https://bpemb.h-its.org/${arr[$i]}/${arr[$i]}.wiki.bpe.vs200000.vocab"
		wget "https://bpemb.h-its.org/${arr[$i]}/${arr[$i]}.wiki.bpe.vs200000.model"
		wget "https://bpemb.h-its.org/${arr[$i]}/${arr[$i]}.wiki.bpe.vs200000.d300.w2v.bin.tar.gz"
		tar xf ${arr[$i]}.wiki.bpe.vs200000.d300.w2v.bin.tar.gz
		mv ${LANG_ROOT_PATH}/data/${arr[$i]}/${arr[$i]}.wiki.bpe.vs200000.d300.w2v.bin ${LANG_ROOT_PATH}
		rm -rf ${arr[$i]}.wiki.bpe.vs200000.d300.w2v.bin.tar.gz
		rm -rf ${LANG_ROOT_PATH}/data
	done

	MULTILANG_ROOT_PATH=${DATA_PATH}/tokenizer/multi
	rm -rf ${MULTILANG_ROOT_PATH}
	mkdir -p ${MULTILANG_ROOT_PATH}
	cd ${MULTILANG_ROOT_PATH}
	wget "https://bpemb.h-its.org/multi/multi.wiki.bpe.vs1000000.vocab"
	wget "https://bpemb.h-its.org/multi/multi.wiki.bpe.vs1000000.model"
	wget "https://bpemb.h-its.org/multi/multi.wiki.bpe.vs1000000.d300.w2v.bin.tar.gz"
	tar xf multi.wiki.bpe.vs1000000.d300.w2v.bin.tar.gz
	rm -rf multi.wiki.bpe.vs1000000.d300.w2v.bin.tar.gz
	
	cd ${ROOT_PATH}
}

install_spacy_data() {
	echo "Loading spacy data"
	cd ${ROOT_PATH}
	${PYTHON} -m spacy download en_core_web_sm
	${PYTHON} -m spacy download zh_core_web_sm
	${PYTHON} -m spacy download ja_core_news_sm
	${PYTHON} -m spacy download de_core_news_sm
	${PYTHON} -m spacy download ko_core_news_sm
	${PYTHON} -m spacy download xx_ent_wiki_sm
	${PYTHON} -m unidic download
	cd ${ROOT_PATH}
}

install_yolo5() {
	echo "Install yolo5"
	cd ${ROOT_PATH}/${VENDOR_NAME}
	git clone https://github.com/ultralytics/yolov5
	${PIP} install -r yolov5/requirements.txt  
}

cleanup() {
    echo "Cleanup temp"
	cd ${ROOT_PATH}
	sudo rm -rf ${EXTENSION_PATH}
	rm -rf ${ROOT_PATH}/build
	rm -rf ${ROOT_PATH}/dist
	rm -rf ${ROOT_PATH}/src/FadoML.egg-info
}

fix_keras() {
	file_util=${ROOT_PATH}/${VENDOR_NAME}/lib/python3.8/site-packages/keras/utils/generic_utils.py
	cp -rf ${file_util} ${file_util}.${DATE_TIME}
	sed '12d' ${file_util} > ${file_util}.t0
	sed '11 a from tensorflow.python.keras.utils.generic_utils import populate_dict_with_module_objects, to_snake_case' ${file_util}.t0 > ${file_util}.t1
	mv ${file_util}.t1 ${file_util}
}

install_python3_venv
source ${VENDOR_NAME}/bin/activate
install_fadoml_pip
install_cococ
install_underthesea
install_nmt_tokenizer
install_nmt
install_fadoml
install_httpx
install_fastext
install_vn_accent
install_nltk_data
install_yolo5
install_spacy_data
install_bpemb_data
cleanup