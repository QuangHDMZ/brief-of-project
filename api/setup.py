from setuptools import setup

setup(
    name="FadoML",
    install_requires=[
        "Werkzeug>=2.0.0rc4",
        "Jinja2>=3.0.0rc1",
        "itsdangerous>=2.0.0rc2",
        "click>=7.1.2",
        "sanic",
        "Flask",
        "Cython",
        "pybind11",
        "requests",
        "gevent",
        "eventlet",
        "pymongo",
        "pystalk",
        "nltk",
        "connection_pool"
    ],
    extras_require={
        "async": ["asgiref>=3.2"],
        "dotenv": ["python-dotenv"],
    },
)