# Fado global ML Restful

Fado global ML Restful API

# Installation
--------------

### Add logging
mkdir -p log
chown -R www-data:www-data log

### Build ALL

bash build_all.sh

### Build HTTP server ONLY

bash build_server.sh

# Testing
--------------

fadoml --debug 1

# Tokenizer service
--------------

### POST /tokenizer 
```
Params:
- lang : iso2code , ex: ja, vi, en
- option : 1 (fado), 2(cococ), 3(underthesea)
- text : text to tokenize as vector
```

https://github.com/miczone/coccoc-tokenizer/tree/master/python
https://github.com/OpenNMT/Tokenizer/tree/master/bindings/python

Curl testing:

https://iot.heratap.me/api/tokenizer?apikey=d4abf36d-11cb-4999-bd7e-2be1ee5adfbe&lang=vi&text=xin%20ch%C3%A0o,%20t%C3%B4i%20l%C3%A0%20ng%C6%B0%E1%BB%9Di%20Vi%E1%BB%87t%20Nam&option=2
https://iot.heratap.me/api/translate?apikey=d4abf36d-11cb-4999-bd7e-2be1ee5adfbe&lang_from=auto&lang_to=en&text=TITACUTE%20USB%20Type%20C%20%E8%B6%85%E6%80%A5%E9%80%9F%E5%85%85%E9%9B%BB%20%E3%82%B1%E3%83%BC%E3%83%96%E3%83%AB%20%5B5A%20%E8%B6%85%E6%80%A5%E9%80%9F%E5%85%85%E9%9B%BB%20%280.3m%20%E7%99%BD%E3%81%84%201%E6%9C%AC%29%5D%20SuperCharge%20Super%20Charging%20%E3%83%81%E3%83%A3%E3%83%BC%E3%82%B8%205A%20%E8%B6%85%E6%80%A5%E9%80%9F%E5%85%85%E9%9B%BB%20%E3%83%A2%E3%83%90%E3%82%A4%E3%83%AB%E3%83%90%E3%83%83%E3%83%86%E3%83%AA%E3%83%BC%20%E3%82%B1%E3%83%BC%E3%83%96%E3%83%AB%20Huawei%20Mate20%E3%80%81Mate%2020%20X%E3%80%81%20P20%20Pro%20P30%E3%80%81P30%20Pro%20%E3%82%B9%E3%83%BC%E3%83%91%E3%83%BC%E3%83%81%E3%83%A3%E3%83%BC%E3%82%B8%20super%20charge%20Type-C%E6%A9%9F%E5%99%A8%E5%AF%BE%E5%BF%9C%20%E7%B4%94%E6%AD%A3%E5%93%81%E3%81%A8%E5%90%8C%E3%81%98%E6%A9%9F%E8%83%BD%20%E9%AB%98%E5%93%81%E8%B3%AATPE%E7%B4%A0%E6%9D%90%20%E3%82%B1%E3%83%BC%E3%83%96%E3%83%AB%20Type%20C%20to%20Type%20A%20%E3%82%BF%E3%82%A4%E3%83%97C%E5%85%85%E9%9B%BB%20%E5%85%85%E9%9B%BB%E3%82%B1%E3%83%BC%E3%83%96%E3%83%AB%20type-c%20%E3%83%87%E3%83%BC%E3%82%BF%E8%BB%A2%E9%80%81%26
http://127.0.0.1:8085/api/tokenizer?apikey=d4abf36d-11cb-4999-bd7e-2be1ee5adfbe&lang=vi&text=xin%20ch%C3%A0o,%20t%C3%B4i%20l%C3%A0%20ng%C6%B0%E1%BB%9Di%20Vi%E1%BB%87t%20Nam&option=2

http://13.57.163.56/api/tokenizer?apikey=d4abf36d-11cb-4999-bd7e-2be1ee5adfbe&lang=vi&text=xin%20ch%C3%A0o,%20t%C3%B4i%20l%C3%A0%20ng%C6%B0%E1%BB%9Di%20Vi%E1%BB%87t%20Nam&option=2

http://13.57.163.56/api/tokenizer?apikey=d4abf36d-11cb-4999-bd7e-2be1ee5adfbe&lang=vi&text=Ch%C3%A0ng%20trai%209X%20Qu%E1%BA%A3ng%20Tr%E1%BB%8B%20kh%E1%BB%9Fi%20nghi%E1%BB%87p%20t%E1%BB%AB%20n%E1%BA%A5m%20s%C3%B2&option=1

http://13.57.163.56/api/tokenizer?apikey=d4abf36d-11cb-4999-bd7e-2be1ee5adfbe&lang=vi&text=B%E1%BB%99%20Y%20t%E1%BA%BF%20t%E1%BB%91i%206/5%20ghi%20nh%E1%BA%ADn%2060%20ca%20d%C6%B0%C6%A1ng%20t%C3%ADnh%20nCoV,%20trong%20%C4%91%C3%B3%2056%20ca%20l%C3%A2y%20nhi%E1%BB%85m%20trong%20n%C6%B0%E1%BB%9Bc,%204%20ca%20nh%E1%BA%ADp%20c%E1%BA%A3nh%20%C4%91%C6%B0%E1%BB%A3c%20c%C3%A1ch%20ly%20ngay.&option=3

# Mapping service
--------------

Fado global mapping APIs

# HScode service 
--------------

Fado global HScode prediction APIs

# Books
--------------

https://github.com/rockkhuya/DongDu