import os
import argparse
from fadoml import *
from fadoml.util.constant import ENGLISH_ISO2CODE, JAPANESE_ISO2CODE
from fadoml.app.util import nmt_preprocessor
from fadoml.util import open_translator, helper, tracking_translator
from fadoml.app import create_threadpool, load_captcha, load_hscoder, \
    load_proxies, load_bpe_tokenizer, load_ban_checker, \
    load_categorizer, load_ocr, load_vn_accent, load_id_card, \
    load_nmt_wiki_tokenizer, load_stopwords, load_textrank, \
    load_japanese_summarization, load_english_summarization
from fadoml.app.singleapp import start_app_thread, stop_app_thread
import logging
import torch.multiprocessing as mp
import eventlet
import gevent
import stopwordsiso as stopwords

### Check CUDA devices
helper.patch_cuda(True)
helper.patch_gpus(True)

### Parse parameters from CLI
parser = argparse.ArgumentParser(prog='fadoml')
parser.add_argument('--debug', type=int, default=1, help='Debug mode')
parser.add_argument('--havecococ', type=bool, default=True, help='Have cococ tokenizer model')
parser.add_argument('--host', type=str, default="127.0.0.1", help='HTTP Host')
parser.add_argument('--port', type=int, default=8085, help='HTTP Port')
parser.add_argument('--inport', type=int, default=7110, help='HTTP Internal Port')
parser.add_argument('--bhost', type=str, default="127.0.0.1", help='Beanstalkd Host')
parser.add_argument('--bport', type=int, default=11300, help='Beanstalkd Port')
parser.add_argument('--language', type=str, default="vi,en,ja", help='Supported languages (Split by common)')
parser.add_argument('--data', type=str, default="", help='Data path (includes models ....')
parser.add_argument('--upload', type=str, default="", help='Upload path')
parser.add_argument('--log', type=str, default="", help='Logger path')
parser.add_argument('--mongo_uri', type=str, default="127.0.0.1:27017", help='Mongo URI')
parser.add_argument('--mongo_username', type=str, default="", help='Mongo Username')
parser.add_argument('--mongo_password', type=str, default="", help='Mongo Password')
parser.add_argument('--nmttkdim', type=int, default=200000, help='BPE nmt dim number')
parser.add_argument('--nmttkmode', type=str, default="none", help='BPE nmt tokenize mode')
parser.add_argument('--mode', type=str, default="threading", help='Event mode : threading, eventlet, gevent')

def main() -> None:
    args = parser.parse_args()

    if (args.data is None) or (len(args.data) == 0):
        args.data = "{}/data".format(os.getcwd())

    if (args.upload is None) or (len(args.upload) == 0):
        args.upload = "{}/upload".format(os.getcwd())

    if (args.log is None) or (len(args.log) == 0):
        args.log = "{}/log".format(os.getcwd())

    logging_level = logging.INFO
    if args.debug == 2:
        logging_level = logging.DEBUG

    logger_file = "{}/app.log".format(args.log)
    logging.basicConfig(
        filename=logger_file, 
        level=logging_level, 
        format=f'%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s'
    )

    if args.debug == 1:
        print("Loading cococ and NMT tokenizer at : {}".format(args.data))
    languages = args.language.split(",")

    # Load all tokenizer model to MEMORY
    cocCocTokenizer = load_cococ_tokenizer(args.havecococ)
    nmt_language_supported = {
        "vi" : {
            "vs": 200000,
            "dim": 300,
            "mode": "none"
        }, 
        "ja" : {
            "vs": 200000,
            "dim": 300,
            "mode": "none"
        }, 
        "en" : {
            "vs": 200000,
            "dim": 300,
            "mode": "none"
        }, 
        "km" : {
            "vs": 200000,
            "dim": 300,
            "mode": "none"
        }, 
        "ko" : {
            "vs": 200000,
            "dim": 300,
            "mode": "none"
        }, 
        "zh" : {
            "vs": 200000,
            "dim": 300,
            "mode": "none"
        }, 
        "de" : {
            "vs": 200000,
            "dim": 300,
            "mode": "none"
        },
        "multi" : {
            "vs": 1000000,
            "dim": 300,
            "mode": "none"
        }
    }
    nmt_tokenizer = {el : load_nmt_wiki_tokenizer(el, args.data, nmt_language_supported[el]["vs"], nmt_language_supported[el]["dim"], nmt_language_supported[el]["mode"]) for el in nmt_language_supported}

    # Load all BPE tokenizer model to MEMORY
    bpe_language_supported = nmt_language_supported
    bpe_tokenizer = {el : load_bpe_tokenizer(el, args.data, bpe_language_supported[el]["vs"], bpe_language_supported[el]["dim"]) for el in bpe_language_supported}
    
    # Load page ranking
    textrank_language_supported = {
        "en": {
            "module": "en_core_web_sm"
        },
        "zh": {
            "module": "zh_core_web_sm"
        },
        "ja": {
            "module": "ja_core_news_sm"
        },
        "de": {
            "module": "de_core_news_sm"
        },
        "ko": {
            "module": "ko_core_news_sm"
        },
        "multi": {
            "module": "xx_ent_wiki_sm"
        }
    }
    spacy_textrank = {el : load_textrank(textrank_language_supported[el]["module"]) for el in textrank_language_supported}

    # Load all stopwords to MEMORY
    stopwords_language_supported = set(["km"])
    stopwords_language_supported = stopwords_language_supported | stopwords.langs()
    iso_stopwords = {el : load_stopwords(el, args.data) for el in stopwords_language_supported}

    # Load all models to MEMORY
    load_khmern_tokenizer()
    load_japanese_tokenizer()

    # Load summarization model
    summarization_map = {}
    summarization_map[ENGLISH_ISO2CODE] = load_english_summarization()
    summarization_map[JAPANESE_ISO2CODE] = load_japanese_summarization()

    # Get current CPU number
    cpu_min_number = 8
    cpu_number = (mp.cpu_count() + 1)
    if cpu_number < cpu_min_number:
        cpu_number = cpu_min_number

    if args.debug == 1:
        print("Loading beanstalkd at : {}:{}".format(args.bhost, args.bport))
    beanstalkdPool = load_beanstalk_pool(args.bhost, args.bport, cpu_number)

    gg_file_path = "{}/gg.json".format(args.data)
    openTranslator = open_translator.OpenTranslator(gg_file_path=gg_file_path, debug_mode=args.debug, proxies=load_proxies(args.data))
    trackingTranslator = tracking_translator.TrackingTranslator(args.mongo_uri, args.mongo_username, args.mongo_password, args.data, args.debug)

    if args.debug == 1:
        print("Upload path {}".format(args.upload))
    if not os.path.exists(args.upload):
        os.makedirs(args.upload)

    # Checker
    app_checker_port = args.inport
    app_checker = create_checker_app({
        "debug": args.debug, 
        "data_path": args.data, 
        "upload_path": args.upload,
        "model_languages": args.language,
        "trigger": [
            ("ban_checker", lambda : {el: load_ban_checker(el, args.data) for el in languages})
        ],
    })
    app_checker_thread = start_app_thread(app_checker, app_checker_port)
    
    # Categorizer
    app_mapping_port = args.inport + 1
    app_mapping = create_mapping_app({
        "debug": args.debug, 
        "data_path": args.data, 
        "upload_path": args.upload,
        "model_languages": args.language,
        "trigger": [
            ("categorizer", lambda: load_categorizer(args.data, ENGLISH_ISO2CODE, nmt_language_supported))
        ],
    })
    app_mapping_thread = start_app_thread(app_mapping, app_mapping_port)

    # NMTranslate
    app_nmt_translator_port = args.inport + 2
    app_nmt_translator = create_nmt_translator_app({
        "debug": args.debug, 
        "data_path": args.data, 
        "trigger": [
            ("nmt_translator", lambda: load_nmt_translator(args.data))
        ],
    })
    app_nmt_translator_thread = start_app_thread(app_nmt_translator, app_nmt_translator_port)

    #Finger print
    app_finger_print_port = args.inport + 3
    app_finger_print = create_finger_print_app({
        "debug": args.debug, 
        "data_path": args.data, 
        "trigger": [
            ("finger_print", lambda: load_finger_print(args.data))
        ],
    })
    app_finger_print_thread = start_app_thread(app_finger_print, app_finger_print_port)
    
    # OCR
    app_ocr_port = args.inport + 4
    ocr_data_path = "{}/ocr".format(args.data)
    helper.make_dirs_if_needed(ocr_data_path)
    app_ocr = create_ocr_app({
        "debug": args.debug, 
        "data_path": args.data, 
        "trigger": [
            ("ocr", lambda: load_ocr(ocr_data_path, helper.have_gpus(True)))
        ],
    })
    app_ocr_thread = start_app_thread(app_ocr, app_ocr_port)

    # Vn-accent
    app_vn_accent_port = args.inport + 5
    app_vn_accent = create_vn_accent_app({
        "debug": args.debug, 
        "data_path": args.data, 
        "trigger": [
            ("vn_accent", lambda: load_vn_accent(args.data, helper.have_cuda(True)))
        ],
    })
    app_vn_accent_thread = start_app_thread(app_vn_accent, app_vn_accent_port)

    # Hscode
    app_hscode_port = args.inport + 6
    app_hscode = create_hscode_app({
        "debug": args.debug, 
        "data_path": args.data, 
        "trigger": [
            ("hscoder", lambda: load_hscoder(args.data))
        ],
    })
    app_hscode_thread = start_app_thread(app_hscode, app_hscode_port)

    # Captcha
    app_captcha_port = args.inport + 7
    app_captcha = create_captcha_app({
        "debug": args.debug,
        "data_path": args.data,
        "trigger": [
            ("captcha", lambda: load_captcha(args.data, helper.have_cuda(True)))
        ],
    })
    app_captcha_thread = start_app_thread(app_captcha, app_captcha_port)

    # Identity card
    app_id_card_port = args.inport + 8
    app_id_card = create_id_card_app({
        "debug": args.debug,
        "data_path": args.data,
        "upload_path": args.upload,
        "threaded": True,
        "trigger": [
            ("id_card", lambda: load_id_card(args.data, helper.have_cuda(True))),
            ("threadpool", lambda: create_threadpool(cpu_number))
        ],
    })
    app_id_card_thread = start_app_thread(app_id_card, app_id_card_port)

    # All variables in Main Thread
    app = create_main_app({
        "debug": args.debug, 
        "data_path": args.data, 
        "upload_path": args.upload,
        "apikeys": load_api_json(args.data),
        "categories": load_category_json(args.data),
        "have_cococ": args.havecococ,
        "nmt_tokenizer": nmt_tokenizer, 
        "bpe_tokenizer": bpe_tokenizer,
        "spacy_textrank": spacy_textrank,
        "summarization_map": summarization_map,
        "threadpool": create_threadpool(cpu_number),
        "vi_tokenizer": load_vi_tokenizer(),
        "cococ_tokenizer": cocCocTokenizer,
        "open_translator": openTranslator,
        "iso_stopwords": iso_stopwords,
        "fast_text": load_fastext(args.data),
        "fastlangid": load_fastlangid(args.data),
        "gcld3": load_gcld3(),
        "tracking_translator": trackingTranslator,
        "beanstalkd": beanstalkdPool,
        "finger_print": load_finger_print(args.data),
        "checker_port": app_checker_port,
        "mapping_port": app_mapping_port,
        "nmt_translator_port": app_nmt_translator_port,
        "nmt_preprocessor": nmt_preprocessor.NMTPreprocessor(),
        "finger_print_port": app_finger_print_port,
        "ocr_port": app_ocr_port,
        "vn_accent_port": app_vn_accent_port,
        "hscode_port": app_hscode_port,
        "captcha_port": app_captcha_port,
        "id_card_port": app_id_card_port,
    })

    print("Http Server is starting at {}:{}".format(args.host, args.port))
    if args.mode == "eventlet":
        eventlet_socket = eventlet.listen((args.host, args.port))
        eventlet.wsgi.server(eventlet_socket, app)
    elif args.mode == "gevent":
        environ = {
            'wsgi.multithread': True,
            'SERVER_NAME': 'fado'
        }
        http_server = gevent.pywsgi.WSGIServer((args.host, args.port), application=app, environ=environ)
        http_server.serve_forever()
    else:
        app.run(host=args.host, port=args.port, debug = False, access_log=False, workers = cpu_number, backlog = 2048)
    
    stop_app_thread(app_checker_thread)
    stop_app_thread(app_mapping_thread)
    stop_app_thread(app_nmt_translator_thread)
    stop_app_thread(app_finger_print_thread)
    stop_app_thread(app_ocr_thread)
    stop_app_thread(app_vn_accent_thread)
    stop_app_thread(app_hscode_thread)
    stop_app_thread(app_captcha_thread)
    stop_app_thread(app_id_card_thread)
if __name__ == '__main__':
    main()