def test_ban_checker(image_checker, image_path="/home/hongdta/mydata/new/project/fado-global/src/fdglmlapi/upload/1012.jpg"):
    print("image_checker {}".format(image_checker))
    vector = image_checker[0].vectorize(image_path)
    vector = [vector]
    print("vector {}".format(vector))
    prediction = image_checker[1].predict(vector)
    result = {
        'yes': prediction[0][0],
        'no': prediction[0][1],
    }

    print("result {}".format(result))
    print("init classifier {}".format(image_checker[1]))

def test_categorizer(groups, translatedTextStr = 'Launch of Hobby Products · Cancellation Due to the deadline for figures, plastic models, anime goods, card games, Shokugan, the launch date may be postponed due to the convenience of the manufacturer.If the release date is postponed, we will notify you of the new release date by email.In addition, the cancel deadline is also changed as the release date postponement.Please check the latest cancellation deadline above.Also, manufacturers may change product specifications.Please note.'):
    #textStr = 'ホビー商品の発売日・キャンセル期限に関して フィギュア・プラモデル・アニメグッズ・カードゲーム・食玩の商品は、メーカー都合により発売日が延期される場合があります。 発売日が延期された場合、Eメールにて新しい発売日をお知らせします。また、発売日延期に伴いキャンセル期限も変更されます。 最新のキャンセル期限は上記よりご確認ください。また、メーカー都合により商品の仕様が変更される場合があります。あらかじめご了承ください。'
    
    vectorizer, classifier = groups['parent_group']
    vector = vectorizer.vectorize(translatedTextStr)
    groups_result = classifier.predict(vector)["groups"]
    print("groups_result {}".format(groups_result))

    group_max_prob = "group{}".format(max(groups_result, key=groups_result.get))
    vectorizer, classifier = groups[group_max_prob]
    vector = vectorizer.vectorize(translatedTextStr)
    nodes = classifier.predict(vector)["nodes"]
    print("nodes {}".format(nodes))