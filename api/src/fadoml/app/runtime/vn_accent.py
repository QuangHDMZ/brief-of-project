import os
import sys
import traceback
from fadoml.util.constant import *
from fadoml.app.runtime.model import AppModel
from flask import Flask, jsonify, current_app, request
from fadoml.util.helper import build_text, is_none_param, string_to_float, string_to_int
from fadoml.msg import error_msg, response_msg
import logging
from pyvi import ViUtils

class VnAccentModel(AppModel):
    def __init__(self, name):
        AppModel.__init__(self, name)

    def get_app(self, config=None):
        app = Flask(self.get_name(), instance_relative_config=True)

        if config is None:
            config = {}
        config["app_name"] = self.get_name()
        app.config.update(config)

        try:
            os.makedirs(app.instance_path)
        except OSError:
            pass

        @app.route("/")
        def index():
            return jsonify(self.to_json())

        @app.route("/api/predict", methods=("GET", "POST"))
        def predict():
            textStr = request.args.get("text")
            optionNum = request.args.get("option")
            if request.method == "POST":
                textStr = request.form["text"]
                optionNum = request.form.get("option")

            textStr = build_text(textStr)
            if is_none_param(textStr) == True:
                errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
                return jsonify(errorMsg.toJSON())
    
            if len(textStr) > MAX_LEN_VN_ACCENT or len(textStr) < MIN_LEN_VN_ACCENT:
                errorMsg = error_msg.ErrorMsg(1, "Valid len text from {} to {}".format(MIN_LEN_VN_ACCENT, MAX_LEN_VN_ACCENT))
                return jsonify(errorMsg.toJSON())

            if optionNum is None:
                optionNum = "0"
            optionNum = string_to_int(optionNum)

            text = None
            prob = 0.0
            # Use pyvi accents
            if optionNum > 0:
                try:
                    text =ViUtils.add_accents(textStr)
                    prob = 1.0
                except:
                    logging.error(traceback.format_tb(sys.exc_info()[2]))
                    pass
            
            # Use woka model data
            if not is_none_param(text):
                try:
                    accent_translator = current_app.config["vn_accent"]
                    text, prob = accent_translator.translate(textStr)
                except:
                    logging.error(traceback.format_tb(sys.exc_info()[2]))
                    pass
            
            # If accents is OK
            if not is_none_param(text):
                accentTranslatorMsg = response_msg.AccentTranslatorMsg(0, text, round(prob, DECIMAL_NUMBER))
                return jsonify(accentTranslatorMsg.toJSON())

            errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
            return jsonify(errorMsg.toJSON())

        return app
