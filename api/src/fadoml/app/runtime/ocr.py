import os, sys, traceback
from fadoml.util.constant import *
from fadoml.app.runtime.model import AppModel
from flask import Flask, jsonify, current_app, request
from fadoml.util.helper import build_text, is_none_param, string_to_float, string_to_int
from fadoml.msg import error_msg, response_msg
import logging

class OCRModel(AppModel):
    def __init__(self, name):
        AppModel.__init__(self,name)

    def get_app(self, config=None):
        app = Flask(self.get_name(), instance_relative_config=True)

        if config is None:
            config = {}
        config["app_name"] = self.get_name()
        app.config.update(config)

        try:
            os.makedirs(app.instance_path)
        except OSError:
            pass

        @app.route("/")
        def index():
            return jsonify(self.to_json())

        @app.route("/api/predict", methods=("GET", "POST"))
        def predict():
            img_file = request.args.get("img")
            lang = request.args.get("lang")
            min_prob = request.args.get("min_prob")
            rotate = request.args.get("rotate")

            if request.method == "POST":
                img_file = request.form["img"]
                lang = request.form["lang"]
                min_prob = request.form["min_prob"]
                rotate = request.form["rotate"]

            if lang not in VALID_LANGUAGES_OCR:
                errorMsg = error_msg.ErrorMsg(1, "Just support lang : {}".format(", ".join(VALID_LANGUAGES_OCR)))
                return jsonify(errorMsg.toJSON())

            min_prob = string_to_float(min_prob)
            rotate = string_to_int(rotate)
            
            if min_prob > 1 or min_prob < 0:
                errorMsg = error_msg.ErrorMsg(1, "Valid 0<= min_prob <=1")
                return jsonify(errorMsg.toJSON())

            if is_none_param(img_file):
                errorMsg = error_msg.ErrorMsg(1, "Invalid image file")
                return jsonify(errorMsg.toJSON())
            
            rotations = []
            for i, rotation in enumerate(ROTATIONS):
                if (rotate & i) == i:
                    rotations.append(rotation)

            if lang in VALID_LANGUAGES_EASYOCR:
                option = EASYOCR_TYPE
            
            if lang in VALID_LANGUAGES_PADDLEOCR:
                option = PADDLEOCR_TYPE

            try:
                ocr, ocv = current_app.config["ocr"]
                vector = ocv.vectorize(img_file, option)
                result = ocr.predict(vector, lang, min_prob, option, rotations)
                
                if not is_none_param(result):
                    ocrMsg = response_msg.ResponseMsg(0, result)
                    return jsonify(ocrMsg.toJSON())

            except:
                logging.error(traceback.format_tb(sys.exc_info()[2]))
                pass
            
            errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
            return jsonify(errorMsg.toJSON())

        return app