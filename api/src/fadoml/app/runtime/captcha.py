import os
import sys
import traceback
from fadoml.app.util import captcha_recognizer
from fadoml.util.constant import *
from fadoml.app.runtime.model import AppModel
from flask import Flask, jsonify, current_app, request
from fadoml.util.helper import argsort, build_text, is_none_param, string_to_float, string_to_int
from fadoml.msg import error_msg, response_msg
import logging


class CaptchaModel(AppModel):
    def __init__(self, name):
        AppModel.__init__(self, name)

    def get_app(self, config=None):
        app = Flask(self.get_name(), instance_relative_config=True)

        if config is None:
            config = {}
        config["app_name"] = self.get_name()
        app.config.update(config)

        try:
            os.makedirs(app.instance_path)
        except OSError:
            pass

        @app.route("/")
        def index():
            return jsonify(self.to_json())

        @app.route("/api/predict", methods=("GET", "POST"))
        def predict():
            img_file = request.args.get("img")
            pattern = request.args.get("pattern")

            if request.method == "POST":
                img_file = request.form["img"]
                pattern = request.form["pattern"]

            if is_none_param(img_file):
                errorMsg = error_msg.ErrorMsg(1, "Invalid image file")
                return jsonify(errorMsg.toJSON())

            pattern = string_to_int(pattern)
            if pattern not in VALID_PATTERN_CAPTCHA_ENUM:
                errorMsg = error_msg.ErrorMsg(1, "Invalid pattern captcha")
                return jsonify(errorMsg.toJSON())

            try:
                captcha_reg, captcha_vec = current_app.config["captcha"]

                if pattern in range(1,5):
                    models = {
                        "model_pattern_1234": captcha_reg.models["model_pattern_1234"]
                    }
                elif pattern == 5:
                    models = {
                        "model_pattern_5": captcha_reg.models["model_pattern_5"]
                    }
                else: # get all 
                    models = captcha_reg.models

                vector = captcha_vec.vectorize(img_file)
                results = []
                probs = []

                for model_name, model in models.items():
                    label, prob = captcha_reg.predict(model, vector)
                    result = {
                        "model": model_name,
                        "captcha": label,
                        "prob": round(prob, DECIMAL_NUMBER),
                    }
                    
                    results.append(result)
                    probs.append(prob)

                if len(probs) > 1:
                    probs_argsort = argsort(probs)
                    results = [results[i] for i in probs_argsort]

                if len(results) != 0:
                    captchaMsg = response_msg.ResponseMsg(0, results)
                    return jsonify(captchaMsg.toJSON())

            except:
                logging.error(traceback.format_tb(sys.exc_info()[2]))
                pass

            errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
            return jsonify(errorMsg.toJSON())

        return app
