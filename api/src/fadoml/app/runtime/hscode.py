import os, sys, traceback
from fadoml.util.constant import *
from fadoml.app.runtime.model import AppModel
from flask import Flask, jsonify, current_app, request
from fadoml.util.helper import build_text, is_none_param
from fadoml.msg import error_msg, response_msg
import logging

class HscodeModel(AppModel):
    def __init__(self, name):
        AppModel.__init__(self,name)

    def get_app(self, config=None):
        app = Flask(self.get_name(), instance_relative_config=True)

        if config is None:
            config = {}
        config["app_name"] = self.get_name()
        app.config.update(config)

        try:
            os.makedirs(app.instance_path)
        except OSError:
            pass

        @app.route("/")
        def index():
            return jsonify(self.to_json())

        @app.route("/api/predict", methods=("GET", "POST"))
        def predict():
            textStr = request.args.get("text")
            lang = request.args.get("lang")
            if request.method == "POST":
                textStr = request.form["text"]
                lang = request.form["lang"]

            if is_none_param(textStr) or (lang != ENGLISH_ISO2CODE):
                errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
                return jsonify(errorMsg.toJSON())

            textStr = build_text(textStr)
            if len(textStr) == 0:
                errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
                return jsonify(errorMsg.toJSON())

            hscoder = current_app.config["hscoder"]
            # Predict
            try:
                hscodes = []
                n_top = 3
                for group_name, classifier in hscoder.classifiers.items():
                    preprocessed_text = hscoder.preprocess_text(textStr, group_name)
                    probabilities, labels = classifier.predict(preprocessed_text, n_top)
                    
                    for i, label in enumerate(labels):
                        hscode = hscoder.label_mapping[group_name][str(label)]
                        hscode_info = hscoder.hscode_info[hscode] if hscode in hscoder.hscode_info else {}
                        
                        hscodes.append({
                            "child_hscode": hscode,
                            "prob": round(float(probabilities[i]), DECIMAL_NUMBER),
                            "group": group_name,
                            "info":hscode_info,
                        })

                hscodeMsg = response_msg.HScodeMsg(0, lang, hscodes)
                return jsonify(hscodeMsg.toJSON())
            except:
                logging.error(traceback.format_tb(sys.exc_info()[2]))
                pass
            
            errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
            return jsonify(errorMsg.toJSON())

        return app