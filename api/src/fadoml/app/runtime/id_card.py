import os, sys, traceback
from telnetlib import BINARY
from unittest import result

from fadoml.util.constant import *
from fadoml.app.runtime.model import AppModel
from flask import Flask, jsonify, current_app, request
from fadoml.util.helper import is_none_param,argsort, string_to_int
from fadoml.util.imgutil import convert_base64_to_cv_image, convert_base64_to_pil_image, convert_cv_image_to_base64, convert_pil_image_to_base64, convert_cv2_img_to_pil_img,toImgPIL
from fadoml.msg import error_msg, response_msg
import logging
import concurrent.futures
import numpy as np
import cv2
import imutils
from tensorflow.keras.preprocessing import image
from PIL import Image

"""
Create internal id card app
"""



class IDCardModel(AppModel):
    def __init__(self, name):
        AppModel.__init__(self,name)

    def get_app(self, config=None):
        app = Flask(self.get_name(), instance_relative_config=True)

        if config is None:
            config = {}
        config["app_name"] = self.get_name()
        app.config.update(config)
        try:
            os.makedirs(app.instance_path)
        except OSError:
            pass

        @app.route("/")
        def index():
            return jsonify(self.to_json())
        

        def id_binary_classification(cv_img, model):
            dim_binary = BINARY_SIZE
            cv_img = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
            cv_img=cv2.resize(cv_img, dim_binary, interpolation = cv2.INTER_AREA)
            np_img = np.asarray(cv_img)
            np_img = np_img*(1./255)
            X = np.expand_dims(np_img,axis=0)
            result = model.predict(X)
            # binary model will always return 1 (cmnd) or 0(cccd)
            return np.round(result)
    
        def detect_corner(predictions_align_df):
            predictions_align_df["sum"] = predictions_align_df["xmin"] + predictions_align_df["ymin"]
            index_ = predictions_align_df["sum"].idxmax()
            if index_== 0:
                top_right = (predictions_align_df.xmax[0],predictions_align_df.ymax[0])
                bot_right = (predictions_align_df.xmin[2],predictions_align_df.ymax[2]) 
                bot_left = (predictions_align_df.xmin[3],predictions_align_df.ymin[3]) 
                top_left = (predictions_align_df.xmax[1],predictions_align_df.ymin[1]) 
            elif index_ == 1:
                top_left = (predictions_align_df.xmax[1],predictions_align_df.ymax[1])
                top_right = (predictions_align_df.xmin[0],predictions_align_df.ymax[0]) 
                bot_right = (predictions_align_df.xmin[2],predictions_align_df.ymin[2]) 
                bot_left = (predictions_align_df.xmax[3],predictions_align_df.ymin[3]) 
            elif index_ == 3:
                bot_left = (predictions_align_df.xmax[3],predictions_align_df.ymax[3])
                top_left = (predictions_align_df.xmin[1],predictions_align_df.ymax[1]) 
                top_right = (predictions_align_df.xmin[0],predictions_align_df.ymin[0]) 
                bot_right = (predictions_align_df.xmax[2],predictions_align_df.ymin[2]) 
            else:
                bot_right = (predictions_align_df.xmax[2],predictions_align_df.ymax[2])
                bot_left = (predictions_align_df.xmin[3],predictions_align_df.ymax[3]) 
                top_left = (predictions_align_df.xmin[1],predictions_align_df.ymin[1]) 
                top_right = (predictions_align_df.xmax[0],predictions_align_df.ymin[0])    
            arr_corner = np.float32([
                top_left, top_right , bot_right, bot_left
            ]) 
            return arr_corner

        def perspective_transoform(image, source_points):
            dest_points = np.float32([[0,0], [640,0], [640,480], [0,480]])
            M = cv2.getPerspectiveTransform(source_points, dest_points)
            dst = cv2.warpPerspective(image, M, (640, 480))
            dst = cv2.cvtColor(dst, cv2.COLOR_BGR2RGB)
            return dst

        def fingerprint_extractor(label, cv_img, predictions_df):
            key_name = MAPPING_LABELS_FPRINT_ON_ID_CARD[label]
            res = (key_name, "", 0.0)

            try:
                bbox = predictions_df.loc[predictions_df[predictions_df['name']==label].index]
                if bbox.empty:
                    return res
                
                xmin = bbox.xmin
                ymin = bbox.ymin
                xmax = bbox.xmax
                ymax = bbox.ymax
                image_crop = cv_img[int(ymin):int(ymax), int(xmin):int(xmax)]

                img_base64 = convert_cv_image_to_base64(image_crop)
                res = (key_name, img_base64, round(float(bbox["confidence"]), DECIMAL_NUMBER),image_crop)
            except:
                logging.error(traceback.format_tb(sys.exc_info()[2]))
            
            return res

        @app.route("/api/vn/fingerprint", methods=("GET", "POST"))
        def detect_vn_fingerprint():
            imgbase64 = request.args.get("imgbase64")
            if request.method == "POST":
                imgbase64 = request.form["imgbase64"]
                check_classify = request.form["classify"]
            check_classify = string_to_int(check_classify)     
            cv_img = convert_base64_to_cv_image(imgbase64)
            if cv_img is None:
                errorMsg = error_msg.ErrorMsg(1, INVALID_IMAGE)
                return jsonify(errorMsg.toJSON())
            
            try:
                fp_classifier = current_app.config["id_card"]["fp_classifier"]
                fp_verifier = current_app.config["id_card"]["fp_verifier"]
                vectorizer = current_app.config["id_card"]["vectorizer"]
                
                fprint_model = current_app.config["id_card"]["fprint"]
                if fprint_model is None:
                    errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
                    return jsonify(errorMsg.toJSON())

                align_model = current_app.config["id_card"]["align"]
                if align_model is None:
                    errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
                    return jsonify(errorMsg.toJSON())

                cv_img = cv2.resize(cv_img, (IDCARD_ALIGN_SIZE, IDCARD_ALIGN_SIZE))
                cv_img = cv2.cvtColor(cv_img, cv2.COLOR_RGB2BGR)

                predictions_align =  align_model(cv_img)
                predictions_align_df = predictions_align.pandas().xyxy[0]
                if len(predictions_align_df) ==0:
                    errorMsg = error_msg.ErrorMsg(1, INVALID_CORNER)
                    return jsonify(errorMsg.toJSON())
                
                predictions_align_df.sort_values(["class","confidence"],inplace=True,ascending = False)    
                predictions_align_df.drop_duplicates("class",inplace =True)
                predictions_align_df.reset_index(drop=True,inplace=True)
                if len(predictions_align_df) <4:
                    errorMsg = error_msg.ErrorMsg(1, INVALID_CORNER)
                    return jsonify(errorMsg.toJSON())
                arr_corner = detect_corner(predictions_align_df)
                align_img = perspective_transoform(cv_img, arr_corner)
                align_crop = align_img.copy()
                align_img = toImgPIL(align_img)
                # phai convert sang pil 
                predictions = fprint_model(align_img) # predict
                predictions_df = predictions.pandas().xyxy[0]
                if len(predictions_df) == 0:
                    errorMsg = error_msg.ErrorMsg(1, INVALID_IMAGE)

                    return jsonify(errorMsg.toJSON())
                res = {}
                executor = current_app.config["threadpool"]
                
                futures = []
                for label in MAPPING_LABELS_FPRINT_ON_ID_CARD.keys():
                    futures.append(executor.submit(fingerprint_extractor, label=label, cv_img=align_crop, predictions_df=predictions_df))

                for future in concurrent.futures.as_completed(futures):
                    item = future.result()
                    if check_classify > 0:
                        vector = vectorizer.vectorize_idcard(item[3], MIN_IMG_SIZE_FPRINT_VERIFY)
                        prob_verifier = fp_verifier.predict(vector)[0][0].astype(float).round(DECIMAL_NUMBER)
                        if prob_verifier >= THRESHOLD_VERIFY :
                            vector = vectorizer.vectorize_idcard(item[3], MIN_IMG_SIZE_FPRINT)
                            probs_classifier = fp_classifier.predict(vector)[0].astype(float)
                            probs_argsort = argsort(probs_classifier)
                            labels_fprint = LABELS_FPRINT_LEFT if item[0] == LEFT_PRINT else LABELS_FPRINT_RIGHT
                        else:
                            errorMsg = error_msg.ErrorMsg(1, INVALID_FPRINT)
                            return jsonify(errorMsg.toJSON())
                        res_fprint_classifier = []
                        for id in probs_argsort:
                            res_fprint_classifier.append({
                                'class': labels_fprint[id],
                                'prob': round(probs_classifier[id], DECIMAL_NUMBER),
                            })    
                    else :
                        res_fprint_classifier = "N/A"

                    if len(item) == 4:
                        res[item[0]]= {
                            "imgbase64": item[1],
                            "prob": item[2],
                            "type": res_fprint_classifier
                        }
                resMsg = response_msg.ResponseMsg(0, res)
                return jsonify(resMsg.toJSON())  
                    
            except:
                logging.error(traceback.format_tb(sys.exc_info()[2]))
                pass
            
            errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
            return jsonify(errorMsg.toJSON())

        def img2text_ocr(label, img, vietocr_model, new_predictions_df):
            res = (label, "")
            
            try:
                bbox = new_predictions_df.loc[new_predictions_df[new_predictions_df['name']==label].index]
                if bbox.empty:
                    return res

                ymin = bbox.ymin
                ymax = bbox.ymax 
                xmin = bbox.xmin 
                xmax = bbox.xmax

                img = np.asarray(img)
                image_crop = img[int(ymin):int(ymax), int(xmin):int(xmax)]
                image_crop = convert_cv2_img_to_pil_img(image_crop)

                if image_crop != None:
                    text = vietocr_model.predict(image_crop)
                    res = (label, text)
            except:
                logging.error(traceback.format_tb(sys.exc_info()[2]))
            
            return res

        @app.route("/api/vn/ocr", methods=("GET", "POST"))
        def detect_vn_ocr():
            def text_box_fixing_v1(res, label, separator=" "):
                new_text_arr = []
                label_1 = "{}_1".format(label)
                label_2 = "{}_2".format(label)
                if label_1 in res:
                    new_text_arr.append(res[label_1])
                if label_2 in res:
                    new_text_arr.append(res[label_2])
                return separator.join(new_text_arr)


            imgbase64 = request.args.get("imgbase64")
            if request.method == "POST":
                imgbase64 = request.form["imgbase64"]

            cv_img = convert_base64_to_cv_image(imgbase64) 
            # # use cv image have performance than pil image
            if cv_img is None:
                errorMsg = error_msg.ErrorMsg(1, INVALID_IMAGE)
                return jsonify(errorMsg.toJSON())

            try:
                align_model = current_app.config["id_card"]["align"]
                if align_model is None:
                    errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
                    return jsonify(errorMsg.toJSON())

                binary_model = current_app.config["id_card"]["binary"]
                frontside_model = current_app.config["id_card"]["frontside"]
                if frontside_model is None:
                    errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
                    return jsonify(errorMsg.toJSON())

                frontside_model_new = current_app.config["id_card"]["frontside_new"]
                if frontside_model_new is None:
                    errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
                    return jsonify(errorMsg.toJSON())

                vietocr_model = current_app.config["id_card"]["vietocr"]
                if vietocr_model is None:
                    errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
                    return jsonify(errorMsg.toJSON())

                cv_img = cv2.resize(cv_img, (IDCARD_ALIGN_SIZE, IDCARD_ALIGN_SIZE))
                cv_img = cv2.cvtColor(cv_img, cv2.COLOR_RGB2BGR)

                predictions_align =  align_model(cv_img)
                predictions_align_df = predictions_align.pandas().xyxy[0]
                if len(predictions_align_df) ==0:
                    errorMsg = error_msg.ErrorMsg(1, INVALID_CORNER)
                    return jsonify(errorMsg.toJSON())

                predictions_align_df.sort_values(["class","confidence"],inplace=True,ascending = False)
                predictions_align_df.drop_duplicates("class",inplace=True)
                predictions_align_df.reset_index(drop=True,inplace=True)

                if len(predictions_align_df) <4:
                    errorMsg = error_msg.ErrorMsg(1, INVALID_CORNER)
                    return jsonify(errorMsg.toJSON())
                arr_corner = detect_corner(predictions_align_df)
                align_img = perspective_transoform(cv_img, arr_corner)
                check_cmnd = id_binary_classification(align_img,binary_model)
                
                align_img = toImgPIL(align_img)
                if check_cmnd == 1:
                    predictions = frontside_model(align_img)
                else:
                    predictions = frontside_model_new(align_img)
                
                predictions_df = predictions.pandas().xyxy[0]
                if len(predictions_df) == 0:
                    errorMsg = error_msg.ErrorMsg(1, INVALID_IMAGE)
                    return jsonify(errorMsg.toJSON())    
                idx=predictions_df.groupby(['name'])['confidence'].transform(max) == predictions_df['confidence'] #groupby + extract max confidence per class
                new_predictions_df=predictions_df[idx]
                fid_conf = MIN_FID_CONFIDENCE_THRESH
                new_predictions_df=new_predictions_df[new_predictions_df.confidence>=fid_conf] #filter by threshold

                res = {}
                executor = current_app.config["threadpool"]
                
                futures = []
                for label in LABELS_FRONT_SIDE_ID_CARD[1:]:
                    futures.append(executor.submit(img2text_ocr, label=label, img=align_img, vietocr_model=vietocr_model, new_predictions_df=new_predictions_df))

                for future in concurrent.futures.as_completed(futures):
                    item = future.result()
                    if len(item) == 2:
                        res[item[0]] = item[1]

                for label in USER_INFO_FRONT_SIDE_ID_CARD:
                    if label == 'name':
                        res[label] = text_box_fixing_v1(res, label, ' ').upper()
                    elif label in ["home", "address"]:
                        res[label] = text_box_fixing_v1(res, label, ', ').upper()

                resMsg = response_msg.ResponseMsg(0, res)
                return jsonify(resMsg.toJSON())
            except:
                logging.error(traceback.format_tb(sys.exc_info()[2]))
                pass
            
            errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
            return jsonify(errorMsg.toJSON())

        @app.route("/api/vn/avatar", methods=("GET", "POST"))
        def detect_vn_avatar():
            imgbase64 = request.args.get("imgbase64")
            if request.method == "POST":
                imgbase64 = request.form["imgbase64"]

            cv_img = convert_base64_to_cv_image(imgbase64)
            if cv_img is None:
                errorMsg = error_msg.ErrorMsg(1, INVALID_IMAGE)
                return jsonify(errorMsg.toJSON())
            try:
                align_model = current_app.config["id_card"]["align"]
                if align_model is None:
                    errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
                    return jsonify(errorMsg.toJSON())

                frontside_model = current_app.config["id_card"]["frontside"]
                if frontside_model is None:
                    errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
                    return jsonify(errorMsg.toJSON())

                cv_img = cv2.resize(cv_img, (IDCARD_ALIGN_SIZE, IDCARD_ALIGN_SIZE))
                cv_img = cv2.cvtColor(cv_img, cv2.COLOR_RGB2BGR)
                
                predictions_align =  align_model(cv_img)
                predictions_align_df = predictions_align.pandas().xyxy[0]
                if len(predictions_align_df) ==0:
                    errorMsg = error_msg.ErrorMsg(1, INVALID_CORNER)
                    return jsonify(errorMsg.toJSON())
                predictions_align_df.sort_values(["class","confidence"],inplace=True,ascending = False)
                predictions_align_df.drop_duplicates("class",inplace=True)
                predictions_align_df.reset_index(drop=True,inplace=True)
                if len(predictions_align_df) <4:
                    errorMsg = error_msg.ErrorMsg(1, INVALID_CORNER)
                    return jsonify(errorMsg.toJSON())
                
                arr_corner = detect_corner(predictions_align_df)
                align_img = perspective_transoform(cv_img, arr_corner)
                align_img = toImgPIL(align_img)    
                predictions = frontside_model(align_img)
                label = LABELS_FRONT_SIDE_ID_CARD[0]
                predictions_df = predictions.pandas().xyxy[0]
                if (len(predictions_df) == 0) or ("avatar" not in predictions_df['name'].values.tolist()):
                    errorMsg = error_msg.ErrorMsg(1, INVALID_IMAGE)
                    return jsonify(errorMsg.toJSON())
                avatars = predictions_df[predictions_df.name==label].sort_values(by=['confidence'],ascending=False)
                ava_conf = round(float(avatars.iloc[0]["confidence"]),DECIMAL_NUMBER)
                if ava_conf < MIN_AVA_CONFIDENCE_THRESH:
                    errorMsg = error_msg.ErrorMsg(1, INVALID_IMAGE)
                    return jsonify(errorMsg.toJSON())
                res = {}
                if not avatars.empty:
                    avatar = avatars.iloc[0]
                    ymin = avatar.ymin
                    ymax = avatar.ymax 
                    xmin = avatar.xmin 
                    xmax = avatar.xmax
                    align_img = np.asarray(align_img)
                    image_crop = align_img[int(ymin):int(ymax), int(xmin):int(xmax)]
                    image_crop = cv2.cvtColor(image_crop,cv2.COLOR_BGR2RGB)
                    img_base64 = convert_cv_image_to_base64(image_crop)
                    res[label]= {
                        "imgbase64": img_base64,
                        "prob": round(float(avatar["confidence"]), DECIMAL_NUMBER),
                    }

                resMsg = response_msg.ResponseMsg(0, res)
                return jsonify(resMsg.toJSON())   
            except:
                logging.error(traceback.format_tb(sys.exc_info()[2]))
                pass
            errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
            return jsonify(errorMsg.toJSON())

        return app