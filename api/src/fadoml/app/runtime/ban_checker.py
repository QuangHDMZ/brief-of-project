import os, sys, traceback
from fadoml.util.constant import *
from fadoml.app.runtime.model import AppModel
from flask import Flask, jsonify, current_app, request
from fadoml.util.helper import build_text, is_none_param, exist_file, remove_file
from fadoml.msg import error_msg, response_msg
import numpy as np
import logging

class CheckerModel(AppModel):
    def __init__(self, name):
        AppModel.__init__(self, name)

    def get_app(self, config=None):
        app = Flask(self.get_name(), instance_relative_config=True)

        if config is None:
            config = {}
        config["app_name"] = self.get_name()
        app.config.update(config)

        try:
            os.makedirs(app.instance_path)
        except OSError:
            pass

        @app.route("/")
        def index():
            return jsonify(self.to_json())

        @app.route("/api/predict", methods=("GET", "POST"))
        def predict():
            textStr = request.args.get("text")
            imageStr = request.args.get("image")
            textOCR = request.args.get("text_ocr")
            lang = request.args.get("lang")
            if request.method == "POST":
                textStr = request.form.get("text")
                imageStr = request.form.get("image")
                textOCR = request.form.get("text_ocr")
                lang = request.form.get("lang")

            if (lang not in current_app.config["ban_checker"]) or (current_app.config["ban_checker"][lang] is None):
                errorMsg = error_msg.ErrorMsg(1, "lang={} is not support".format(lang))
                return jsonify(errorMsg.toJSON())

            json_data = {}
            if is_none_param(textStr) == False:
                textStr = build_text(textStr)
                if len(textStr) == 0:
                    errorMsg = error_msg.ErrorMsg(1, "text is empty")
                    return jsonify(errorMsg.toJSON())
                # Predict
                try:
                    (vectorizer, classifier) = current_app.config["ban_checker"][lang]["text"]
                    vector = vectorizer.transform(np.array([textStr]))
                    prediction = classifier.predict_proba(vector)
                    if len(prediction) > 0:
                        json_data["text"] = {
                            "yes": round(float(prediction[0][1]), 3),
                            "no": round(float(prediction[0][0]), 3)
                        }
                except:
                    logging.error(traceback.format_tb(sys.exc_info()[2]))
                    pass

            if is_none_param(textOCR) == False:
                textOCR = build_text(textOCR)
                if len(textOCR) == 0:
                    errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
                    return jsonify(errorMsg.toJSON())

                # Predict
                try:
                    (vectorizer, classifier) = current_app.config["ban_checker"][lang]["text"]
                    vector = vectorizer.transform(np.array([textOCR]))
                    prediction = classifier.predict_proba(vector)
                    if len(prediction) > 0:
                        json_data["ocr"] = {
                            "text": textOCR,
                            "yes": round(float(prediction[0][1]), 3),
                            "no": round(float(prediction[0][0]), 3)
                        }
                except:
                    logging.error(traceback.format_tb(sys.exc_info()[2]))
                    pass

            if is_none_param(imageStr) == False:
                if exist_file(imageStr) == True:
                    (vectorizer, classifier) = current_app.config["ban_checker"][lang]["image"]
                    vector = vectorizer.vectorize(imageStr)
                    if vector is not None:
                        prediction = classifier.predict([vector])
                        if len(prediction) > 0:
                            json_data["image"] = {
                                "yes": round(float(prediction[0][0]), 3),
                                "no": round(float(prediction[0][1]), 3)
                            }
                    remove_file(imageStr)

            if len(json_data) > 0:
                categoryMsg = response_msg.DetectBannedProductMsg(0, lang, json_data)
                return jsonify(categoryMsg.toJSON())

            errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
            return jsonify(errorMsg.toJSON())

        return app