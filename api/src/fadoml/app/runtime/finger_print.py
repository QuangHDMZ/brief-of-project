import os, sys, traceback
from fadoml.util.constant import *
from fadoml.app.runtime.model import AppModel
from flask import Flask, jsonify, current_app, request
from fadoml.util.helper import argsort, is_none_param, string_to_int
from fadoml.msg import error_msg, response_msg
import logging

class FingerPrintModel(AppModel):
    def __init__(self, name):
        AppModel.__init__(self,name)

    def get_app(self, config=None):
        app = Flask(self.get_name(), instance_relative_config=True)

        if config is None:
            config = {}
        config["app_name"] = self.get_name()
        app.config.update(config)

        try:
            os.makedirs(app.instance_path)
        except OSError:
            pass

        @app.route("/")
        def index():
            return jsonify(self.to_json())

        @app.route("/api/classify", methods=("GET", "POST"))
        def classify():
            img_file = request.args.get("img")
            side_hand_enum = request.args.get("side_hand_enum")

            if request.method == "POST":
                img_file = request.form["img"]
                side_hand_enum = request.form["side_hand_enum"]
            
            
            if is_none_param(img_file):
                errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
                return jsonify(errorMsg.toJSON())

            side_hand_enum = string_to_int(side_hand_enum)
            if side_hand_enum not in SIDE_HAND.keys():
                errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
                return jsonify(errorMsg.toJSON())
            try:
                fprint = current_app.config["finger_print"]
                classifier, vectorizer = fprint["classifier"], fprint["vectorizer"]
                vector = vectorizer.vectorize(img_file)
                probs = classifier.predict(vector)[0].astype(float)
                probs_argsort = argsort(probs)
                labels_fprint = LABELS_FPRINT_LEFT if side_hand_enum == 1 else LABELS_FPRINT_RIGHT
                
                res = []
                for id in probs_argsort:
                    res.append({
                        'class': labels_fprint[id],
                        'prob': round(probs[id], DECIMAL_NUMBER),
                    })

                if not is_none_param(res):
                    fingerPrintMsg = response_msg.ResponseMsg(0, res)
                    return jsonify(fingerPrintMsg.toJSON())
            except:
                logging.error(traceback.format_tb(sys.exc_info()[2]))
                pass
            
            errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
            return jsonify(errorMsg.toJSON())

        @app.route("/api/verify", methods=("GET", "POST"))
        def verify():
            img_file = request.args.get("img")
            if request.method == "POST":
                img_file = request.form["img"]

            if is_none_param(img_file):
                errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
                return jsonify(errorMsg.toJSON())

            try:
                fprint = current_app.config["finger_print"]
                verifier, vectorizer = fprint["verifier"], fprint["vectorizer"]
                vector = vectorizer.vectorize(img_file, MIN_IMG_SIZE_FPRINT_VERIFY)
                prob = verifier.predict(vector)[0][0].astype(float).round(DECIMAL_NUMBER)

                threshold = 0.5
                prob_pos = prob if prob >= threshold else round (1 - prob, DECIMAL_NUMBER)
                prob_neg = round(1 - prob_pos, DECIMAL_NUMBER)

                data = {
                    "yes": prob_pos,
                    "no": prob_neg,
                }

                fingerPrintMsg = response_msg.ResponseMsg(0, data)
                return jsonify(fingerPrintMsg.toJSON())
                   
            except:
                logging.error(traceback.format_tb(sys.exc_info()[2]))
                pass
            
            errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
            return jsonify(errorMsg.toJSON())

        return app