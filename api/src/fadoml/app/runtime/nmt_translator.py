import os, sys, traceback
from fadoml.util.constant import *
from fadoml.app.runtime.model import AppModel
from flask import Flask, jsonify, current_app, request
from fadoml.util.helper import build_text, convert_score_nmt, is_none_param
from fadoml.msg import error_msg, response_msg
import logging
import math

class NMTTranslatorModel(AppModel):
    def __init__(self, name):
        AppModel.__init__(self,name)

    def get_app(self, config=None):
        app = Flask(self.get_name(), instance_relative_config=True)

        if config is None:
            config = {}
        config["app_name"] = self.get_name()
        app.config.update(config)

        try:
            os.makedirs(app.instance_path)
        except OSError:
            pass

        @app.route("/")
        def index():
            return jsonify(self.to_json())

        @app.route("/api/predict", methods=("GET", "POST"))
        def predict():
            modelID = request.args.get("id")
            src = request.args.get("src")
            if request.method == "POST":
                modelID = request.form["id"]
                src = request.form["src"]
            
            logging.inf("modelID = {}".format(modelID))
            logging.inf("src = {}".format(src))
            try:
                serverModel = current_app.config["nmt_translator"]
                modelID = int(modelID)
            
                inputs = [{"id": modelID, "src": src}]
                trans, scores, nBest, _, aligns = serverModel.run(inputs)
                outStr = trans[0]
                score = scores[0]
                
                new_score = round(convert_score_nmt(score), DECIMAL_NUMBER)

                if not is_none_param(outStr):
                    if modelID == 3:
                        outStr = outStr.replace("_", " ")

                    nmtTranslatorMsg = response_msg.NMTTranslatorMsg(0, outStr, new_score)
                    return jsonify(nmtTranslatorMsg.toJSON())
            except:
                logging.error(traceback.format_tb(sys.exc_info()[2]))
                pass

            errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
            return jsonify(errorMsg.toJSON())

        return app