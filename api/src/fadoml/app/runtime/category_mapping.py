import os, sys, traceback
from fadoml.util.constant import *
from fadoml.app.runtime.model import AppModel
from flask import Flask, jsonify, current_app, request
from fadoml.util.helper import build_text, is_none_param
from fadoml.msg import error_msg, response_msg
import logging

class MappingModel(AppModel):
    def __init__(self, name):
        AppModel.__init__(self,name)

    def get_app(self, config=None):
        app = Flask(self.get_name(), instance_relative_config=True)

        if config is None:
            config = {}
        config["app_name"] = self.get_name()
        app.config.update(config)

        try:
            os.makedirs(app.instance_path)
        except OSError:
            pass

        @app.route("/")
        def index():
            return jsonify(self.to_json())

        @app.route("/api/predict", methods=("GET", "POST"))
        def predict():
            textStr = request.args.get("text")
            lang = request.args.get("lang")
            if request.method == "POST":
                textStr = request.form["text"]
                lang = request.form["lang"]

            if is_none_param(textStr) or (lang != ENGLISH_ISO2CODE):
                errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
                return jsonify(errorMsg.toJSON())

            textStr = build_text(textStr)
            if len(textStr) == 0:
                errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
                return jsonify(errorMsg.toJSON())

            categorizer = current_app.config["categorizer"]
            if categorizer is None:
                errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
                return jsonify(errorMsg.toJSON())
            
            # Predict
            try:
                # Detect group
                vectorizer, classifier = categorizer['parent_group']
                vector = vectorizer.vectorize(textStr)
                groups = classifier.predict(vector)["groups"]
                if current_app.config["debug"] > 0:
                    logging.info("groups {}".format(groups))

                # Detect node_id
                group_max_prob = "group{}".format(max(groups, key=groups.get))
                vectorizer, classifier = categorizer[group_max_prob]
                vector = vectorizer.vectorize(textStr)
                nodes = classifier.predict(vector)["nodes"]

                if current_app.config["debug"] > 0:
                    logging.info("nodes {}".format(nodes))

                categoryMsg = response_msg.CategoryMsg(0, lang, nodes)
                return jsonify(categoryMsg.toJSON())
            except:
                logging.error(traceback.format_tb(sys.exc_info()[2]))
                pass
            
            errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
            return jsonify(errorMsg.toJSON())

        return app