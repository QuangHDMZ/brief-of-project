import logging
from datetime import datetime

class AppModel:
    def __init__(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def to_json(self):
        return {
            "name": self.name,
            "time": str(datetime.now())
        }

    def show_info(self):
        logging.info("Model name {}", self.name)