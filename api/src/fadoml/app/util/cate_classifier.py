from fadoml.app.util import nn_torch
import torch

class CateClassifier():
    def __init__(self, model_params, nodes):
        [model, model_state] = self.load_model(model_params)
        self.model = model
        self.model_state = model_state
        self.nodes = nodes

    def load_model(self, params):
        vocab_size = params["vocab_size"]
        hidden_layer1 = params["hidden_layer1"]
        hidden_layer2 = params["hidden_layer2"]
        hidden_layer3 = params["hidden_layer3"]
        n_groups = params["n_groups"]
        model_path = params["model_path"]

        model = nn_torch.Neural_Network(vocab_size, hidden_layer1, hidden_layer2, hidden_layer3, n_groups)
        model_state = torch.load(model_path)
        model.load_state_dict(model_state)

        return [model, model_state]

    def predict(self, vector):
        predictions, probabilities = self.model.predict(vector)

        max_results = 3
        results = {
            "groups": {},
            "nodes": [],
        }

        for i in range(max_results):
            results["groups"][str(predictions[i])] = round(float(probabilities[i]), 3)
            if len(self.nodes) != 0:
                results["nodes"].append({
                    self.nodes[str(predictions[i])]: round(float(probabilities[i]), 3)
                })

        return results
