from transformers import DistilBertTokenizer, DistilBertForSequenceClassification
import torch

from fadoml.util.constant import INVALID_PARAM_MSG


class SeqTransformerClassifier():
    def __init__(self, config):
        self.set_config(config)

    def set_config(self, config):
        model_path = config["model_path"]
        n_labels = config["n_labels"]
        max_length = config["max_length"]
        from_pretrained = config["from_pretrained"]

        model = self.load_model(model_path, n_labels)
        tokenizer = self.load_tokenizer(from_pretrained)

        if model is None or tokenizer is None or max_length is None:
            raise Exception(INVALID_PARAM_MSG)

        self.model = model
        self.tokenizer = tokenizer
        self.max_length = max_length

    def load_model(self, model_path, n_labels):
        model = DistilBertForSequenceClassification.from_pretrained(
            model_path, num_labels=n_labels)

        return model

    def load_tokenizer(self, from_pretrained):
        tokenizer = DistilBertTokenizer.from_pretrained(from_pretrained)
        return tokenizer

    def vectorize(self, text):
        vector = self.tokenizer(
            text,
            truncation=True,
            padding=True,
            max_length=self.max_length,
            return_tensors="pt",
        )

        return vector

    def predict(self, text, top_n):
        vector = self.vectorize(text)
        predictions = self.model(**vector)

        predictions = torch.nn.functional.softmax(predictions.logits, dim=-1)
        probabilities, labels = predictions.data.topk(top_n)
        probabilities, labels = probabilities[0].numpy(), labels[0].numpy()

        return probabilities, labels
