from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import string
from fadoml.app.util import seq_keras_classifier, seq_transformer_classifier
from fadoml.util.constant import BERT_BASE_UNCASED, INVALID_PARAM_MSG, SEQ_TRANSFORMER


class HScodeClassifier():
    def __init__(self, label_mapping, hscode_info):
        if label_mapping is None or hscode_info is None:
            raise Exception(INVALID_PARAM_MSG)

        self.label_mapping = label_mapping
        self.hscode_info = hscode_info
        self.classifiers = {}
        self.stop_words = set(stopwords.words("english"))

    def preprocess_text(self, text, name):
        tokens = word_tokenize(text)
        
        if name == 'group1' or name == 'group3':
            # convert to lower case
            tokens = [w.lower() for w in tokens]

            # remove punctuation from each word
            table = str.maketrans("", "", string.punctuation)
            stripped = [w.translate(table) for w in tokens]

            # remove remaining tokens that are not alphabetic
            tokens = [word for word in stripped if word.isdigit() == False]

        # filter out stop words
        words = [w for w in tokens if not w in self.stop_words]

        preprocessed_text = " ".join(words)

        return preprocessed_text

    def set_classifier(self, config):
        if config["type"] == SEQ_TRANSFORMER:
            seq_classifier = seq_transformer_classifier.SeqTransformerClassifier(config)
        else:
            seq_classifier = seq_keras_classifier.SeqKerasClassifier(config)

        self.classifiers[config["name"]] = seq_classifier
