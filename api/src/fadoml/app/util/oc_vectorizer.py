import cv2
import numpy as np
from fadoml.util.constant import EASYOCR_TYPE, MAX_IMG_SIZE_OCR, MIN_IMG_SIZE_OCR, VALID_LANGUAGES_EASYOCR
import imutils

class OCVectorizer():
    def __init__(self, dataPath, haveGpu):
        self.dataPath = dataPath
        self.haveGpu = haveGpu

    def resize_to_square(self, img):
        # get size
        height, width, channels = img.shape

        # Create a black img
        x = height if height > width else width
        y = height if height > width else width

        dim = (MAX_IMG_SIZE_OCR, MAX_IMG_SIZE_OCR)
        if x < MIN_IMG_SIZE_OCR:
            dim = (MIN_IMG_SIZE_OCR, MIN_IMG_SIZE_OCR)

        square = np.zeros((x, y, 3), np.uint8)
        square[int((y - height) / 2):int(y - (y - height) / 2),
               int((x - width) / 2):int(x - (x - width) / 2)] = img

        img = cv2.resize(square, dim, interpolation=cv2.INTER_AREA)
        return img

    def inverted_img(self, img):
        img = cv2.bitwise_not(img)
        return img

    def vectorize(self, img, option):
        img = cv2.imread(img)
        img = self.resize_to_square(img)

        # if option == EASYOCR_TYPE:
        #     img = self.inverted_img(img)
        
        return img
