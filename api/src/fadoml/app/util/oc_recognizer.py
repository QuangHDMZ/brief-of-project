from fadoml.util.constant import *
import easyocr
from paddleocr import PaddleOCR
import imutils

class OCRecognizer():
    def __init__(self, dataPath, haveGpu):
        self.dataPath = dataPath
        self.haveGpu = haveGpu

        self.models = {
            EASYOCR_TYPE: {el: self.load_easyocr(el) for el in VALID_LANGUAGES_EASYOCR},
            PADDLEOCR_TYPE: {el: self.load_paddleocr(
                el) for el in VALID_LANGUAGES_PADDLEOCR}
        }

    def load_easyocr(self, lang):
        model = easyocr.Reader(
            [lang], gpu=self.haveGpu, model_storage_directory=self.dataPath)
        return model

    def load_paddleocr(self, lang):
        model = PaddleOCR(
            lang=VALID_LANGUAGES_PADDLEOCR[lang], use_gpu=self.haveGpu, model_storage_directory=self.dataPath)
        return model

    def clean_text_with_confident(self, prediction, min_prob, option):
        clean_text = []
        for i in range(len(prediction)):
            if option == EASYOCR_TYPE:
                text = prediction[i][1]
                prob = prediction[i][2]

            if option == PADDLEOCR_TYPE:
                text, prob = prediction[i][1]

            if prob >= min_prob:
                clean_text.append(text)

        clean_text = " ".join(clean_text)

        return clean_text

    def predict(self, img, lang, min_prob, option, rotations):
        cleaned_predictions = {}

        for rotation in rotations:
            prediction = ""

            if rotation != 0:
                img = imutils.rotate(img, angle=-rotation)

            if option == EASYOCR_TYPE:
                prediction = self.models[EASYOCR_TYPE][lang].readtext(img)

            if option == PADDLEOCR_TYPE:
                prediction = self.models[PADDLEOCR_TYPE][lang].ocr(img)

            cleaned_text = self.clean_text_with_confident(
                prediction, min_prob, option)

            cleaned_predictions[rotation] = cleaned_text

        return cleaned_predictions
