import torch
import json
from fadoml.util.constant import INVALID_PARAM_MSG
from fadoml.util.helper import exist_file, softmax
from fadoml.util.rwutil import read_torch, read_json
from vn_accent.models.model_factory import get_model
from vn_accent.utils import extract_words, pad_sequences, forward, remove_tone_line


class AccentTranslator():
    def __init__(self):
        self.model = None
        self.src_tokenizer = None
        self.trg_tokenizer = None
        self.model_param = None
        self.device = None

    def set_config(self, vocab_path, config_file, model_file, model_name, haveCuda):
        device = torch.device('cuda' if haveCuda else 'cpu')

        # Load tokenizer
        tokenizer = read_torch(vocab_path, device.type)
        src_tokenizer = tokenizer['notone']
        trg_tokenizer = tokenizer['tone']

        # Load model
        config = read_json(config_file)
        if model_name not in config or exist_file(model_file) == False:
            raise Exception(INVALID_PARAM_MSG)

        model_param = config[model_name]
        model_param['src_vocab_size'] = len(src_tokenizer.word_index) + 1
        model_param['trg_vocab_size'] = len(trg_tokenizer.word_index) + 1

        model = get_model(model_param)
        if haveCuda:
            model = model.cuda()

        state = read_torch(model_file, device.type)
        if isinstance(state, dict):
            model.load_state_dict(state['model'])
        else:
            model.load_state_dict(state)

        # Set config
        self.model = model
        self.src_tokenizer = src_tokenizer
        self.trg_tokenizer = trg_tokenizer
        self.model_param = model_param
        self.device = device

    def translate(self, text):
        text, prob = self.custom_translate(self.model, text, self.src_tokenizer, self.trg_tokenizer,
                                           use_mask=self.model_param["use_mask"], device=self.device)

        return text, prob

    # Custom translate function from vn_accent.utils
    def custom_translate(self, model, sents, src_tokenizer, trg_tokenizer, maxlen=200, use_mask=True, device=None):

        words, word_indices = extract_words(sents)
        lower_words = [x.lower() for x in words]

        # Tokenize words
        known_word_mask = []  # Same size as words - True if word is in word list, otherwise False
        seqs = []
        for word in lower_words:
            if word in src_tokenizer.word_index:
                seqs.append(src_tokenizer.word_index[word])
                known_word_mask.append(True)
            else:
                seqs.append(1)
                known_word_mask.append(False)
        seqs = [seqs]

        # Model inference
        seqs = pad_sequences(seqs, maxlen, padding='post')
        seqs = torch.tensor(seqs).long()
        if device is not None and device.type == 'cuda':
            seqs = seqs.cuda()
        with torch.no_grad():
            probs = forward(model, seqs, 0, use_mask=use_mask)
        probs = probs.cpu().detach().numpy()

        # Add tone
        output = sents
        probs = probs[0]
        prob_indices = probs.argsort(axis=-1)[:, ::-1]
        prob_indices = prob_indices[:, :100]
        output_prob = 1  # My custom

        for i, word in enumerate(lower_words):

            # Skip unknown words
            if not known_word_mask[i]:
                continue

            # Find the best solution
            for idx in prob_indices[i, :]:
                target_word = trg_tokenizer.sequences_to_texts([[idx]])[0]

                # My custom - modify if statement (add check prob_indices)
                if prob_indices[i][0] != 1 and remove_tone_line(target_word.lower()) == word:
                # End custom
                    begin_idx, end_idx = word_indices[i]

                    # Correct lower / upper case
                    corrected_word = ""
                    for ic, char in enumerate(words[i]):
                        if char.islower():
                            corrected_word += target_word[ic].lower()
                        else:
                            corrected_word += target_word[ic].upper()

                    output = output[:begin_idx] + \
                        corrected_word + output[end_idx:]

                    # My custom
                    target_prob = softmax(probs[i])[idx]
                    output_prob *= target_prob
                    # End custom
                    break

        return output, output_prob
