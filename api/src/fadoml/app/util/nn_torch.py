import torch

class Neural_Network(torch.nn.Module):
    def __init__(self, n_feature, n_hidden1, n_hidden2, n_hidden3, n_output):
        super(Neural_Network, self).__init__()
        self.hidden1 = torch.nn.Linear(n_feature, n_hidden1)   # hidden layer
        self.hidden1.share_memory()
        self.hidden2 = torch.nn.Linear(n_hidden1, n_hidden2)
        self.hidden2.share_memory()
        self.hidden3 = torch.nn.Linear(n_hidden2, n_hidden3)
        self.hidden3.share_memory()
        self.out = torch.nn.Linear(n_hidden3, n_output)   # output layer
        self.out.share_memory()

    def forward(self, x):
        combine = self.hidden1(x)
        activate = torch.nn.functional.relu(combine)
        combine = self.hidden2(activate)
        activate = torch.nn.functional.relu(combine)
        combine = self.hidden3(activate)
        activate = torch.nn.functional.relu(combine)
        return self.out(activate)

    def predict(self, x):
        predict = self.forward(x)
        prob = torch.nn.functional.softmax(predict)

        predict = predict.detach().numpy()
        prob = prob.detach().numpy()

        sorted_predict = predict.argsort()[::-1]
        sorted_prob = []
        for i in sorted_predict:
            sorted_prob.append(round(prob[i], 3))
        return (sorted_predict, sorted_prob)
