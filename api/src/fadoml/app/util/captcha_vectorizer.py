import cv2
from fadoml.util.constant import WEIGHT_IMG_SIZE_CAPTCHA, HEIGHT_IMG_SIZE_CAPTCHA
from torchvision import transforms


class CaptchaVectorizer():
    def resize_img(self, img, dim):
        img = cv2.resize(img, dim)

        return img

    def convert_to_tensor(self, img):
        convert_tensor = transforms.ToTensor()
        img = convert_tensor(img)

        return img

    def vectorize(self, img_path):
        img = cv2.imread(img_path)
        img = self.resize_img(img, (WEIGHT_IMG_SIZE_CAPTCHA, HEIGHT_IMG_SIZE_CAPTCHA))
        img = self.convert_to_tensor(img)

        return img
