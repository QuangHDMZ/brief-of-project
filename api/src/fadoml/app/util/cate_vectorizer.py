from nltk.stem.snowball import SnowballStemmer
import nltk
import math
import torch
import numpy as np

class CateVectorizer():
    def __init__(self, vocab_df, n_samples, tokenzier):
        self.vocab_df = vocab_df
        self.n_samples = n_samples
        self.tokenizer = tokenzier

        self.snow_stemmer = SnowballStemmer(language='english')

    def pick_noun(self, li):
        tagged = nltk.pos_tag(li)
        li_noun = []
        for (word, tag) in tagged:
            if tag in ['NN', 'NNS', 'NNP', 'NNPS']:  # Tag của các loại danh từ
                li_noun.append(word)
        return li_noun

    # lower chu, xoa bo cac ki tu ko can thiet
    def clean_text(self, text):
        # convert text to lowercase
        text = str(text)
        text = text.strip().lower()

        # replace punctuation characters with spaces
        filters = '!"\'#$%&()*+,-./:;<=>?@[\\]^_`{|}~\t\n,1,2,3,4,5,6,7,8,9,0,00'
        translate_dict = dict((c, " ") for c in filters)
        translate_map = str.maketrans(translate_dict)
        text = text.translate(translate_map)

        return text

    # Stemming đưa các từ về dạng nguyên thủy của nó.

    def stemming(self, term):
        return self.snow_stemmer.stem(term)

    def calculate_term_frequency(self, text):
        if (text is None):
            raise Exception("Invalid params")

        term_dict = {}
        tokens, _ = self.tokenizer.tokenize(text)
        tokens = self.pick_noun(tokens)  # Pick only noun

        for term in tokens:
            #term = stemming(term)
            if (term in term_dict):
                term_dict[term] += 1
            else:
                term_dict[term] = 1

        return term_dict

    def vectorize(self, text):
        text = self.clean_text(text)
        tf_dict = self.calculate_term_frequency(text)
        vocab_df = self.vocab_df
        n_samples = self.n_samples

        vector = []
        if (len(tf_dict) > 0):
            max_tf = max(tf_dict.values())

            for term, df in vocab_df.items():
                idf = math.log10(n_samples / df)

                if term in tf_dict:
                    weight = round((tf_dict[term] / max_tf) * idf, 2)
                    vector.append(weight)
                else:
                    vector.append(0)
        else:
            vector = np.zeros(len(vocab_df))

        return torch.FloatTensor(vector)
