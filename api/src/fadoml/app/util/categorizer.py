from fadoml.util import rwutil
from fadoml.app.util import cate_vectorizer, cate_classifier
import math
from fadoml.util.helper import exist_file

class Categorizer():
    def __init__(self, tokenizer):
        self.tokenizer = tokenizer

    def load_model_by_group(self, group_path, group_by):
        preprocessed_data = rwutil.read_json("{}/preprocessed_data.json".format(group_path))
        if preprocessed_data is None:
            raise Exception("Invalid group_path {}".format(group_path))

        vocab_size = preprocessed_data["vocab_size"]
        groups = preprocessed_data["groups"]
        n_groups = len(groups)
        n_samples = preprocessed_data["n_samples"]
        nodes = preprocessed_data["nodes"]
        completed_model_percent = 100

        vocab_df = rwutil.read_json("{}/vocab_df_group_by_{}_n_groups_{}_vocab_size_{}_lang_en.txt".format(group_path, group_by, n_groups, vocab_size))
        if vocab_df is None:
            raise Exception("Invalid vocab_df {}".format(vocab_df))

        model_path = "{}/model/pytorch_model_vocab_size_{}_n_groups_{}_data_{}%.sav".format(group_path, vocab_size, n_groups, completed_model_percent)
        if exist_file(model_path) == False:
            raise Exception("Invalid model_path {}".format(model_path))

        # load classifier
        hidden_layer1 = int(math.sqrt(vocab_size * n_groups))
        hidden_layer2 = int(math.sqrt((hidden_layer1 + 1) * n_groups))
        hidden_layer3 = int(math.sqrt((hidden_layer2 + 1) * n_groups))

        model_params = {
            "hidden_layer1": hidden_layer1,
            "hidden_layer2": hidden_layer2,
            "hidden_layer3": hidden_layer3,
            "vocab_size": vocab_size,
            "n_groups": n_groups,
            "model_path": model_path
        }

        classifier = cate_classifier.CateClassifier(model_params, nodes)

        # load vectorizer
        vectorizer = cate_vectorizer.CateVectorizer(vocab_df, n_samples, self.tokenizer)
        
        return (vectorizer, classifier)
