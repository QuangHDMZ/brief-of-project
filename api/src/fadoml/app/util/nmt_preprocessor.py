from fadoml.util.constant import CONTRACTIONS_EN_MODIFIED, JAPANESE_ISO2CODE
import html
import re
from fadoml.util.helper import is_none_param


class NMTPreprocessor():
    def convert_lower(self, text):
        text = str(text)
        text = text.lower()
        return text

    def decontract_en(self, text):
        for word in text.split():
            if word.lower() in CONTRACTIONS_EN_MODIFIED:
                text = text.replace(
                    word, CONTRACTIONS_EN_MODIFIED[word.lower()])
        return text

    def clean_text(self, text):
        text = self.convert_lower(text)

        # decode html
        text = html.unescape(text)

        # Replace any character which is not a "word character", ASCII (\[^a-zA-Z0-9_])
        # https://docs.python.org/3/library/re.html
        # https://i.imgur.com/E7bPR98.png
        text = re.sub('\W+', ' ', text)
        text = text.strip()

        return text

    def preprocess_en(self, text):
        text = self.clean_text(text)
        text = self.decontract_en(text)

        return text

    def preprocess_vi(self, text, tokenizer):
        text = self.clean_text(text)
        text = tokenizer.tokenize(text)

        return text

    def preprocess_ja(self, text, tokenizer):
        text = self.clean_text(text)

        pattern_1 = re.compile(
            u"([\u3000-\u303f\u3040-\u309f\u30a0-\u30ff\uff00-\uff9f\u4e00-\u9faf\u3400-\u4dbf]+)")
        pattern_2 = re.compile("[A-Za-z0-9]+")
        matches = re.split(pattern_1, text)

        for i in range(len(matches)):
            if pattern_2.match(matches[i]) is not None:
                continue

            tokens = tokenizer.tokenize(text)[0]

            # filter and merge
            new_tokens = ''
            for j in range(0, len(tokens)):
                token = tokens[j]
                token = token.replace('▁', '')

                if is_none_param(token):
                    continue

                new_tokens = new_tokens + ' ' + token

            matches[i] = new_tokens

        text = ' '.join(matches)

        return text
