
import cv2
import numpy as np
from fadoml.util.constant import MIN_IMG_SIZE_FPRINT


class FPrintVectorizer():
    def resize(self, img, size, interpolation=cv2.INTER_NEAREST):
        resized = cv2.resize(img, (size, size), interpolation=interpolation)
        return resized
    
    def vectorize_idcard(self, img_arr, img_size=MIN_IMG_SIZE_FPRINT):
        # img = cv2.imread(img, cv2.IMREAD_COLOR)
        # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img_arr = self.resize(img_arr, img_size)
        img_arr = img_arr / 255.0
        img_arr = np.array(img_arr)
        img_arr=img_arr.reshape((1, img_arr.shape[0], img_arr.shape[1], img_arr.shape[2]))
        #img_arr = img_arr.reshape(-1, img_size,img_size, 3)
        return img_arr
    
    def vectorize(self, img_arr, img_size=MIN_IMG_SIZE_FPRINT):
        img_arr = cv2.imread(img_arr, cv2.IMREAD_COLOR)
        img_arr = cv2.cvtColor(img_arr, cv2.COLOR_BGR2RGB)
        img_arr = self.resize(img_arr, img_size)
        img_arr = img_arr / 255.0
        img_arr = np.array(img_arr)
        # img_arr=img_arr.reshape((1, img_arr.shape[0], img_arr.shape[1], img_arr.shape[2]))
        img_arr = img_arr.reshape(-1, img_size,img_size, 3)
        return img_arr
