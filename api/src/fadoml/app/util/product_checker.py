import pickle
import os
import tensorflow as tf
import keras
from fadoml.app.util import img_vectorizer
from fadoml.util.helper import exist_file, exist_dir

class ProductChecker():
    def load_ban_text(self, path):
        if exist_file(path) == False:
            return None

        with open(path, 'rb') as file:
            vectorizer, classifier = pickle.load(file)
        return (vectorizer, classifier)

    def load_ban_image(self, path):
        if exist_file(path) == False:
            return None

        vectorizer = img_vectorizer.ImgVectorizer()
        classifier = keras.models.load_model(path)
        return (vectorizer, classifier)