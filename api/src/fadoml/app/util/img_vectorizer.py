import cv2
import numpy as np
import logging, traceback, sys
from fadoml.util.constant import MIN_IMG_SIZE

class ImgVectorizer():
    def crop_and_resize(self, img, w, h):
        im_h, im_w, _ = img.shape
        res_aspect_ratio = w/h
        input_aspect_ratio = im_w/im_h

        if input_aspect_ratio > res_aspect_ratio:
            im_w_r = int(input_aspect_ratio*h)
            im_h_r = h
            img = cv2.resize(img, (im_w_r , im_h_r))
            x1 = int((im_w_r - w)/2)
            x2 = x1 + w
            img = img[:, x1:x2, :]

        if input_aspect_ratio < res_aspect_ratio:
            im_w_r = w
            im_h_r = int(w/input_aspect_ratio)
            img = cv2.resize(img, (im_w_r , im_h_r))
            y1 = int((im_h_r - h)/2)
            y2 = y1 + h
            img = img[y1:y2, :, :]

        if input_aspect_ratio == res_aspect_ratio:
            img = cv2.resize(img, (w, h))

        return img

    def crop_square(self, img, size, interpolation=cv2.INTER_AREA):
        h, w = img.shape[:2]
        min_size = np.amin([h,w])
        crop_img = img[int(h/2-min_size/2):int(h/2+min_size/2), int(w/2-min_size/2):int(w/2+min_size/2)]
        resized = cv2.resize(crop_img, (size, size), interpolation=interpolation)
        return resized

    def vectorize(self, img):
        try:
            img = cv2.imread(img, cv2.IMREAD_COLOR)
            new_arr = self.crop_square(img, MIN_IMG_SIZE)
            new_arr = np.array(new_arr)
            new_arr = new_arr/255.0
            new_arr = new_arr.reshape(-1, MIN_IMG_SIZE, MIN_IMG_SIZE, 3)
            return new_arr
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
        return None