import tensorflow as tf
from fadoml.util.constant import INVALID_PARAM_MSG
from fadoml.util.rwutil import read_pickle
from tensorflow.keras.preprocessing.sequence import pad_sequences


class SeqKerasClassifier():
    def __init__(self, config):
        self.set_config(config)

    def set_config(self, config):
        model_path = config["model_path"]
        tokeniker_path = config["tokeniker_path"]
        max_length = config["max_length"]

        model = self.load_model(model_path)
        tokenizer = self.load_tokenizer(tokeniker_path)

        if model is None or tokenizer is None or max_length is None:
            raise Exception(INVALID_PARAM_MSG)

        self.model = model
        self.tokenizer = tokenizer
        self.max_length = max_length

    def load_model(self, model_path):
        model = tf.keras.models.load_model(model_path)
        return model

    def load_tokenizer(self, tokeniker_path):
        tokenizer = read_pickle(tokeniker_path)
        return tokenizer

    def vectorize(self, text):
        sequences = self.tokenizer.texts_to_sequences([text])
        vector = pad_sequences(sequences, padding="post",
                               maxlen=self.max_length)

        return vector

    def predict(self, text, top_n):
        vector = self.vectorize(text)
        predictions = self.model.predict(vector)
        
        labels = (-predictions[0]).argsort()[:top_n]
        probabilities = predictions[0][labels]

        return probabilities, labels
