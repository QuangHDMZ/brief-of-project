import torch
import string
from fadoml.app.util import nn_captcha
from fadoml.util.constant import WEIGHT_IMG_SIZE_CAPTCHA, HEIGHT_IMG_SIZE_CAPTCHA
from fadoml.util.helper import exist_file, softmax
import numpy as np


class CaptchaRecognizer():
    def __init__(self, have_cuda):
        self.have_cuda = have_cuda
        self.characters = '-' + string.digits + \
            string.ascii_uppercase + string.ascii_lowercase
        self.n_classes = len(self.characters)
        self.n_input_length = 12
        self.n_label_length = 6
        self.models = {}

    def set_model(self, model_path, model_name):
        if exist_file(model_path) == False:
            raise Exception("Invalid model_path {}".format(model_path))

        model = nn_captcha.Model(self.n_classes, input_shape=(
            3, HEIGHT_IMG_SIZE_CAPTCHA, WEIGHT_IMG_SIZE_CAPTCHA))

        if self.have_cuda:
            model = model.cuda()
            model.load_state_dict(torch.load(model_path))
        else:
            model.load_state_dict(torch.load(
                model_path, map_location=torch.device('cpu')))

        model.eval()

        self.models[model_name] = model

    def predict(self, model, vector):
        if self.have_cuda:
            output = model(vector.unsqueeze(0).cuda())
        else:
            output = model(vector.unsqueeze(0))

        output_argmax = output.detach().permute(1, 0, 2).argmax(dim=-1)[0]
        labels, label_ids = self.decode(output_argmax)
        prob = self.caculate_prob(output, output_argmax, label_ids)

        return labels, prob

    def decode(self, sequence):
        characters = self.characters

        a = ''.join([characters[x] for x in sequence])
        s = ''
        h = []
        for j, x in enumerate(a[:-1]):
            if x != characters[0] and x != a[j+1]:
                s += x
                h.append(j)

        if a[-1] != characters[0]:
            if (s[-1] != a[-1]) or (len(s) == self.n_label_length - 1):
                s += a[-1]
                h.append(len(a)-1)

        return s, h

    def caculate_prob(self, output, output_argmax, label_ids):
        if len(label_ids) < 1:
            return 0.0

        prob_sent = 1.0
        for i in label_ids:
            scores = output[i][0].tolist()
            probs = softmax(scores)
            prob_max = probs[output_argmax[i]]
            prob_sent = prob_max * prob_sent

        return prob_sent
