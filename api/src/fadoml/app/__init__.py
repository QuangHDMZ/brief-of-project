import logging
from bpemb import BPEmb
import pyonmttok
from fadoml.app.singleapp import SingleApp
from fadoml.app.util import captcha_recognizer, captcha_vectorizer, categorizer, hscode_classifier, oc_recognizer, oc_vectorizer, product_checker
from fadoml.app.util.accent_translator import AccentTranslator
from fadoml.util.constant import BERT_BASE_UNCASED, GROUPS_HSCODE_MAX_LEN, LABELS_FRONT_SIDE_ID_CARD, MODEL_NAMES_CAPTCHA, N_LABELS_GROUP3, SEQ_KERAS, SEQ_TRANSFORMER, USER_INFO_FRONT_SIDE_ID_CARD
from fadoml.util.helper import exist_file, exist_dir, get_stopword
from fadoml.util.rwutil import read_json, read_vietocr, read_yolo5,load_model_keras
import concurrent.futures
import keras
from fadoml.app.util.fprint_vectorizer import FPrintVectorizer
import json
import spacy
import pytextrank
import stopwordsiso as stopwords
from pysummarization.nlpbase.auto_abstractor import AutoAbstractor
from pysummarization.tokenizabledoc.simple_tokenizer import SimpleTokenizer
from pysummarization.tokenizabledoc.mecab_tokenizer import MeCabTokenizer

__version__ = "1.1.0"
__author__ = "DevTeam"

def load_japanese_summarization():
    auto_abstractor = AutoAbstractor()
    auto_abstractor.tokenizable_doc = MeCabTokenizer()
    auto_abstractor.delimiter_list = ["。", "\n"]
    return auto_abstractor

def load_english_summarization():
    auto_abstractor = AutoAbstractor()
    auto_abstractor.tokenizable_doc = SimpleTokenizer()
    auto_abstractor.delimiter_list = [".", "\n"]
    return auto_abstractor

def load_stopwords(iso2Code, dataPath):
    if stopwords.has_lang(iso2Code) == True:
        return stopwords.stopwords(iso2Code)
    
    stopwordPath = "{}/stopwords/{}.txt".format(dataPath, iso2Code)
    return get_stopword(stopwordPath)

def load_textrank(modelName):
    nlp = spacy.load(modelName)
    nlp.add_pipe("textrank")
    return nlp

def load_bpe_tokenizer(iso2Code, dataPath, vsNum=200000, dimNum=300):
    cacheDir = "{}/tokenizer".format(dataPath)
    return BPEmb(cache_dir=cacheDir, lang=iso2Code, vs=vsNum, dim=dimNum)

def load_nmt_wiki_tokenizer(iso2Code, dataPath, vsNum=200000, dimNum=300, modeStr="none", typeNum=0):
    bpePath = "{}/tokenizer/{}/{}.wiki.bpe.vs{}.model".format(dataPath, iso2Code, iso2Code, vsNum)
    if exist_file(bpePath) == False:
        return None

    vocPath = "{}/tokenizer/{}/{}.wiki.bpe.vs{}.vocab".format(dataPath, iso2Code, iso2Code, vsNum)
    if exist_file(vocPath) == False:
        return None

    langCode = None
    if len(iso2Code) == 2:
        langCode = iso2Code
    
    if typeNum > 0:
        return pyonmttok.Tokenizer(
            mode=modeStr,
            lang=langCode,
            bpe_model_path=bpePath,
            vocabulary_path=vocPath,
        )
    
    return pyonmttok.SentencePieceTokenizer(bpePath)

def load_ban_checker(iso2Code, dataPath):
    text_checker_path = "{}/{}_ban_checker/ban_text_model.pkl".format(
        dataPath, iso2Code)
        
    image_checker_path = "{}/{}_ban_checker/ban_image_model.h5".format(
        dataPath, iso2Code)
    
    if (exist_file(text_checker_path) == False) or (exist_file(image_checker_path) == False):
        return None

    checker = product_checker.ProductChecker()
    text_checker = checker.load_ban_text(text_checker_path)
    image_checker = checker.load_ban_image(image_checker_path)
    if (text_checker is None) or (image_checker is None):
        return None

    checkers = {
        'text': text_checker,
        'image': image_checker,
        'checker': checker
    }
    return checkers


def load_categorizer(dataPath, iso2code, nmt_language_supported):
    cate_dir = "{}/categorizer".format(dataPath)
    if exist_dir(cate_dir) == False:
        logging.warning('No exist model for categorizer')
        return None

    if iso2code not in nmt_language_supported:
        logging.warning('No exist iso2code model for categorizer')
        return None
    
    nmt_info = nmt_language_supported[iso2code]
    tokenizer = load_nmt_wiki_tokenizer(iso2code, dataPath, nmt_info["vs"], nmt_info["dim"], nmt_info["mode"])
    if tokenizer is None:
        logging.warning('No exist tokenizer for categorizer')
        return None

    cate = categorizer.Categorizer(tokenizer)
    groups = {}

    # Load parent group model
    group_by = 'parent_node_id'
    group_name = 'parent_group'
    group_path = "{}/{}".format(cate_dir, group_name)
    groups[group_name] = cate.load_model_by_group(group_path, group_by)

    # Load child group models
    group_by = 'node_id'
    n_child_groups = 84
    for group_idx in range(n_child_groups):
        group_name = 'group{}'.format(group_idx)
        group_path = "{}/{}".format(cate_dir, group_name)
        groups[group_name] = cate.load_model_by_group(group_path, group_by)

    return groups


def load_ocr(orcPath, haveGpu):
    ocr = oc_recognizer.OCRecognizer(orcPath, haveGpu)
    ocv = oc_vectorizer.OCVectorizer(orcPath, haveGpu)

    return (ocr, ocv)


def load_vn_accent(dataPath, haveCuda):
    vn_accent_path = "{}/vn_accent".format(dataPath)

    vocab_path = "{}/tokenizer.h5".format(vn_accent_path)
    config_file = "{}/model_config.json".format(vn_accent_path)
    model_file = "{}/epoch_03.h5".format(vn_accent_path)
    model_name = "transformer_evolved"

    accent_translator = AccentTranslator()
    accent_translator.set_config(vocab_path, config_file,
                                 model_file, model_name, haveCuda)

    return accent_translator


def load_hscoder(dataPath):
    label_mapping_path = "{}/hscode/label_mapping.json".format(dataPath)
    hscode_info_path = "{}/hscode/hscode.json".format(dataPath)

    # Load extra info
    label_mapping = read_json(label_mapping_path)
    hscode_info = read_json(hscode_info_path)
    hscoder = hscode_classifier.HScodeClassifier(label_mapping, hscode_info)

    # Load classifier
    model_dir = "{}/hscode/model".format(dataPath)
    tokenizer_dir = "{}/hscode/tokenizer".format(dataPath)

    for group_name in GROUPS_HSCODE_MAX_LEN.keys():
        config = {
            "max_length": GROUPS_HSCODE_MAX_LEN[group_name],
            "name": group_name,
        }

        if group_name == "group3":
            model_path = "{}/model_{}".format(model_dir, group_name)
            config.update({
                "model_path": model_path,
                "n_labels": N_LABELS_GROUP3,
                "from_pretrained": BERT_BASE_UNCASED,
                "type": SEQ_TRANSFORMER,
            })
        else:
            model_path = "{}/model_{}.h5".format(model_dir, group_name)
            tokenizer_path = "{}/tokenizer_{}.pickle".format(tokenizer_dir, group_name)
            config.update({
                "model_path": model_path,
                "tokeniker_path": tokenizer_path,
                "type": SEQ_KERAS
            })

        hscoder.set_classifier(config)

    return hscoder

def load_captcha(data_path, have_cuda):
    captcha_reg = captcha_recognizer.CaptchaRecognizer(have_cuda)

    for model_name in MODEL_NAMES_CAPTCHA:
        model_path = "{}/captcha/{}.pth".format(data_path, model_name)
        captcha_reg.set_model(model_path, model_name)
    
    captcha_vec = captcha_vectorizer.CaptchaVectorizer()
    return (captcha_reg, captcha_vec)

def load_id_card(data_path, have_cuda):

    fp_classifier = keras.models.load_model("{}/finger_print/17022022_classify_fprint_xception299.h5".format(data_path))
    fp_verifier = keras.models.load_model("{}/finger_print/08022022_verify_fprint_binary.h5".format(data_path))
    vectorizer = FPrintVectorizer()

    align_model_path = "{}/id_card/align_idcard.pt".format(data_path)
    binary_model_path = "{}/id_card/binary_idcard.h5".format(data_path)
    detect_fprint_model_path = "{}/id_card/detect_fprint_idcard.pt".format(data_path)
    detect_frontside_model_path = "{}/id_card/detect_frontside_idcard.pt".format(data_path)
    detect_frontside_new_model_path = "{}/id_card/detect_frontside_idcard_new.pt".format(data_path)

    weight_vietocr_path = "{}/id_card/seq2seq_ocr.pth".format(data_path)
    config_model_vietocr_path = "{}/id_card/vgg-seq2seq.yml".format(data_path)
    config_base_vietocr_path = "{}/id_card/base.yml".format(data_path)

    yolo_type = "cpu"
    if have_cuda == True:
        yolo_type = "gpu"

    align_model = read_yolo5(align_model_path, device_type=yolo_type)
    binary_model = load_model_keras(binary_model_path)
    detect_fprint_model = read_yolo5(detect_fprint_model_path)
    detect_frontside_model = read_yolo5(detect_frontside_model_path)
    detect_frontside_model_new = read_yolo5(detect_frontside_new_model_path)
    vietocr = read_vietocr(weight_vietocr_path, config_model_vietocr_path, config_base_vietocr_path)

    id_card = {
        "fp_classifier" :fp_classifier,
        "fp_verifier":fp_verifier,
        "vectorizer":vectorizer,
        "align": align_model,
        "binary" : binary_model,
        "fprint": detect_fprint_model,
        "frontside": detect_frontside_model,
        "frontside_new": detect_frontside_model_new,
        'vietocr': vietocr,
    }
    
    return id_card

def create_threadpool(n_threads):
    return concurrent.futures.ThreadPoolExecutor(max_workers=n_threads)

def load_proxies(data_path):
    json_file_path = "{}/proxies.json".format(data_path)
    proxies = []
    with open(json_file_path) as json_file:
        proxies = json.loads(json_file.read())
        json_file.close()
    return proxies

__all__ = [
    'load_nmt_wiki_tokenizer',
    'load_ban_checker',
    'load_categorizer',
    'load_hscoder',
    'load_ocr',
    'load_vn_accent',
    'load_captcha',
    'load_id_card',
    'runtime',
    'util',
    'create_threadpool',
    'SingleApp',
    'load_proxies',
    'load_textrank',
    'load_japanese_summarization',
    'load_english_summarization'
]
