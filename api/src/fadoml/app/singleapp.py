import torch
import torch.multiprocessing as mp
import logging
import eventlet
import gevent
from fadoml.util import helper

class SingleApp(mp.Process):
    def __init__(self, app_single, port, host="127.0.0.1"):
        mp.Process.__init__(self)
        self.app_single = app_single
        self.host = host
        self.port = port

    def run_default(self, threadedFlg=False):
        self.app_single.run(host=self.host, port=self.port, threaded=threadedFlg)

    def run_eventlet(self):
        eventlet_socket = eventlet.listen((self.host, self.port))
        eventlet.wsgi.server(eventlet_socket, self.app_single)

    def run_gevent(self):
        environ = {
            'wsgi.multithread': False,
            'SERVER_NAME': 'fado'
        }
        http_server = gevent.pywsgi.WSGIServer((self.host, self.port), application=self.app_single, environ=environ)
        http_server.serve_forever()

    def run(self):
        helper.patch_cuda(True)
        helper.patch_gpus(True)

        app_name = __name__
        if "app_name" in self.app_single.config:
            app_name = self.app_single.config["app_name"]

        if "trigger" in self.app_single.config:
            for (key_name, func_name) in self.app_single.config["trigger"]:
                self.app_single.config[key_name] = func_name()
        
        logging.info("Running APP [{}] at {}:{}".format(app_name, self.host, self.port))
        threadedFlg = False
        if "threaded" in self.app_single.config:
            threadedFlg = self.app_single.config["threaded"]
        self.run_default(threadedFlg)

def start_app_thread(app_single, port, host="127.0.0.1"):
    app_thread = SingleApp(app_single, port, host)
    app_thread.start()
    return app_thread

def stop_app_thread(app_thread):
    app_thread.join()