import sys
from sanic import response
from fadoml.util.constant import *
from fadoml.util.helper import put_translate_tracking, \
    build_text, is_none_param, \
    build_fasttext_text, word_tokenizer
import logging
import numpy as np
import time
import hashlib
import traceback
from fadoml.api import http_internal_client
from langdetect import detect_langs
import concurrent.futures

async def build_json_response(msgBody, statusCode = 200):
    headers = {
        "Content-Type": "application/json; charset=utf-8",
        "Server": "fado"
    }
    return response.json(msgBody, status=statusCode, headers=headers, content_type=headers["Content-Type"])

def detect_glanguage(text):
    text = build_fasttext_text(text)

    labels = detect_langs(text)
    if labels == None:
        return (ENGLISH_ISO2CODE, 1.0)

    if len(labels) == 0:
        return (ENGLISH_ISO2CODE, 1.0)

    if isinstance(labels, list):
        lang_code = labels[0].lang
        prob_num = round(labels[0].prob, DECIMAL_NUMBER)
    else:
        lang_code = labels.lang
        prob_num = round(labels.prob, DECIMAL_NUMBER)

    return (lang_code, prob_num)

def detect_language(request, text):
    text = build_fasttext_text(text)

    if request.app.config["fast_text"] is not None:
        try:
            result = request.app.config["fast_text"].predict(text)
            if len(result) == 0:
                return (ENGLISH_ISO2CODE, 1.0)
            
            if isinstance(result[0][0], list):
                lang_code = result[0][0][0].replace("__label__", "")
                prob_num = round(result[1][0][0], DECIMAL_NUMBER)
            else:
                lang_code = result[0][0].replace("__label__", "")
                prob_num = round(result[1][0], DECIMAL_NUMBER)
            
            return (lang_code, prob_num)
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass

    return (ENGLISH_ISO2CODE, 1.0)

def detect_language_fastlangid(request, text):
    text = build_fasttext_text(text)

    if request.app.config["fastlangid"] is not None and is_none_param(text) == False:
        try:
            result = request.app.config["fastlangid"].predict(text, prob=True, force_second=False)
            if len(result) == 0:
                return (ENGLISH_ISO2CODE, 1.0)
            
            if isinstance(result, list):
                lang_code = result[0][0].replace("__label__", "")
                prob_num = round(result[0][1], DECIMAL_NUMBER)
                return (lang_code, prob_num)
            elif isinstance(result, tuple):
                lang_code = result[0].replace("__label__", "")
                prob_num = round(result[1], DECIMAL_NUMBER)
                return (lang_code, prob_num)
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass

    return (ENGLISH_ISO2CODE, 1.0)

def detect_language_gcld3(request, text):
    text = build_fasttext_text(text)

    if request.app.config["gcld3"] is not None:
        try:
            result = request.app.config["gcld3"].FindLanguage(text=text)
            lang_code = result.language.replace("__label__", "")
            prob_num = round(result.probability, DECIMAL_NUMBER)
            return (lang_code, prob_num)
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass

    return (ENGLISH_ISO2CODE, 1.0)

def detect_language_compare(request, text, shuffle = False):
    lang1From, prob1 = detect_language(request, text)
    if shuffle == True:
        lang2From, prob2 = detect_language_gcld3(request, text)
        if prob2 < PROB_THREAD_HOLD:
            lang2From, prob2 = detect_glanguage(text)
        if (prob1 < prob2) and (len(lang2From) == 2):
            return (lang2From, prob2)
    return (lang1From, prob1)

def detect_languages(request, text):
    text = build_fasttext_text(text)
    
    data = []
    if request.app.config["fast_text"] is not None:
        try:
            label, prob = request.app.config["fast_text"].predict(text, k = 3)
            if len(label) == 0:
                data.append((ENGLISH_ISO2CODE, 1.0))
                return data
            i = 0
            for label_name in label:
                lang_code = label_name.replace("__label__", "")
                prob_num = round(prob[i], DECIMAL_NUMBER)
                data.append((lang_code, prob_num))
                i = i + 1
            sorted(data, key=lambda item: item[1])
            return data
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
    
    data.append((ENGLISH_ISO2CODE, 1.0))
    return data

def detect_glanguages(text):
    text = build_text(text)
    data = []

    try: 
        labels = detect_langs(text)
        if labels == None:
            data.append((ENGLISH_ISO2CODE, 1.0))
            return data

        if len(labels) == 0:
            data.append((ENGLISH_ISO2CODE, 1.0))
            return data

        for label in labels:
            prob_num = round(label.prob, DECIMAL_NUMBER)
            data.append((label.lang, prob_num))
    except:
        data.append((ENGLISH_ISO2CODE, 1.0))
        return data

    return data

def translate_tracking(request, langTo, langFrom, textStr, translatedTextStr, translatedTextProb):
    try:
        if request.app.config["beanstalkd"] is not None:
            textField = 'text_{}'.format(langTo)
            textFieldProb = 'text_{}_prob'.format(langTo)
            put_translate_tracking(request.app.config["beanstalkd"], {
                "lang_from": langFrom,
                "lang_to": langTo,
                "text": textStr,
                textField: translatedTextStr,
                textFieldProb: translatedTextProb,
                "md5_text": hashlib.md5(textStr.encode('utf-8')).hexdigest(),
                "created_date": int(time.time()),
            })

        if request.app.config["tracking_translator"] is not None:
            request.app.config["tracking_translator"].put(textStr, langFrom, langTo, translatedTextStr, translatedTextProb)
    except:
        logging.error(traceback.format_tb(sys.exc_info()[2]))

def check_mapping_product(request, lang, text='', ridStr=None):
    if "mapping_port" not in request.app.config:
        return None

    params = {
        "text": text,
        "lang": lang,
    }
    if ridStr is not None:
        params['rid'] = ridStr
    
    try:
        resp = http_internal_client.post(
            request.app.config["mapping_port"],
            "/api/predict",
            params
        )
        
        if (resp is None) or (resp["code"] > 0):
            return None
        return resp["msg"]
    except:
        logging.error(traceback.format_tb(sys.exc_info()[2]))
        pass
    
    return None

def check_nmt_translator(request, modelID, textStr, ridStr=None):
    if "nmt_translator_port" not in request.app.config:
        return None
    
    params = {
        'id': modelID,
        'src': textStr
    }
    if ridStr is not None:
        params['rid'] = ridStr

    try:
        resp = http_internal_client.post(
            request.app.config["nmt_translator_port"],
            "/api/predict",
            params
        )
        
        if (resp is None) or (resp["code"] > 0):
            return None
        return resp["msg"]
    except:
        logging.error(traceback.format_tb(sys.exc_info()[2]))
        pass
    
    return None

def check_ocr(request, img_file, lang, min_prob, rotate, ridStr=None):
    if "ocr_port" not in request.app.config:
        return None

    params = {
        'img': img_file,
        'lang': lang,
        'min_prob': min_prob,
        'rotate': rotate,
    }
    if ridStr is not None:
        params['rid'] = ridStr
    
    try:
        resp = http_internal_client.post(
            request.app.config["ocr_port"],
            "/api/predict",
            params
        )
        
        if (resp is None) or (resp["code"] > 0):
            return None
        return resp["msg"]
    except:
        logging.error(traceback.format_tb(sys.exc_info()[2]))
        pass
    
    return None


def check_vn_accent(request, text, ridStr=None):
    if "vn_accent_port" not in request.app.config:
        return None

    params = {
        'text': text,
    }
    if ridStr is not None:
        params['rid'] = ridStr
    
    try:
        resp = http_internal_client.post(
            request.app.config["vn_accent_port"],
            "/api/predict",
            params
        )
        
        if (resp is None) or (resp["code"] > 0):
            return None
        return resp["msg"]
    except:
        logging.error(traceback.format_tb(sys.exc_info()[2]))
        pass
    
    return None
    

def check_ban_product(request, lang, text, image, text_ocr, ridStr=None):
    if "checker_port" not in request.app.config:
        return None
    
    params = {
        "lang": lang
    }

    if is_none_param(text) == False:
        params["text"] = text

    if is_none_param(image) == False:
        params["image"] = image
    
    if is_none_param(text_ocr) == False:
        params["text_ocr"] = text_ocr

    if ridStr is not None:
        params['rid'] = ridStr

    try:
        resp = http_internal_client.post(
            request.app.config["checker_port"],
            "/api/predict",
            params
        )
        
        if (resp is None) or (resp["code"] > 0):
            return None
        
        no_arr = []
        yes_arr = []
        for label in ["image", "text"]:
            if label in resp["msg"]["data"]:
                if "no" in resp["msg"]["data"][label]:
                    no_arr.append(resp["msg"]["data"][label]["no"])
                
                if "yes" in resp["msg"]["data"][label]:
                    yes_arr.append(resp["msg"]["data"][label]["yes"])

        yes_max = max(yes_arr)
        no_max = max(no_arr)
        resp["msg"]["data"]["max"] = {
            "no": no_max,
            "yes": yes_max
        }

        """
        prohibited = 1 : khong cam
        prohibited = 2 : hang cam
        prohibited =  3 : co the la hang cam
        """

        prohibited = 1
        if yes_max >= 0.7:
            prohibited = 2
        elif yes_max < 0.5:
            prohibited = 1
        else:
            prohibited = 3
        resp["msg"]["data"]["prohibited"] = prohibited

        return resp["msg"]
    except:
        logging.error(traceback.format_tb(sys.exc_info()[2]))
        pass
    
    return None

def check_hscode(request, lang, text, ridStr=None):
    text = build_text(text)
    if "hscode_port" not in request.app.config:
        return None

    params = {
        "text": text,
        "lang": lang,
    }

    if ridStr is not None:
        params['rid'] = ridStr
    
    try:
        resp = http_internal_client.post(
            request.app.config["hscode_port"],
            "/api/predict",
            params
        )
        
        if (resp is None) or (resp["code"] > 0):
            return None
        return resp["msg"]
    except:
        logging.error(traceback.format_tb(sys.exc_info()[2]))
        pass
    
    return None

def check_captcha(request, img_file, pattern, ridStr=None):
    if "captcha_port" not in request.app.config:
        return None

    params = {
        'pattern' : pattern,
        'img': img_file
    }

    if ridStr is not None:
        params['rid'] = ridStr
    
    try:
        resp = http_internal_client.post(
            request.app.config["captcha_port"],
            "/api/predict",
            params
        )

        if (resp is None) or (resp["code"] > 0):
            return None
        return resp["msg"]
    except:
        logging.error(traceback.format_tb(sys.exc_info()[2]))
        pass

    return None

def check_idcard_fingerprint(request, imgBase64=None, classify=None, ridStr=None):
    if "id_card_port" not in request.app.config:
        return None

    params = {}

    if imgBase64 is not None:
        params['imgbase64'] = imgBase64

    if classify is not None:
        params['classify'] = classify

    if ridStr is not None:
        params['rid'] = ridStr
    
    try:
        resp = http_internal_client.post(
            request.app.config["id_card_port"],
            "/api/vn/fingerprint",
            params
        )

        if (resp is None) or (resp["code"] > 0):
            return None
        return resp["msg"]
    except:
        logging.error(traceback.format_tb(sys.exc_info()[2]))
        pass

    return None

def check_idcard_ocr(request, imgBase64=None, ridStr=None):
    if "id_card_port" not in request.app.config:
        return None

    params = {}

    if imgBase64 is not None:
        params['imgbase64'] = imgBase64

    if ridStr is not None:
        params['rid'] = ridStr
    
    try:
        resp = http_internal_client.post(
            request.app.config["id_card_port"],
            "/api/vn/ocr",
            params
        )

        if (resp is None) or (resp["code"] > 0):
            return None
        return resp["msg"]
    except:
        logging.error(traceback.format_tb(sys.exc_info()[2]))
        pass

    return None

def check_idcard_vn_avatar(request, imgBase64=None, ridStr=None):
    if "id_card_port" not in request.app.config:
        return None

    params = {}

    if imgBase64 is not None:
        params['imgbase64'] = imgBase64

    if ridStr is not None:
        params['rid'] = ridStr
    
    try:
        resp = http_internal_client.post(
            request.app.config["id_card_port"],
            "/api/vn/avatar",
            params
        )

        if (resp is None) or (resp["code"] > 0):
            return None
        return resp["msg"]
    except:
        logging.error(traceback.format_tb(sys.exc_info()[2]))
        pass

    return None

def create_internal_http_request(port, uri, params, method = POST_METHOD):
    try:
        resp = None
        if method == POST_METHOD:
            resp = http_internal_client.post(port, uri, params)
        elif method == GET_METHOD:
            resp = http_internal_client.get(port, uri, params)

        if (resp is None) or (resp["code"] > 0):
            return None
        return resp
    except:
        logging.error(traceback.format_tb(sys.exc_info()[2]))
        pass

    return None

def check_translator_result(request, textOut, langTo = None):
    if (textOut is None) or (len(textOut) == 0):
        return (False, 0.0)

    lang_code, prob = detect_language_compare(request, textOut, shuffle=True)
    if (lang_code == langTo) and (prob >= PROB_THREAD_HOLD):
        return (True, prob)
    return (False, prob)

def mltranslator(request, textStr, langFrom = None, arrLangTo = None, tagging=0, arr_stopwords=[]):
    if (langFrom is None) or (len(langFrom) == 0):
        langFrom = 'auto'

    resp = {}
    if (arrLangTo is None) or (len(arrLangTo) == 0):
        return resp

    future_translator = {request.app.config["threadpool"].submit(translator, request, textStr, langFrom, lang, tagging, arr_stopwords): lang for lang in arrLangTo}
    for future in concurrent.futures.as_completed(future_translator):
        lang = future_translator[future]
        try:
            resp[lang] = future.result()
        except:
            pass
    
    return resp

def get_tagging(request, iso2Code, textStr, arr_stopwords=[]):
    arrToken = word_tokenizer(
        iso2Code, textStr,
        request.app.config["cococ_tokenizer"], 
        request.app.config["bpe_tokenizer"], 
        request.app.config["nmt_tokenizer"],
        request.app.config["spacy_textrank"]
    )

    arrTokenFinal = []
    if arrToken is not None:
        if len(arr_stopwords) > 0:
            arrTokenFinal = [word.strip() for word in arrToken if ((not word in arr_stopwords) and word.strip())]
        else:
            arrTokenFinal = arrToken
    return arrTokenFinal

def translator(request, textStr, langFrom = None, langTo = None, tagging=0, arr_stopwords=[], more=False):
    if (langFrom is None) or (len(langFrom) == 0):
        langFrom = 'auto'

    if (langTo is None) or (len(langTo) == 0):
        langTo = ENGLISH_ISO2CODE

    arrTagging = []
    if langFrom == langTo:
        if tagging > 0:
            arrTagging = get_tagging(request, langTo, textStr, arr_stopwords)
        return (textStr, 1.0, 0, arrTagging)
    
    type_id = 0
    arr_data = []
    #logging.info("Translate [{}] , from {} => {}".format(textStr, langFrom, langTo))

    ### If request is the same translate language
    status, prob = check_translator_result(request, textStr, langTo)
    if status == True:
        if tagging > 0:
            arrTagging = get_tagging(request, langTo, textStr, arr_stopwords)
        return (textStr, prob, type_id, arrTagging)

    ### Retry translate language
    try:
        outStr = request.app.config["open_translator"].translate_from_google(textStr, langFrom, langTo)
        status, prob = check_translator_result(request, outStr, langTo)
        type_id = type_id + 1
        if outStr is not None:
            arr_data.append((outStr, prob, type_id))
        if status == True:
            if tagging > 0:
                arrTagging = get_tagging(request, langTo, outStr, arr_stopwords)
            return (outStr, prob, type_id, arrTagging)

        outStr = request.app.config["open_translator"].translate_from_deep_goole(textStr, langFrom, langTo)
        status, prob = check_translator_result(request, outStr, langTo)
        type_id = type_id + 1
        if outStr is not None:
            arr_data.append((outStr, prob, type_id))
        if status == True:
            if tagging > 0:
                arrTagging = get_tagging(request, langTo, outStr, arr_stopwords)
            return (outStr, prob, type_id, arrTagging)
        
        outStr = request.app.config["open_translator"].translate_from_ulion_google(textStr, langFrom, langTo)
        status, prob = check_translator_result(request, outStr, langTo)
        type_id = type_id + 1
        if outStr is not None:
            arr_data.append((outStr, prob, type_id))
        if status == True:
            if tagging > 0:
                arrTagging = get_tagging(request, langTo, outStr, arr_stopwords)
            return (outStr, prob, type_id, arrTagging)

        outStr = request.app.config["open_translator"].translate_from_ulion_bing(textStr, langFrom, langTo)
        status, prob = check_translator_result(request, outStr, langTo)
        type_id = type_id + 1
        if outStr is not None:
            arr_data.append((outStr, prob, type_id))
        if status == True:
            if tagging > 0:
                arrTagging = get_tagging(request, langTo, outStr, arr_stopwords)
            return (outStr, prob, type_id, arrTagging)

        outStr = request.app.config["open_translator"].translate_from_ulion_yandex(textStr, langFrom, langTo)
        status, prob = check_translator_result(request, outStr, langTo)
        type_id = type_id + 1
        if outStr is not None:
            arr_data.append((outStr, prob, type_id))
        if status == True:
            if tagging > 0:
                arrTagging = get_tagging(request, langTo, outStr, arr_stopwords)
            return (outStr, prob, type_id, arrTagging)

        outStr = request.app.config["open_translator"].translate_from_ulion_iciba(textStr, langFrom, langTo)
        status, prob = check_translator_result(request, outStr, langTo)
        type_id = type_id + 1
        if outStr is not None:
            arr_data.append((outStr, prob, type_id))
        if status == True:
            if tagging > 0:
                arrTagging = get_tagging(request, langTo, outStr, arr_stopwords)
            return (outStr, prob, type_id, arrTagging)

        outStr = request.app.config["open_translator"].translate_from_ulion_sogou(textStr, langFrom, langTo)
        status, prob = check_translator_result(request, outStr, langTo)
        type_id = type_id + 1
        if outStr is not None:
            arr_data.append((outStr, prob, type_id))
        if status == True:
            if tagging > 0:
                arrTagging = get_tagging(request, langTo, outStr, arr_stopwords)
            return (outStr, prob, type_id, arrTagging)
        
        outStr = request.app.config["open_translator"].translate_from_ulion_alibaba(textStr, langFrom, langTo)
        status, prob = check_translator_result(request, outStr, langTo)
        type_id = type_id + 1
        if outStr is not None:
            arr_data.append((outStr, prob, type_id))
        if status == True:
            if tagging > 0:
                arrTagging = get_tagging(request, langTo, outStr, arr_stopwords)
            return (outStr, prob, type_id, arrTagging)

        outStr = request.app.config["open_translator"].translate_from_ulion_youdao(textStr, langFrom, langTo)
        status, prob = check_translator_result(request, outStr, langTo)
        type_id = type_id + 1
        if outStr is not None:
            arr_data.append((outStr, prob, type_id))
        if status == True:
            if tagging > 0:
                arrTagging = get_tagging(request, langTo, outStr, arr_stopwords)
            return (outStr, prob, type_id, arrTagging)
        
        outStr = request.app.config["open_translator"].translate_from_ulion_baidu(textStr, langFrom, langTo)
        status, prob = check_translator_result(request, outStr, langTo)
        type_id = type_id + 1
        if outStr is not None:
            arr_data.append((outStr, prob, type_id))
        if status == True:
            if tagging > 0:
                arrTagging = get_tagging(request, langTo, outStr, arr_stopwords)
            return (outStr, prob, type_id, arrTagging)
        
        # Need more translation
        if more == True:
            outStr = request.app.config["open_translator"].translate_from_deepl(textStr, langFrom, langTo)
            status, prob = check_translator_result(request, outStr, langTo)
            type_id = type_id + 1
            if outStr is not None:
                arr_data.append((outStr, prob, type_id))
            if status == True:
                if tagging > 0:
                    arrTagging = get_tagging(request, langTo, outStr, arr_stopwords)
                return (outStr, prob, type_id, arrTagging)
            
            outStr = request.app.config["open_translator"].translate_from_libretranslate(textStr, langFrom, langTo)
            status, prob = check_translator_result(request, outStr, langTo)
            type_id = type_id + 1
            if outStr is not None:
                arr_data.append((outStr, prob, type_id))
            if status == True:
                if tagging > 0:
                    arrTagging = get_tagging(request, langTo, outStr, arr_stopwords)
                return (outStr, prob, type_id, arrTagging)

            outStr = request.app.config["open_translator"].translate_from_deep_mymemory(textStr, langFrom, langTo)
            status, prob = check_translator_result(request, outStr, langTo)
            type_id = type_id + 1
            if outStr is not None:
                arr_data.append((outStr, prob, type_id))
            if status == True:
                if tagging > 0:
                    arrTagging = get_tagging(request, langTo, outStr, arr_stopwords)
                return (outStr, prob, type_id, arrTagging)
    except:
        logging.error(traceback.format_tb(sys.exc_info()[2]))
        pass

    if len(arr_data) > 0:
        sorted(arr_data, key=lambda item: item[1])
        outStr, prob, type_id = arr_data[0]
        if (outStr is None) or (len(outStr) == 0):
            outStr = textStr
        
        if tagging > 0:
            arrTagging = get_tagging(request, langTo, outStr, arr_stopwords)
        return (outStr, prob, type_id)

    if tagging > 0:
        arrTagging = get_tagging(request, langTo, textStr, arr_stopwords)
    return (textStr, 0.0, 0, arrTagging)

def htranslator(request, textStr, langTo = None, threadpool = None):
    if (langTo is None) or (len(langTo) == 0):
        langTo = ENGLISH_ISO2CODE
    
    type_id = 0
    #logging.info("HTranslate [{}] ({})".format(textStr, langTo))

    ### Retry translate language
    try:
        outStr = request.app.config["open_translator"].translate_html_from_ulion_google(textStr, langTo, threadpool=threadpool)
        type_id = type_id + 1
        if outStr is not None:
            return (outStr, type_id)

        outStr = request.app.config["open_translator"].translate_html_from_ulion_bing(textStr, langTo, threadpool=threadpool)
        type_id = type_id + 1
        if outStr is not None:
            return (outStr, type_id)
        
        outStr = request.app.config["open_translator"].translate_html_from_ulion_yandex(textStr, langTo, threadpool=threadpool)
        type_id = type_id + 1
        if outStr is not None:
            return (outStr, type_id)

        outStr = request.app.config["open_translator"].translate_html_from_ulion_iciba(textStr, langTo, threadpool=threadpool)
        type_id = type_id + 1
        if outStr is not None:
            return (outStr, type_id)

        outStr = request.app.config["open_translator"].translate_html_from_ulion_alibaba(textStr, langTo, threadpool=threadpool)
        type_id = type_id + 1
        if outStr is not None:
            return (outStr, type_id)
    except:
        logging.error(traceback.format_tb(sys.exc_info()[2]))
        pass

    return (textStr, 0)