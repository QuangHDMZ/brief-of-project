from sanic import Blueprint
from fadoml.util.constant import *
from fadoml.msg import error_msg, response_msg
from fadoml.util.helper import build_text, is_none_param, \
    string_to_int, word_tokenizer, get_punctuation_list, \
    summarizer
from fadoml.api.helper import *

app_name = __name__.replace(".", "_")
bp = Blueprint(app_name, url_prefix="/api")

@bp.route("/detect", methods=("GET", "POST"))
async def detect(request):
    textStr = request.args.get("text")
    apiKeyStr = request.args.get("apikey")
    ridStr = request.args.get("rid")
    if request.method == "POST":
        textStr = request.form.get("text")
        ridStr = request.form.get("rid")
        apiKeyStr = request.form.get("apikey")

    if apiKeyStr not in request.app.config["apikeys"]:
        errorMsg = error_msg.ErrorMsg(1, "Invalid apikey : {}".format(apiKeyStr))
        return await build_json_response(errorMsg.toJSON())

    textStr = build_text(textStr)
    if is_none_param(textStr) == True:
        errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
        return await build_json_response(errorMsg.toJSON())
    
    dataMsg = response_msg.TranslateMsg(0, detect_languages(request, textStr))
    extras_data = {
        "c1" : detect_glanguage(textStr),
        "c2": detect_language_gcld3(request, textStr),
        "c3": detect_language_fastlangid(request, textStr),
        "c4": detect_language_compare(request, textStr, shuffle=True)
    }
    if is_none_param(ridStr) == False:
        extras_data["rid"] = ridStr
    dataMsg.set_extras(extras_data)
    
    return await build_json_response(dataMsg.toJSON())

@bp.route("/tokenizer_wtsw", methods=("GET", "POST"))
async def tokenizer(request):
    iso2Code = request.args.get("lang")
    textStr = request.args.get("text")
    apiKeyStr = request.args.get("apikey")
    punctuation = request.args.get("punctuation")
    ridStr = request.args.get("rid")
    if request.method == "POST":
        ridStr = request.form.get("rid")
        iso2Code = request.form.get("lang")
        textStr = request.form.get("text")
        apiKeyStr = request.form.get("apikey")
        punctuation = request.form.get("punctuation")

    if apiKeyStr not in request.app.config["apikeys"]:
        errorMsg = error_msg.ErrorMsg(1, "Invalid apikey : {}".format(apiKeyStr))
        return await build_json_response(errorMsg.toJSON())

    textStr = build_text(textStr)
    if is_none_param(textStr) == True:
        errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
        return await build_json_response(errorMsg.toJSON())
    
    if (iso2Code is None) or (len(iso2Code) == 0) or (iso2Code == 'auto'):
        iso2Code, _ = detect_language_compare(request, textStr, shuffle=True)
    
    if is_none_param(punctuation) == True:
        punctuation = "0"
    punctuation = string_to_int(punctuation)

    arr_stopwords = set()
    if iso2Code in request.app.config["iso_stopwords"]:
        arr_stopwords = request.app.config["iso_stopwords"][iso2Code]
    
    if punctuation > 0:
        arr_stopwords = arr_stopwords | get_punctuation_list()

    arrToken = word_tokenizer(
        iso2Code, textStr, 
        request.app.config["cococ_tokenizer"], 
        request.app.config["bpe_tokenizer"], 
        request.app.config["nmt_tokenizer"],
        request.app.config["spacy_textrank"]
    )
    if arrToken is None:
        errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
        return await build_json_response(errorMsg.toJSON())
    
    if len(arr_stopwords) > 0:
        arrTokenFinal = [word.strip() for word in arrToken if ((not word in arr_stopwords) and word.strip())]
    else:
        arrTokenFinal = arrToken

    dataMsg = response_msg.TokenizerMsg(0, iso2Code, arrTokenFinal)
    if is_none_param(ridStr) == False:
        dataMsg.set_extras({
            "rid": ridStr
        })
    
    return await build_json_response(dataMsg.toJSON())

@bp.route("/tokenizer", methods=("GET", "POST"))
async def tokenizer(request):
    iso2Code = request.args.get("lang")
    textStr = request.args.get("text")
    apiKeyStr = request.args.get("apikey")
    ridStr = request.args.get("rid")
    if request.method == "POST":
        ridStr = request.form.get("rid")
        iso2Code = request.form.get("lang")
        textStr = request.form.get("text")
        apiKeyStr = request.form.get("apikey")

    if apiKeyStr not in request.app.config["apikeys"]:
        errorMsg = error_msg.ErrorMsg(1, "Invalid apikey : {}".format(apiKeyStr))
        return await build_json_response(errorMsg.toJSON())

    textStr = build_text(textStr)
    if is_none_param(textStr) == True:
        errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
        return await build_json_response(errorMsg.toJSON())
    
    if (iso2Code is None) or (len(iso2Code) == 0) or (iso2Code == 'auto'):
        iso2Code, _ = detect_language_compare(request, textStr, shuffle=True)

    arrToken = word_tokenizer(
        iso2Code, textStr, 
        request.app.config["cococ_tokenizer"], 
        request.app.config["bpe_tokenizer"],
        request.app.config["nmt_tokenizer"],
        request.app.config["spacy_textrank"]
    )
    if arrToken is None:
        errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
        return await build_json_response(errorMsg.toJSON())

    dataMsg = response_msg.TokenizerMsg(0, iso2Code, arrToken)
    if is_none_param(ridStr) == False:
        dataMsg.set_extras({
            "rid": ridStr
        })
    
    return await build_json_response(dataMsg.toJSON())

@bp.route("/translate", methods=("GET", "POST"))
async def translate(request):
    langFrom = request.args.get("lang_from")
    langTo = request.args.get("lang_to")
    textStr = request.args.get("text")
    apiKeyStr = request.args.get("apikey")
    tagging = request.args.get("tagging")
    ridStr = request.args.get("rid")
    if request.method == "POST":
        ridStr = request.form.get("rid")
        textStr = request.form.get("text")
        langFrom = request.form.get("lang_from")
        langTo = request.form.get("lang_to")
        tagging = request.form.get("tagging")
        apiKeyStr = request.form.get("apikey")
        
    if (langTo is None) or (len(langTo) == 0):
        langTo = ENGLISH_ISO2CODE
    
    if apiKeyStr not in request.app.config["apikeys"]:
        errorMsg = error_msg.ErrorMsg(1, "Invalid apikey : {}".format(apiKeyStr))
        return await build_json_response(errorMsg.toJSON())

    textStr = build_text(textStr)
    if is_none_param(textStr) == True:
        errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
        return await build_json_response(errorMsg.toJSON())
    
    if (langFrom is None) or (len(langFrom) == 0) or (langFrom == 'auto'):
        langFrom, _ = detect_language_compare(request, textStr, shuffle=True)

    if is_none_param(tagging) == True:
        tagging = "0"
    tagging = string_to_int(tagging)

    arr_stopwords = set()
    if tagging > 0:
        if langTo in request.app.config["iso_stopwords"]:
            arr_stopwords = request.app.config["iso_stopwords"][langTo]
        arr_stopwords = arr_stopwords | get_punctuation_list()

    dataMsg = response_msg.TranslateMsg(1, INTERNAL_ERROR_MSG)
    if is_none_param(ridStr) == False:
        dataMsg.set_extras({
            "rid": ridStr
        })

    if langFrom == langTo:
        errorMsg = error_msg.ErrorMsg(1, 'lang_from, lang_to is the same ({})'.format(langFrom))
        dataMsg.data = {
            "text": textStr,
            "prob": 1.0,
            "type": 1,
            "ext": errorMsg.msg,
            "from": langFrom,
            "to": langTo
        }
        dataMsg.code = 0

        # IF need tagging
        if tagging > 0:
            arrTagging = get_tagging(request, langTo, textStr, arr_stopwords)
            dataMsg.update_extras({
                "tags": arrTagging
            })

        return await build_json_response(dataMsg.toJSON())

    if request.app.config["tracking_translator"] is not None:
        json_data = request.app.config["tracking_translator"].get(textStr, langFrom, langTo)
        if json_data is not None:
            status, _ = check_translator_result(request, json_data["text"], langTo)
            if status == True:
                dataMsg.data = {
                    "text": json_data["text"],
                    "prob": json_data["prob"],
                    "type": -1,
                    "ext": "",
                    "from": langFrom,
                    "to": langTo
                }
                dataMsg.code = 0

                # IF need tagging
                if tagging > 0:
                    arrTagging = get_tagging(request, langTo, json_data["text"], arr_stopwords)
                    dataMsg.update_extras({
                        "tags": arrTagging
                    })

                return await build_json_response(dataMsg.toJSON())

    outStr, prob, type_id, arrTagging = translator(request, textStr, langFrom, langTo, tagging, arr_stopwords)
    if (outStr is None) or (len(outStr) == 0):
        errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
        return await build_json_response(errorMsg.toJSON())
    
    dataMsg.data = {
        "text": outStr,
        "prob": prob,
        "type": type_id,
        "ext": "",
        "from": langFrom,
        "to": langTo
    }
    dataMsg.code = 0

    # IF need tagging
    if tagging > 0:
        dataMsg.update_extras({
            "tags": arrTagging
        })

    # Tracking translate from external system as : google, deepL
    translate_tracking(
        request, langTo, langFrom, 
        textStr, outStr, prob
    )
    
    return await build_json_response(dataMsg.toJSON())

@bp.route("/mltranslate", methods=("GET", "POST"))
async def mltranslate(request):
    langFrom = request.args.get("lang_from")
    langTo = request.args.get("lang_to")
    textStr = request.args.get("text")
    apiKeyStr = request.args.get("apikey")
    tagging = request.args.get("tagging")
    ridStr = request.args.get("rid")
    if request.method == "POST":
        ridStr = request.form.get("rid")
        textStr = request.form.get("text")
        langFrom = request.form.get("lang_from")
        langTo = request.form.get("lang_to")
        tagging = request.form.get("tagging")
        apiKeyStr = request.form.get("apikey")

    if (langTo is None) or (len(langTo) == 0):
        langTo = ENGLISH_ISO2CODE
    
    if apiKeyStr not in request.app.config["apikeys"]:
        errorMsg = error_msg.ErrorMsg(1, "Invalid apikey : {}".format(apiKeyStr))
        return await build_json_response(errorMsg.toJSON())

    if is_none_param(tagging) == True:
        tagging = "0"
    tagging = string_to_int(tagging)

    arr_stopwords = set()
    if tagging > 0:
        if langTo in request.app.config["iso_stopwords"]:
            arr_stopwords = request.app.config["iso_stopwords"][langTo]
        arr_stopwords = arr_stopwords | get_punctuation_list()

    textStr = build_text(textStr)
    if is_none_param(textStr) == True:
        errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
        return await build_json_response(errorMsg.toJSON())
    
    if (langFrom is None) or (len(langFrom) == 0) or (langFrom == 'auto'):
        langFrom, _ = detect_language_compare(request, textStr, shuffle=True)

    dataMsg = response_msg.TranslateMsg(1, INTERNAL_ERROR_MSG)
    if is_none_param(ridStr) == False:
        dataMsg.set_extras({
            "rid": ridStr
        })
    
    dataMsg.data = []
    if langFrom == langTo:
        errorMsg = error_msg.ErrorMsg(1, 'lang_from, lang_to is the same ({})'.format(langFrom))
        item = {
            "text": textStr,
            "prob": 1.0,
            "type": 1,
            "ext": errorMsg.msg,
            "from": langFrom,
            "to": langTo
        }
        if tagging > 0:
            arrTagging = get_tagging(request, langTo, textStr, arr_stopwords)
            item["tags"] = arrTagging
        
        dataMsg.data.append(item)
        dataMsg.code = 0
        return await build_json_response(dataMsg.toJSON())

    arrLangTo = langTo.split(",")
    arrLangToInCache = []

    # Get in caching DB database
    if request.app.config["tracking_translator"] is not None:
        for lang in arrLangTo:
            json_data = request.app.config["tracking_translator"].get(textStr, langFrom, lang)
            if json_data is not None:
                status, _ = check_translator_result(request, json_data["text"], lang)
                if status == True:
                    arrLangToInCache.append(lang)
                    item = {
                        "text": json_data["text"],
                        "prob": json_data["prob"],
                        "type": -1,
                        "ext": "",
                        "from": langFrom,
                        "to": lang
                    }
                    if tagging > 0:
                        arrTagging = get_tagging(request, lang, json_data["text"], arr_stopwords)
                        item["tags"] = arrTagging
                    dataMsg.data.append(item)

    # Check miss key in caching
    arrLangToMiss = list(set(arrLangTo) - set(arrLangToInCache))
    if len(arrLangToMiss) == 0:
        dataMsg.code = 0
        return await build_json_response(dataMsg.toJSON())
    
    # Translate in multithread tasking
    resp = mltranslator(request, textStr, langFrom, arrLangToMiss, tagging, arr_stopwords)
    if (resp is None) or (len(resp) == 0):
        errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
        return await build_json_response(errorMsg.toJSON())

    # Build response data
    for lang, (outStr, prob, type_id, arrTagging) in resp.items():
        dataMsg.data.append({
            "text": outStr,
            "prob": prob,
            "type": type_id,
            "ext": "",
            "from": langFrom,
            "to": lang,
            "tags": arrTagging
        })

        # Tracking translate from external system as : google, deepL
        translate_tracking(
            request, lang, langFrom, 
            textStr, outStr, prob
        )
    
    dataMsg.code = 0
    return await build_json_response(dataMsg.toJSON())

@bp.route("/htranslate", methods=("GET", "POST"))
async def translate(request):
    langTo = request.args.get("lang_to")
    textStr = request.args.get("html")
    apiKeyStr = request.args.get("apikey")
    ridStr = request.args.get("rid")
    if request.method == "POST":
        ridStr = request.form.get("rid")
        textStr = request.form.get("html")
        langTo = request.form.get("lang_to")
        apiKeyStr = request.form.get("apikey")

    if (langTo is None) or (len(langTo) == 0):
        langTo = ENGLISH_ISO2CODE
    
    if apiKeyStr not in request.app.config["apikeys"]:
        errorMsg = error_msg.ErrorMsg(1, "Invalid apikey : {}".format(apiKeyStr))
        return await build_json_response(errorMsg.toJSON())

    if is_none_param(textStr) == True:
        errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
        return await build_json_response(errorMsg.toJSON())
    
    dataMsg = response_msg.TranslateMsg(1, INTERNAL_ERROR_MSG)
    if is_none_param(ridStr) == False:
        dataMsg.set_extras({
            "rid": ridStr
        })

    threadpool = None
    if request.app.config["threadpool"] is not None:
        threadpool = request.app.config["threadpool"]
    
    outStr, type_id = htranslator(request, textStr, langTo, threadpool=threadpool)
    if (outStr is None) or (len(outStr) == 0):
        errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
        return await build_json_response(errorMsg.toJSON())

    dataMsg.data = {
        "text": outStr,
        "type": type_id,
        "to": langTo
    }
    dataMsg.code = 0

    return await build_json_response(dataMsg.toJSON())

@bp.route("/summarize", methods=("GET", "POST"))
async def summarize(request):
    iso2Code = request.args.get("lang")
    textStr = request.args.get("text")
    apiKeyStr = request.args.get("apikey")
    ridStr = request.args.get("rid")
    if request.method == "POST":
        ridStr = request.form.get("rid")
        textStr = request.form.get("text")
        iso2Code = request.form.get("lang")
        apiKeyStr = request.form.get("apikey")
    
    if apiKeyStr not in request.app.config["apikeys"]:
        errorMsg = error_msg.ErrorMsg(1, "Invalid apikey : {}".format(apiKeyStr))
        return await build_json_response(errorMsg.toJSON())

    textStr = build_text(textStr)
    if is_none_param(textStr) == True:
        errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
        return await build_json_response(errorMsg.toJSON())
    
    if (iso2Code is None) or (len(iso2Code) == 0) or (iso2Code == 'auto'):
        iso2Code, _ = detect_language_compare(request, textStr, shuffle=True)

    # Translate if not exist summarization mapping
    sum2Code = iso2Code
    if iso2Code not in request.app.config["summarization_map"]:
        outStr, _, _, _ = translator(request, textStr, iso2Code, ENGLISH_ISO2CODE)
        if (outStr is None) or (len(outStr) == 0):
            errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
            return await build_json_response(errorMsg.toJSON())
        textStr =  outStr
        sum2Code = ENGLISH_ISO2CODE
    
    arrText = summarizer(sum2Code, textStr, request.app.config["summarization_map"])
    if (arrText is None) or (len(arrText) == 0):
        errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
        return await build_json_response(errorMsg.toJSON())
    
    dataMsg = response_msg.CommonMsg(0, arrText)
    if is_none_param(ridStr) == False:
        dataMsg.set_extras({
            "rid": ridStr
        })
    
    return await build_json_response(dataMsg.toJSON())