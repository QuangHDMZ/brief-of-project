__version__ = "1.1.0"
__author__ = "DevTeam"
from fadoml.api.ihttp import InternalHttpClient

http_internal_client = InternalHttpClient()

__all__ = [
    'common',
    'helper',
    'v1',
    'ihttp',
]