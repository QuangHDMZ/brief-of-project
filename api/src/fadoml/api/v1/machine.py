import sys
from sanic import Blueprint
from fadoml.util import imgutil
from fadoml.util.helper import build_text, is_none_param, \
    string_to_float, string_to_int, download_binary_file_from_url
from fadoml.util.constant import *
from fadoml.msg import error_msg, response_msg
from fadoml.api.helper import *
import logging
import aiofiles
import os
import uuid
import traceback
import json

app_name = __name__.replace(".", "_")
bp = Blueprint(app_name, url_prefix="/api/v1")

@bp.route("/detect_ban_product", methods=("GET", "POST"))
async def detect_ban_product(request):
    textStr = request.args.get("text")
    imageStr = request.args.get("imageurl")
    lang = request.args.get("lang")
    apiKeyStr = request.args.get("apikey")
    ocr = request.args.get("ocr")
    file = None
    ridStr = request.args.get("rid")
    if request.method == "POST":
        ridStr = request.form.get("rid")
        textStr = request.form.get("text")
        imageStr = request.form.get("imageurl")
        lang = request.form.get("lang")
        apiKeyStr = request.form.get("apikey")
        ocr = request.form.get("ocr")
        if 'image' in request.files:
            if len(request.files['image']) > 0:
                file = request.files['image'][0]

    if apiKeyStr not in request.app.config["apikeys"]:
        errorMsg = error_msg.ErrorMsg(1, "Invalid apikey : {}".format(apiKeyStr))
        return await build_json_response(errorMsg.toJSON())

    detectedLangProb = 1.0
    if is_none_param(textStr) == False:
        textStr = build_text(textStr)

        if len(textStr) == 0:
            errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
            return await build_json_response(errorMsg.toJSON())

        if is_none_param(lang) or (lang == 'auto'):
            lang, detectedLangProb = detect_language(request, textStr)
    
        # Translate
        if lang != ENGLISH_ISO2CODE: 
            (translatedTextStr, translatedTextProb, _, _) = translator(request, textStr, lang, ENGLISH_ISO2CODE)
        
            # Tracking translate from external system as : google, deepL
            translate_tracking(
                request, lang, ENGLISH_ISO2CODE, 
                textStr, translatedTextStr, translatedTextProb
            )

            if is_none_param(translatedTextStr):
                errorMsg = error_msg.ErrorMsg(1, "translatedTextStr is empty")
                return await build_json_response(errorMsg.toJSON())
            
            textStr = translatedTextStr

    uploadFile = None
    textOCR = ''
    fileType = None
    if file is not None:
        fileType = file.type
        uploadFileTmp = request.app.config["upload_path"] + "/" + file.name
        _, extension = os.path.splitext(uploadFileTmp)

        if (extension is None) or (len(extension) == 0):
            errorMsg = error_msg.ErrorMsg(1, "Invalid image type: {}. Just support {}".format(file.name, VALID_IMG_TYPE))
            return await build_json_response(errorMsg.toJSON())
    
        uploadFile = "{}/{}{}".format(request.app.config["upload_path"], uuid.uuid4(), extension)
        try:
            async with aiofiles.open(uploadFile, 'wb') as fwritter:
                await fwritter.write(file.body)
            fwritter.close()
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            uploadFile = None
    elif (is_none_param(imageStr) == False):
        (local_file_path, content_type, local_extension, local_filename) = download_binary_file_from_url(imageStr, request.app.config["upload_path"])
        uploadFile = local_file_path
        fileType = content_type
        extension = local_extension

    if (fileType is not None) and (fileType not in VALID_IMG_TYPE):
        errorMsg = error_msg.ErrorMsg(1, "Invalid image type: {}. Just support {}".format(file.name, VALID_IMG_TYPE))
        return await build_json_response(errorMsg.toJSON())

    if (is_none_param(textStr) == True) and (uploadFile is None):
        errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
        return await build_json_response(errorMsg.toJSON())

    ocr = string_to_int(ocr)
    if (ocr == 1) and (uploadFile is None):
        errorMsg = error_msg.ErrorMsg(1, "ocr=1 just use when have image")
        return await build_json_response(errorMsg.toJSON())

    if uploadFile is not None:
        try:
            if fileType == 'image/gif':
                uploadFile = imgutil.convert_gif_to_jpeg(uploadFile)
                if uploadFile is None:
                    errorMsg = error_msg.ErrorMsg(1, "Error convert gif to jpeg")
                    return await build_json_response(errorMsg.toJSON())

            if imgutil.valid_min_size(uploadFile, MIN_IMG_SIZE, MIN_IMG_SIZE) == False:
                errorMsg = error_msg.ErrorMsg(1, "Invalid min image size: {}".format(MIN_IMG_SIZE))
                return await build_json_response(errorMsg.toJSON())

            if ocr == 1:
                json_data = check_ocr(request, uploadFile, lang, 0, 0)
                if not is_none_param(json_data):
                    textOCR = ' '.join(json_data["data"].values())
                    textOCR = build_text(textOCR)

        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))

    json_data = check_ban_product(request, lang, textStr, uploadFile, textOCR)
    if json_data is None:
        errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
        return await build_json_response(errorMsg.toJSON())

    if ("text" not in json_data["data"]) and ("image" not in json_data["data"]):
        errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
        return await build_json_response(errorMsg.toJSON())

    detectBannedProductMsg = response_msg.DetectBannedProductMsg(
        0, 
        {
            'detect': lang,
            'prob': detectedLangProb
        }, 
        json_data["data"]
    )

    if is_none_param(ridStr) == False:
        detectBannedProductMsg.set_extras({
            "rid": ridStr
        })

    return await build_json_response(detectBannedProductMsg.toJSON())

@bp.route("/predict_categories", methods=["GET", "POST"])
async def predict_categories(request):
    textStr = request.args.get("text")
    apiKeyStr = request.args.get("apikey")
    lang = request.args.get("lang")
    ridStr = request.args.get("rid")
    if request.method == "POST":
        ridStr = request.form.get("rid")
        textStr = request.form.get("text")
        apiKeyStr = request.form.get("apikey")
        lang = request.form.get("lang")

    if apiKeyStr not in request.app.config["apikeys"]:
        errorMsg = error_msg.ErrorMsg(1, "Invalid apikey : {}".format(apiKeyStr))
        return await build_json_response(errorMsg.toJSON())

    textStr = build_text(textStr)
    if is_none_param(textStr):
        errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
        return await build_json_response(errorMsg.toJSON())

    detectedLangProb = 1.0
    if is_none_param(lang) or (lang == AUTO):
        lang, detectedLangProb = detect_language(request, textStr)
    
    # Translate
    if lang != ENGLISH_ISO2CODE: 
        (translatedTextStr, translatedTextProb, _, _) = translator(request, textStr, lang, ENGLISH_ISO2CODE)
    
        # Tracking translate from external system as : google, deepL
        translate_tracking(
            request, lang, ENGLISH_ISO2CODE, 
            textStr, translatedTextStr, translatedTextProb
        )

        if is_none_param(translatedTextStr):
            errorMsg = error_msg.ErrorMsg(1, "translatedTextStr is empty")
            return await build_json_response(errorMsg.toJSON())
        
        textStr = translatedTextStr
    
    json_data = check_mapping_product(request, ENGLISH_ISO2CODE ,textStr)
    if json_data is None:
        errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
        return await build_json_response(errorMsg.toJSON())

    categories = []
    for item in json_data["categories"]:
        for (node_id, score) in item.items():
            category_data = {"prob": score, "node_id": node_id}
            if node_id in request.app.config["categories"]:
                category_data["info"] = request.app.config["categories"][node_id]
            categories.append(category_data)

    categoryMsg = response_msg.CategoryMsg(
        0, 
        {
            'detect': lang,
            'prob': detectedLangProb
        }, 
        categories
    )

    if is_none_param(ridStr) == False:
        categoryMsg.set_extras({
            "rid": ridStr
        })
    
    return await build_json_response(categoryMsg.toJSON())

@bp.route("/nmt/translate", methods=["GET", "POST"])
async def translate(request):
    langFrom = request.args.get("lang_from")
    langTo = request.args.get("lang_to")
    textStr = request.args.get("text")
    apiKeyStr = request.args.get("apikey")
    ridStr = request.args.get("rid")
    if request.method == "POST":
        ridStr = request.form.get("rid")
        textStr = request.form.get("text")
        langFrom = request.form.get("lang_from")
        langTo = request.form.get("lang_to")
        apiKeyStr = request.form.get("apikey")

    if is_none_param(langTo):
        langTo = ENGLISH_ISO2CODE

    if apiKeyStr not in request.app.config["apikeys"]:
        errorMsg = error_msg.ErrorMsg(1, "Invalid apikey : {}".format(apiKeyStr))
        return await build_json_response(errorMsg.toJSON())

    textStr = build_text(textStr)
    if is_none_param(textStr) == True:
        errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
        return await build_json_response(errorMsg.toJSON())
    
    if is_none_param(langFrom) or (langFrom == 'auto'):
        langFrom, _ = detect_language_compare(request, textStr, shuffle=True)

    dataMsg = response_msg.NMTTranslatorMsg(1, textStr, INTERNAL_ERROR_MSG)
    if is_none_param(ridStr) == False:
        dataMsg.set_extras({
            "rid": ridStr
        })
    
    if langFrom == langTo:
        errorMsg = error_msg.ErrorMsg(1, 'lang_from, lang_to is the same ({})'.format(langFrom))
        dataMsg.text = textStr
        dataMsg.score = 1.0
        dataMsg.code = 0
        return await build_json_response(dataMsg.toJSON())

    # Start translate
    modelID = "{}-{}".format(langFrom, langTo)
    if modelID not in NMT_TRANSLATOR:
        errorMsg = error_msg.ErrorMsg(1, 'Just support pairs (lang_from-lang_to): {} (langFrom = {})'.format(" ,".join(NMT_TRANSLATOR.keys()), langFrom))
        dataMsg.text = textStr
        dataMsg.score = 0.0
        dataMsg.code = 0
        return await build_json_response(dataMsg.toJSON())
    
    modelID = NMT_TRANSLATOR[modelID]
    nmt_preprocessor = request.app.config["nmt_preprocessor"]
    logging.info("textStr (0) = {}".format(textStr))
    if langFrom == JAPANESE_ISO2CODE:
        tokenizer = request.app.config["nmt_tokenizer"][JAPANESE_ISO2CODE]
        textStr = nmt_preprocessor.preprocess_ja(textStr, tokenizer)
    elif langFrom == VIETNAMESE_ISO2CODE:
        tokenizer = request.app.config["vi_tokenizer"]
        textStr = nmt_preprocessor.preprocess_vi(textStr, tokenizer)
    elif langFrom == ENGLISH_ISO2CODE:
        textStr = nmt_preprocessor.preprocess_en(textStr)
    logging.info("textStr (1) = {}".format(textStr))

    json_data = check_nmt_translator(request, modelID, textStr, ridStr)
    logging.info("json_data = {}".format(json_data))
    if json_data is None:
        errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
        return await build_json_response(errorMsg.toJSON())

    dataMsg.text = json_data["text"]
    dataMsg.score = json_data["score"]
    return await build_json_response(dataMsg.toJSON())

@bp.route("/fingerprint/classify", methods=["POST"])
async def classify_fprint(request):
    apiKeyStr = request.args.get("apikey")
    sideHandEnum = request.args.get("side_hand")
    ridStr = request.args.get("rid")
    if request.method == "POST":
        ridStr = request.form.get("rid")
        apiKeyStr = request.form.get("apikey")
        sideHandEnum = request.form.get("side_hand")

    if apiKeyStr not in request.app.config["apikeys"]:
        errorMsg = error_msg.ErrorMsg(1, "Invalid apikey : {}".format(apiKeyStr))
        return await build_json_response(errorMsg.toJSON())

    sideHandEnum = string_to_int(sideHandEnum)
    if sideHandEnum not in SIDE_HAND.keys():
        errorMsg = error_msg.ErrorMsg(1, "Valid side_hand : {}".format(json.dumps(SIDE_HAND)))
        return await build_json_response(errorMsg.toJSON())

    file = None
    if 'image' in request.files:
        if len(request.files['image']) > 0:
            file = request.files['image'][0]

    if  is_none_param(file):
        errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
        return await build_json_response(errorMsg.toJSON())

    uploadFile = None
    fileType = file.type
    if fileType not in VALID_IMG_TYPE_FPRINT:
        errorMsg = error_msg.ErrorMsg(1, "Invalid image type: {}. Just support {}".format(file.name, VALID_IMG_TYPE))
        return await build_json_response(errorMsg.toJSON())
    
    uploadFileTmp = request.app.config["upload_path"] + "/" + file.name
    _, extension = os.path.splitext(uploadFileTmp)

    if (extension is None) or (len(extension) == 0):
        errorMsg = error_msg.ErrorMsg(1, "Invalid image type: {}. Just support {}".format(file.name, VALID_IMG_TYPE))
        return await build_json_response(errorMsg.toJSON())

    uploadFile = "{}/{}{}".format(request.app.config["upload_path"], uuid.uuid4(), extension)
    try:
        async with aiofiles.open(uploadFile, 'wb') as fwritter:
            await fwritter.write(file.body)
        fwritter.close()

        if imgutil.valid_min_size(uploadFile, MIN_IMG_SIZE_FPRINT, MIN_IMG_SIZE_FPRINT) == False:
            errorMsg = error_msg.ErrorMsg(1, "Invalid min image size: {}".format(MIN_IMG_SIZE_FPRINT))
            return await build_json_response(errorMsg.toJSON())
    except:
        logging.error(traceback.format_tb(sys.exc_info()[2]))
        pass

    params = {
        'img': uploadFile,
        'side_hand_enum': sideHandEnum,
    }

    json_data = create_internal_http_request(request.app.config["finger_print_port"], '/api/classify', params)

    if json_data is None:
        errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
        return await build_json_response(errorMsg.toJSON())

    return await build_json_response(json_data)

@bp.route("/fingerprint/verify", methods=["POST"])
async def verify_fprint(request):
    apiKeyStr = request.args.get("apikey")
    ridStr = request.args.get("rid")
    if request.method == "POST":
        ridStr = request.form.get("rid")
        apiKeyStr = request.form.get("apikey")

    if apiKeyStr not in request.app.config["apikeys"]:
        errorMsg = error_msg.ErrorMsg(1, "Invalid apikey : {}".format(apiKeyStr))
        return await build_json_response(errorMsg.toJSON())

    file = None
    if 'image' in request.files:
        if len(request.files['image']) > 0:
            file = request.files['image'][0]

    if  is_none_param(file):
        errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
        return await build_json_response(errorMsg.toJSON())

    uploadFile = None
    fileType = file.type
    if fileType not in VALID_IMG_TYPE_FPRINT:
        errorMsg = error_msg.ErrorMsg(1, "Invalid image type: {}. Just support {}".format(file.name, VALID_IMG_TYPE))
        return await build_json_response(errorMsg.toJSON())
    
    uploadFileTmp = request.app.config["upload_path"] + "/" + file.name
    _, extension = os.path.splitext(uploadFileTmp)

    if (extension is None) or (len(extension) == 0):
        errorMsg = error_msg.ErrorMsg(1, "Invalid image type: {}. Just support {}".format(file.name, VALID_IMG_TYPE))
        return await build_json_response(errorMsg.toJSON())

    uploadFile = "{}/{}{}".format(request.app.config["upload_path"], uuid.uuid4(), extension)
    try:
        async with aiofiles.open(uploadFile, 'wb') as fwritter:
            await fwritter.write(file.body)
        fwritter.close()

        if imgutil.valid_min_size(uploadFile, MIN_IMG_SIZE_FPRINT_VERIFY, MIN_IMG_SIZE_FPRINT_VERIFY) == False:
            errorMsg = error_msg.ErrorMsg(1, "Invalid min image size: {}".format(MIN_IMG_SIZE_FPRINT_VERIFY))
            return await build_json_response(errorMsg.toJSON())
    except:
        logging.error(traceback.format_tb(sys.exc_info()[2]))
        pass

    params = {
        'img': uploadFile,
    }

    json_data = create_internal_http_request(request.app.config["finger_print_port"], '/api/verify', params)
    if json_data is None:
        errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
        return await build_json_response(errorMsg.toJSON())

    return await build_json_response(json_data)


@bp.route("/ocr/predict", methods=["POST"])
async def predict_ocr(request):
    apiKeyStr = request.args.get("apikey")
    lang = request.args.get("lang")
    min_prob = request.args.get("min_prob")
    rotate = request.args.get("rotate")
    ridStr = request.args.get("rid")
    if request.method == "POST":
        ridStr = request.form.get("rid")
        apiKeyStr = request.form.get("apikey")
        lang = request.form.get("lang")
        min_prob = request.form.get("min_prob")
        rotate = request.form.get("rotate")

    if apiKeyStr not in request.app.config["apikeys"]:
        errorMsg = error_msg.ErrorMsg(1, "Invalid apikey : {}".format(apiKeyStr))
        return await build_json_response(errorMsg.toJSON())

    if lang not in VALID_LANGUAGES_OCR:
        errorMsg = error_msg.ErrorMsg(1, "Just support lang : {}".format(", ".join(VALID_LANGUAGES_OCR)))
        return await build_json_response(errorMsg.toJSON())

    min_prob = string_to_float(min_prob)
    rotate = string_to_int(rotate)

    if min_prob > 1 or min_prob < 0:
        errorMsg = error_msg.ErrorMsg(1, "Valid 0<= min_prob <=1")
        return await build_json_response(errorMsg.toJSON())

    file = None
    if 'image' in request.files:
        if len(request.files['image']) > 0:
            file = request.files['image'][0]

    if  is_none_param(file):
        errorMsg = error_msg.ErrorMsg(1, "Invalid file")
        return await build_json_response(errorMsg.toJSON())

    uploadFile = None
    fileType = file.type
    if fileType not in VALID_IMG_TYPE_OCR:
        errorMsg = error_msg.ErrorMsg(1, "Invalid image type: {}. Just support {}".format(file.name, VALID_IMG_TYPE))
        return await build_json_response(errorMsg.toJSON())
    
    uploadFileTmp = request.app.config["upload_path"] + "/" + file.name
    _, extension = os.path.splitext(uploadFileTmp)

    if (extension is None) or (len(extension) == 0):
        errorMsg = error_msg.ErrorMsg(1, "Invalid image type: {}. Just support {}".format(file.name, VALID_IMG_TYPE))
        return await build_json_response(errorMsg.toJSON())

    uploadFile = "{}/{}{}".format(request.app.config["upload_path"], uuid.uuid4(), extension)
    try:
        async with aiofiles.open(uploadFile, 'wb') as fwritter:
            await fwritter.write(file.body)
        fwritter.close()
    except:
        logging.error(traceback.format_tb(sys.exc_info()[2]))
        pass
    
    json_data = check_ocr(request, uploadFile, lang, min_prob, rotate)
    if json_data is None:
        errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
        return await build_json_response(errorMsg.toJSON())

    ocrMsg = response_msg.ResponseMsg(0, json_data["data"])
    if is_none_param(ridStr) == False:
        ocrMsg.set_extras({
            "rid": ridStr
        })

    return await build_json_response(ocrMsg.toJSON())

@bp.route("/vn_accent/predict", methods=["POST"])
async def vn_accent(request):
    textStr = request.args.get("text")
    apiKeyStr = request.args.get("apikey")
    ridStr = request.args.get("rid")
    if request.method == "POST":
        ridStr = request.form.get("rid")
        textStr = request.form.get("text")
        apiKeyStr = request.form.get("apikey")

    if apiKeyStr not in request.app.config["apikeys"]:
        errorMsg = error_msg.ErrorMsg(1, "Invalid apikey : {}".format(apiKeyStr))
        return await build_json_response(errorMsg.toJSON())

    textStr = build_text(textStr)
    if is_none_param(textStr) == True:
        errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
        return await build_json_response(errorMsg.toJSON())
    
    if len(textStr) > MAX_LEN_VN_ACCENT or len(textStr) < MIN_LEN_VN_ACCENT:
        errorMsg = error_msg.ErrorMsg(1, "Valid len text from {} to {}".format(MIN_LEN_VN_ACCENT, MAX_LEN_VN_ACCENT))
        return await build_json_response(errorMsg.toJSON())

    json_data = check_vn_accent(request, textStr)
    if json_data is None:
        errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
        return await build_json_response(errorMsg.toJSON())

    accentTranslatorMsg = response_msg.AccentTranslatorMsg(0, json_data["text"], json_data["prob"])
    if is_none_param(ridStr) == False:
        accentTranslatorMsg.set_extras({
            "rid": ridStr
        })
    
    return await build_json_response(accentTranslatorMsg.toJSON())

@bp.route("/hscode/predict", methods=["GET", "POST"])
async def predict_hscode(request):
    textStr = request.args.get("text")
    apiKeyStr = request.args.get("apikey")
    lang = request.args.get("lang")
    ridStr = request.args.get("rid")
    if request.method == "POST":
        ridStr = request.form.get("rid")
        textStr = request.form.get("text")
        apiKeyStr = request.form.get("apikey")
        lang = request.form.get("lang")

    if apiKeyStr not in request.app.config["apikeys"]:
        errorMsg = error_msg.ErrorMsg(1, "Invalid apikey : {}".format(apiKeyStr))
        return await build_json_response(errorMsg.toJSON())

    textStr = build_text(textStr)
    if is_none_param(textStr):
        errorMsg = error_msg.ErrorMsg(1, INVALID_PARAM_MSG)
        return await build_json_response(errorMsg.toJSON())

    detectedLangProb = 1.0
    if is_none_param(lang) or (lang == AUTO):
        lang, detectedLangProb = detect_language(request, textStr)

    # Translate
    if lang != ENGLISH_ISO2CODE: 
        (translatedTextStr, translatedTextProb, _, _) = translator(request, textStr, lang, ENGLISH_ISO2CODE)
    
        # Tracking translate from external system as : google, deepL
        translate_tracking(
            request, lang, ENGLISH_ISO2CODE, 
            textStr, translatedTextStr, translatedTextProb
        )

        if is_none_param(translatedTextStr):
            errorMsg = error_msg.ErrorMsg(1, "translatedTextStr is empty")
            return await build_json_response(errorMsg.toJSON())
        
        textStr = translatedTextStr

    json_data = check_hscode(request, lang, textStr)
    if json_data is None:
        errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
        return await build_json_response(errorMsg.toJSON())

    hscodeMsg = response_msg.HScodeMsg(
        0, 
        {
            'detect': lang,
            'prob': detectedLangProb
        }, 
        json_data["hscodes"]
    )

    if is_none_param(ridStr) == False:
        hscodeMsg.set_extras({
            "rid": ridStr
        })
        
    return await build_json_response(hscodeMsg.toJSON())

@bp.route("/captcha/predict", methods=["POST"])
async def predict_captcha(request):
    apiKeyStr = request.args.get("apikey")
    pattern = request.args.get("pattern")
    ridStr = request.args.get("rid")
    if request.method == "POST":
        ridStr = request.form.get("rid")
        apiKeyStr = request.form.get("apikey")
        pattern = request.form.get("pattern")

    if apiKeyStr not in request.app.config["apikeys"]:
        errorMsg = error_msg.ErrorMsg(1, "Invalid apikey : {}".format(apiKeyStr))
        return await build_json_response(errorMsg.toJSON())

    if is_none_param(pattern):
        pattern = "1"

    pattern = string_to_int(pattern)
    if pattern not in VALID_PATTERN_CAPTCHA_ENUM:
        errorMsg = error_msg.ErrorMsg(1, "Invalid pattern captcha")
        return await build_json_response(errorMsg.toJSON())

    file = None
    if 'image' in request.files:
        if len(request.files['image']) > 0:
            file = request.files['image'][0]

    if is_none_param(file):
        errorMsg = error_msg.ErrorMsg(1, "Invalid file")
        return await build_json_response(errorMsg.toJSON())

    uploadFile = None
    fileType = file.type
    if fileType not in VALID_IMG_TYPE_CAPTCHA:
        errorMsg = error_msg.ErrorMsg(
            1, "Invalid image type: {}. Just support {}".format(file.name, VALID_IMG_TYPE))
        return await build_json_response(errorMsg.toJSON())

    uploadFileTmp = request.app.config["upload_path"] + "/" + file.name
    _, extension = os.path.splitext(uploadFileTmp)
    if (extension is None) or (len(extension) == 0):
        errorMsg = error_msg.ErrorMsg(
            1, "Invalid image type: {}. Just support {}".format(file.name, VALID_IMG_TYPE))
        return await build_json_response(errorMsg.toJSON())

    uploadFile = "{}/{}{}".format(
        request.app.config["upload_path"], uuid.uuid4(), extension)

    try:
        async with aiofiles.open(uploadFile, 'wb') as fwritter:
            await fwritter.write(file.body)
        fwritter.close()
    except:
        logging.error(traceback.format_tb(sys.exc_info()[2]))
        pass

    json_data = check_captcha(request, uploadFile, pattern)
    if json_data is None:
        errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
        return await build_json_response(errorMsg.toJSON())

    captchaMsg = response_msg.ResponseMsg(0, json_data["data"])
    if is_none_param(ridStr) == False:
        captchaMsg.set_extras({
            "rid": ridStr
        })

    return await build_json_response(captchaMsg.toJSON())

@bp.route("/idcard/vn/fingerprint", methods=["POST"])
async def detect_fprint_from_id_card(request):
    apiKeyStr = request.args.get("apikey")
    imgBase64 = request.args.get("imgbase64")
    classify  = request.args.get("classify")
    ridStr = request.args.get("rid")
    if request.method == "POST":
        ridStr = request.form.get("rid")
        apiKeyStr = request.form.get("apikey")
        imgBase64 = request.form.get("imgbase64")
        classify  = request.form.get("classify")
    classify = string_to_int(classify)

    if apiKeyStr not in request.app.config["apikeys"]:
        errorMsg = error_msg.ErrorMsg(1, "Invalid apikey : {}".format(apiKeyStr))
        return await build_json_response(errorMsg.toJSON())
    
    json_data = check_idcard_fingerprint(request, imgBase64, classify, ridStr)
    if json_data is None:
        errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
        return await build_json_response(errorMsg.toJSON())

    resMsg = response_msg.ResponseMsg(0, json_data["data"])
    if is_none_param(ridStr) == False:
        resMsg.set_extras({
            "rid": ridStr
        })

    return await build_json_response(resMsg.toJSON())

@bp.route("/idcard/vn/ocr", methods=["POST"])
async def detect_ocr_from_id_card(request):
    apiKeyStr = request.args.get("apikey")
    imgBase64 = request.args.get("imgbase64")
    ridStr = request.args.get("rid")
    if request.method == "POST":
        ridStr = request.form.get("rid")
        apiKeyStr = request.form.get("apikey")
        imgBase64 = request.form.get("imgbase64")

    if apiKeyStr not in request.app.config["apikeys"]:
        errorMsg = error_msg.ErrorMsg(1, "Invalid apikey : {}".format(apiKeyStr))
        return await build_json_response(errorMsg.toJSON())

    json_data = check_idcard_ocr(request, imgBase64, ridStr)
    if json_data is None:
        errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
        return await build_json_response(errorMsg.toJSON())

    resMsg = response_msg.ResponseMsg(0, json_data["data"])
    if is_none_param(ridStr) == False:
        resMsg.set_extras({
            "rid": ridStr
        })
    
    return await build_json_response(resMsg.toJSON())

@bp.route("/idcard/vn/avatar", methods=["POST"])
async def detect_avatar_from_id_card(request):
    apiKeyStr = request.args.get("apikey")
    imgBase64 = request.args.get("imgbase64")
    ridStr = request.args.get("rid")
    if request.method == "POST":
        ridStr = request.form.get("rid")
        apiKeyStr = request.form.get("apikey")
        imgBase64 = request.form.get("imgbase64")

    if apiKeyStr not in request.app.config["apikeys"]:
        errorMsg = error_msg.ErrorMsg(1, "Invalid apikey : {}".format(apiKeyStr))
        return await build_json_response(errorMsg.toJSON())

    json_data = check_idcard_vn_avatar(request, imgBase64, ridStr)
    if json_data is None:
        errorMsg = error_msg.ErrorMsg(1, INTERNAL_ERROR_MSG)
        return await build_json_response(errorMsg.toJSON())

    resMsg = response_msg.ResponseMsg(0, json_data["data"])
    if is_none_param(ridStr) == False:
        resMsg.set_extras({
            "rid": ridStr
        })

    return await build_json_response(resMsg.toJSON())