import requests
import logging

class InternalHttpClient():
    def __init__(self, host="127.0.0.1", timeout=60):
        self.host = host
        self.timeout = timeout

    def build_url(self, port, uri):
        return "http://{}:{}{}".format(self.host, port, uri)

    def get(self, port, uri, payload):
        url = self.build_url(port, uri)
        try:
            r = requests.get(url, params=payload, timeout=self.timeout)
            if r.status_code == requests.codes.ok:
                return r.json()
        except Exception as e:
            logging.error('Error : {}'.format(e.__str__()))
            pass
        return None

    def post(self, port, uri, payload):
        url = self.build_url(port, uri)
        try:
            r = requests.post(url, data=payload, timeout=self.timeout)
            if r.status_code == requests.codes.ok:
                return r.json()
        except Exception as e:
            logging.error('Error : {}'.format(e.__str__()))
            pass
        return None