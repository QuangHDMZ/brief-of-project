# HTTP
POST_METHOD = "POST"
GET_METHOD = "GET"

#
INTERNAL_ERROR_MSG = "Something when wrong"
INVALID_PARAM_MSG = "Invalid parameters"
INVALID_IMAGE = "Invalid image"
INVALID_FPRINT = "Invalid fprint"
INVALID_CORNER = "Cant Detect 4 Corner"
DEFAULT_SERVER_MSG = "FadoML HTTP Server"
VIETNAMESE_ISO2CODE = "vi"
ENGLISH_ISO2CODE = "en"
JAPANESE_ISO2CODE = 'ja'
KHERME_ISO2CODE = 'km'
PROB_THREAD_HOLD = 0.4
DECIMAL_NUMBER = 5
VALID_IMG_TYPE = ['image/jpeg', 'image/png', 'image/gif', 'image/webp', 'image/jpg']
MIN_IMG_SIZE = 128
AUTO = 'auto'
VALID_IMG_FORMAT = ["jpg", "jpeg", "png", "gif", "webp", "bmp"]
MIN_SUBMIT_IMG_SIZE = 50
#CAPTCHA
WEIGHT_IMG_SIZE_CAPTCHA = 192
HEIGHT_IMG_SIZE_CAPTCHA = 64
VALID_IMG_TYPE_CAPTCHA = ['image/jpeg', 'image/png', 'image/webp', 'image/bmp', 'image/tif']
VALID_PATTERN_CAPTCHA_ENUM = [-1, 1, 2, 3, 4, 5]
MODEL_NAMES_CAPTCHA = ["model_pattern_1234", "model_pattern_5"]

# OCR
EASYOCR_TYPE = 'easyocr'
PADDLEOCR_TYPE = 'paddleocr'
VALID_LANGUAGES_OCR = ['vi', 'ja', 'en', 'de']
VALID_LANGUAGES_EASYOCR = ['en', 'de', 'vi']
VALID_LANGUAGES_PADDLEOCR = {
    # 'en': 'en',
    'ja': 'japan',
    # 'de': 'german',
}

VALID_IMG_TYPE_OCR = ['image/jpeg', 'image/png', 'image/webp', 'image/bmp', 'image/tif']

ROTATIONS = [0, 90, 180, 270]
MIN_IMG_SIZE_OCR = 600
MAX_IMG_SIZE_OCR = 1200

# Finger print
INVALID_CLASSIFY = "Invalid classify number only 0 or 1"
LEFT_PRINT = "left_fprint"
THRESHOLD_VERIFY = 0.6
MIN_IMG_SIZE_FPRINT = 299
MIN_IMG_SIZE_FPRINT_VERIFY = 128 
VALID_IMG_TYPE_FPRINT = ['image/jpeg', 'image/png', 'image/webp', 'image/bmp', 'image/tif']
SIDE_HAND = {
    1: 'Left',
    2: 'Right',
}
LABELS_FPRINT_LEFT = {
    0: 'ARCH',
    1: 'RL',
    2: 'UL',
    3: 'WCDI',
    4: 'WE',
    5: 'WPL',
    6: 'WST',
}
LABELS_FPRINT_RIGHT = {
    0: 'ARCH',
    1: 'UL',
    2: 'RL',
    3: 'WCDI',
    4: 'WE',
    5: 'WPL',
    6: 'WST',
}

# ID card
IDCARD_ALIGN_SIZE = 640

MAPPING_LABELS_FPRINT_ON_ID_CARD = {
    "PF Left": "left_fprint", 
    "PF Right":"right_fprint",
}

MIN_AVA_CONFIDENCE_THRESH = 0.5
MIN_FID_CONFIDENCE_THRESH = 0.2

LABELS_FRONT_SIDE_ID_CARD = [
    "avatar",
    "id",
    "name_1",
    "name_2",
    "birthday",
    "sex",
    "nationality",
    "home_1",
    "home_2",
    "address_1",
    "address_2",
    "expiry_date"
]

LABELS_FRONT_SIDE_ID_CARD_NEW = [
    "avatar",
    "id",
    "name -1-",
    "name -2-",
    "birthday",
    "sex",
    "nationality",
    "home -1-",
    "home -2-",
    "address -1-",
    "address -2-",
    "expiry_date"
]

BINARY_SIZE = (224,224)

USER_INFO_FRONT_SIDE_ID_CARD = [
    "id",
    "name",
    "birthday",
    "sex",
    "nationality",
    "home",
    "address",
]

# Translate
NMT_TRANSLATOR = {
    'vi-en': 1,
    'ja-en': 2,
    'en-vi': 3,
}

CONTRACTIONS_EN_MODIFIED = {
"ain't": "am not",
"aren't": "are not",
"can't": "can not",
"can't've": "can not have",
"'cause": "because",
"could've": "could have",
"couldn't": "could not",
"couldn't've": "could not have",
"didn't": "did not",
"doesn't": "does not",
"don't": "do not",
"hadn't": "had not",
"hadn't've": "had not have",
"hasn't": "has not",
"haven't": "have not",
# "he'd": "he had / he would",
"he'd've": "he would have",
# "he'll": "he shall / he will",
# "he'll've": "he shall have / he will have",
# "he's": "he has / he is",
"how'd": "how did",
"how'd'y": "how do you",
"how'll": "how will",
# "how's": "how has / how is",
# "i'd": "I had / I would",
"i'd've": "I would have",
# "i'll": "I shall / I will",
# "i'll've": "I shall have / I will have",
"i'm": "I am",
"i've": "I have",
"isn't": "is not",
# "it'd": "it had / it would",
"it'd've": "it would have",
# "it'll": "it shall / it will",
# "it'll've": "it shall have / it will have",
# "it's": "it has / it is",
"let's": "let us",
"ma'am": "madam",
"mayn't": "may not",
"might've": "might have",
"mightn't": "might not",
"mightn't've": "might not have",
"must've": "must have",
"mustn't": "must not",
"mustn't've": "must not have",
"needn't": "need not",
"needn't've": "need not have",
"o'clock": "of the clock",
"oughtn't": "ought not",
"oughtn't've": "ought not have",
"shan't": "shall not",
"sha'n't": "shall not",
"shan't've": "shall not have",
# "she'd": "she had / she would",
"she'd've": "she would have",
# "she'll": "she shall / she will",
# "she'll've": "she shall have / she will have",
# "she's": "she has / she is",
"should've": "should have",
"shouldn't": "should not",
"shouldn't've": "should not have",
"so've": "so have",
# "so's": "so as / so is",
# "that'd": "that would / that had",
"that'd've": "that would have",
# "that's": "that has / that is",
# "there'd": "there had / there would",
"there'd've": "there would have",
# "there's": "there has / there is",
# "they'd": "they had / they would",
"they'd've": "they would have",
# "they'll": "they shall / they will",
# "they'll've": "they shall have / they will have",
"they're": "they are",
"they've": "they have",
"to've": "to have",
"wasn't": "was not",
# "we'd": "we had / we would",
"we'd've": "we would have",
"we'll": "we will",
"we'll've": "we will have",
"we're": "we are",
"we've": "we have",
"weren't": "were not",
# "what'll": "what shall / what will",
# "what'll've": "what shall have / what will have",
"what're": "what are",
# "what's": "what has / what is",
"what've": "what have",
# "when's": "when has / when is",
"when've": "when have",
"where'd": "where did",
# "where's": "where has / where is",
"where've": "where have",
# "who'll": "who shall / who will",
# "who'll've": "who shall have / who will have",
# "who's": "who has / who is",
"who've": "who have",
# "why's": "why has / why is",
"why've": "why have",
"will've": "will have",
"won't": "will not",
"won't've": "will not have",
"would've": "would have",
"wouldn't": "would not",
"wouldn't've": "would not have",
"y'all": "you all",
"y'all'd": "you all would",
"y'all'd've": "you all would have",
"y'all're": "you all are",
"y'all've": "you all have",
# "you'd": "you had / you would",
"you'd've": "you would have",
# "you'll": "you shall / you will",
# "you'll've": "you shall have / you will have",
"you're": "you are",
"you've": "you have"
}

# Accent
MAX_LEN_VN_ACCENT = 200
MIN_LEN_VN_ACCENT = 10

# HScode
BERT_BASE_UNCASED = "distilbert-base-uncased"
SEQ_KERAS = "seq_keras"
SEQ_TRANSFORMER = "seq_transformer"
GROUPS_HSCODE_MAX_LEN = {
    "group1": 262,
    "group2": 463,
    "group3": 257,
    "group4": 487,
}
N_LABELS_GROUP3 = 374