import pandas as pd
import json
import torch
from fadoml.util.helper import exist_file
import pickle
from vietocr.tool.predictor import Predictor
from vietocr.tool.config import Cfg
import yaml
from tensorflow import keras
import logging

def read_txt(path):
    data = open(path, "r")
    data = data.read()
    return data

def write_txt(data, path):
    with open(path, 'w') as file:
        for item in data:
            file.write("%s  " % item)

def read_csv(path):
    data = pd.read_csv(path)
    return data

def write_csv(data, path):
    with open(path, 'a') as file:
        data.to_csv(file)

def read_json(path):
    if exist_file(path) == False:
        logging.error("Can not load json : {}".format(path))
        return None

    data = None
    with open(path) as file:
        data = json.load(file)
        file.close()
    return data

def write_json(data, path):
    with open(path, "w") as file:
        json.dump(data, file)

def read_torch(path, device_type='cpu'):
    if exist_file(path) == False:
        logging.error("Can not load torch model : {}".format(path))
        return None

    model = torch.load(path, map_location=(device_type))
    return model

def read_yolo5(path, device_type='cpu'):
    if exist_file(path) == False:
        logging.error("Can not load yolo5 model : {}".format(path))
        return None

    model = torch.hub.load('ultralytics/yolov5', 'custom', path=path, device=device_type)
    return model

def load_model_keras(model_path):
    try:
        return keras.models.load_model(model_path)
    except:
        logging.error("Can not load keras model : {}".format(model_path))
    return None

def _load_config_from_file_vietocr(fname, fbase):
    base_config = None
    if exist_file(fbase) == True:
        with open(fbase, encoding='utf-8') as f:
            base_config = yaml.safe_load(f)
            f.close()
    
    if exist_file(fname) == True:
        with open(fname, encoding='utf-8') as f:
            config = yaml.safe_load(f)
            if base_config is not None:
                if config is not None:
                    base_config.update(config)
            f.close()
    
    if base_config is None:
        base_config = {}
    return Cfg(base_config)

def read_vietocr(weight_path, fname_path, fbase_path, device_type='cpu'):
    try:
        # config = Cfg.load_config_from_name('vgg_transformer')
        config = _load_config_from_file_vietocr(fname_path, fbase_path)
        
        if config is None:
            config = {}
        if 'cnn' not in config:
            config['cnn'] = {}
        if 'predictor' not in config:
            config['predictor'] = {}
        
        config['weights'] = weight_path
        config['cnn']['pretrained']=False
        config['predictor']['beamsearch']=False
        config['device'] = device_type

        return Predictor(config)
    except:
        logging.error("Can not load vietocr model : {}".format(fname_path))
    return None

def read_pickle(path):
    if exist_file(path) == False:
        logging.error("Can not load pickle : {}".format(path))
        return None

    data = None
    with open(path, "rb") as handle:
        data = pickle.load(handle)
        handle.close()
        
    return data