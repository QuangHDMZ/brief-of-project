import cv2
import os
from fadoml.util.helper import exist_file
import base64
from io import BytesIO
from PIL import Image
import numpy as np
from fadoml.util.constant import VALID_IMG_FORMAT, MIN_SUBMIT_IMG_SIZE, VALID_IMG_TYPE
from mimetypes import guess_extension, guess_type

def check_valid_image(img):
    """
    img: imgbase64, filename, path or URL
    """
    try:
        ext = guess_extension(guess_type(img)[0])
        if ext is None:
            return False
        ext = ext.strip()
        if len(ext) == 0:
            return False
        ext = ext.replace(".", "")
        if ext.lower() in VALID_IMG_FORMAT:
            return True
    except:
        pass
    
    return False

def convert_base64_to_pil_image(im_b64):
    if check_valid_image(im_b64) == False:
        return None

    arr_im_b64 = im_b64.split(",")
    if len(arr_im_b64) < 1:
        return None

    try:
        im_bytes = base64.b64decode(arr_im_b64[1])
        im_file = BytesIO(im_bytes)
        img = Image.open(im_file)
        
        width, height = img.size
        if (width < MIN_SUBMIT_IMG_SIZE) or (height < MIN_SUBMIT_IMG_SIZE):
            return None
        return img
    except:
        pass
    
    return None

def convert_pil_image_to_base64(img, imread=False, ext="jpeg"):
    if ext.lower() not in VALID_IMG_FORMAT:
        return None

    try:
        if imread == True:
            img = Image.open(img)

        im_file = BytesIO()
        img.save(im_file, format=("{0}".format(ext.upper())))
        im_bytes = im_file.getvalue()
        im_b64 = base64.b64encode(im_bytes)
        return "data:image/{0};base64,{1}".format(ext.lower(), im_b64.decode("utf-8"))
    except:
        pass
    
    return None

def convert_base64_to_cv_image(im_b64):
    if check_valid_image(im_b64) == False:
        return None

    arr_im_b64 = im_b64.split(",")
    if len(arr_im_b64) < 1:
        return None

    try:
        im_bytes = base64.b64decode(arr_im_b64[1])
        im_arr = np.frombuffer(im_bytes, dtype=np.uint8)
        img = cv2.imdecode(im_arr, flags=cv2.IMREAD_COLOR)
        
        (height, width, _channel) = img.shape
        if (width < MIN_SUBMIT_IMG_SIZE) or (height < MIN_SUBMIT_IMG_SIZE):
            return None
        return img
    except:
        pass
    
    return None

def convert_cv_image_to_base64(img, imread=False, ext="jpeg"):
    if ext.lower() not in VALID_IMG_FORMAT:
        return None

    try:
        if imread == True:
            img = cv2.imread(img)

        _, im_arr = cv2.imencode(".{0}".format(ext.lower()), img)
        im_bytes = im_arr.tobytes()
        im_b64 = base64.b64encode(im_bytes)
        return "data:image/{0};base64,{1}".format(ext.lower(), im_b64.decode("utf-8"))
    except:
        pass
    
    return None

def convert_gif_to_jpeg(image, ext="jpeg"):
    if exist_file(image) == False:
        return None

    try:
        name, _ = os.path.splitext(image)
        newImage = "{0}.{1}".format(name, ext)
        
        gif = cv2.VideoCapture(image)
        _ret, frame = gif.read()
        cv2.imwrite(newImage, frame)
        return newImage
    except:
        pass
    
    return None

def valid_min_size(image, min_height, min_width):
    if (exist_file(image) == False) or (min_height < 0) or (min_width < 0):
        return False

    try:
        img = cv2.imread(image)

        (height, width, _channel) = img.shape
        if (height < min_height) or (width < min_width):
            return False
        return True
    except:
        pass
    
    return False

def convert_cv2_img_to_pil_img(img, channel="RGB"):
    try:
        img = Image.fromarray(img, channel)
        return img
    except:
        return None

def toImgPIL(imgOpenCV): 
    return Image.fromarray(cv2.cvtColor(imgOpenCV, cv2.COLOR_BGR2RGB))
