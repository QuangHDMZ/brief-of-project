import hashlib
import logging
import rocksdb
from pymongo import MongoClient, DESCENDING
from urllib.parse import quote_plus
from multiprocessing import cpu_count
import datetime
import json
import sys, traceback

class TrackingTranslator:
    def __init__(self, m_uri, m_username, m_password, data_path, debug_mode):
        data_file_path = "{}/tracking.db".format(data_path)
        self.debug_mode = debug_mode
        if debug_mode > 0:
            logging.info("Load translate caching at {}".format(data_file_path))
        self.db = rocksdb.DB(data_file_path, rocksdb.Options(create_if_missing=True))
        
        self.mongos = self.load_mongo_db(m_uri, m_username, m_password)
        if debug_mode > 0:
            logging.info("Create indexes")
        self.create_index()

    def load_mongo_db(self, m_uri, m_username, m_password):
        if (m_username != '') and (m_password != ''):
            uri = "mongodb://%s:%s@%s" % (quote_plus(m_username), quote_plus(m_password), m_uri)
        else:
            uri = "mongodb://%s" % (m_uri)
        return MongoClient(uri, maxPoolSize=(cpu_count() + 1))

    def create_index(self):
        try:
            now = datetime.datetime.now()
            MONGO_TABLE = "translation_{}_{}".format(now.year, now.month)
            resp = self.mongos.fdglml_tracking[MONGO_TABLE].create_index([ ("md5_text", -1), ("created_date", -1) ])
            if self.debug_mode > 0:
                logging.info("Index response: {}".format(resp))
        except:
            pass

    def md5(self, text):
        return hashlib.md5(text.encode('utf-8')).hexdigest()

    def get(self, text, lang_from, lang_to):
        md5_string = self.md5(text)
        try:
            key_name = "{}.{}.{}".format(md5_string, lang_from, lang_to)
            data_string = self.db.get(key_name.encode())
            if data_string is not None:
                return json.loads(data_string)

            now = datetime.datetime.now()
            MONGO_TABLE = "translation_{}_{}".format(now.year, now.month)
            docs = self.mongos.fdglml_tracking[MONGO_TABLE].find({"md5_text": md5_string}).sort("created_date", DESCENDING)
            if docs is not None:
                for doc in docs:
                    if (doc["lang_from"] == lang_from) and (doc["lang_to"] == lang_to):
                        textFieldProb = 'text_{}_prob'.format(doc["lang_to"])
                        textField = 'text_{}'.format(doc["lang_to"])
                        data_string = json.dumps({
                            "text": doc[textField],
                            "prob": doc[textFieldProb]
                        })
                        self.db.put(key_name.encode(), data_string.encode('utf-8'))
                        return {
                            "text": doc[textField],
                            "prob": doc[textFieldProb]
                        }
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None

    def put(self, text, lang_from, lang_to, text_out, prob_num):
        md5_string = self.md5(text)
        key_name = "{}.{}.{}".format(md5_string, lang_from, lang_to)
        try:
            data_string = json.dumps({
                "text": text_out,
                "prob": prob_num
            })
            self.db.put(key_name.encode(), data_string.encode('utf-8'))
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None