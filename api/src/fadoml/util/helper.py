import os
import json
import tensorflow as tf
import torch
import torch.multiprocessing as mp
import numpy as np
import requests
import shutil
import uuid
import mimetypes
import string
from underthesea import word_tokenize
from khmernltk import word_tokenize as kh_word_tokenize
from fadoml.util.constant import VIETNAMESE_ISO2CODE, \
    KHERME_ISO2CODE, JAPANESE_ISO2CODE
import logging
from fugashi import Tagger as JpTagger
from pysummarization.abstractabledoc.top_n_rank_abstractor import TopNRankAbstractor

TRANSLATE_TRACKING_TOPIC = "translate"

def summarizer(iso2Code, textStr, summarization_map, limit=3):
    arrText = []
    if summarization_map is not None:
        if iso2Code in summarization_map:
            if summarization_map[iso2Code] is not None:
                try:
                    abstractable_doc = TopNRankAbstractor()
                    docs = summarization_map[iso2Code].summarize(textStr, abstractable_doc)
                    i = 1
                    for sentence in docs["summarize_result"]:
                        arrText.append(sentence)
                        if i >= limit:
                            break
                        i += 1
                except Exception as e:
                    logging.error('Error : {}'.format(e.__str__()))
                    pass

    if len(arrText) == 0:
        arrText.append(textStr)
    
    return arrText

def get_phrases_rank(iso2Code, textStr, spacytokenizer_map, limit=3):
    arrText = []
    if spacytokenizer_map is not None:
        if iso2Code in spacytokenizer_map:
            if spacytokenizer_map[iso2Code] is not None:
                try:
                    docs = spacytokenizer_map[iso2Code](textStr)
                    i = 1
                    for phrase in docs._.phrases:
                        arrText.append(phrase.text)
                        if i >= limit:
                            break
                        i += 1
                except Exception as e:
                    logging.error('Error : {}'.format(e.__str__()))
                    pass

    if len(arrText) == 0:
        arrText.append(textStr)
    
    return arrText

def get_punctuation_list():
    specials = set(["、", "。"])
    arr = set(string.punctuation) | specials
    return arr

def clean_tokenize_character(textStr):
    specials = ["▁", "_", ".", "、", "。"]
    for c in specials:
        textStr = textStr.replace(c, " ")
    return textStr.strip()

def word_tokenizer(
    iso2Code, textStr, cctokenizer=None, 
    bpetokenizer_map=None, nmttokenizer_map=None,
    spacytokenizer_map=None
):
    arrText = None
    if iso2Code == VIETNAMESE_ISO2CODE:
        if cctokenizer is not None:
            try:
                arrText = cctokenizer.word_tokenize(textStr, tokenize_option=0)
            except Exception as e:
                logging.error('Error : {}'.format(e.__str__()))
                pass

        if is_none_param(arrText) == True:
            try:
                arrText = word_tokenize(textStr)
            except Exception as e:
                logging.error('Error : {}'.format(e.__str__()))
                pass
    elif iso2Code == KHERME_ISO2CODE:
        try:
            arrText = kh_word_tokenize(textStr, return_tokens=True)
        except Exception as e:
            logging.error('Error : {}'.format(e.__str__()))
            pass
    
    # Use spacy tokenizer
    if is_none_param(arrText) == True:
        if spacytokenizer_map is not None:
            if iso2Code in spacytokenizer_map:
                if spacytokenizer_map[iso2Code] is not None:
                    try:
                        words = spacytokenizer_map[iso2Code](textStr)
                        arrText = []
                        for word in words:
                            arrText.append(word.text)
                    except Exception as e:
                        logging.error('Error : {}'.format(e.__str__()))
                        pass

    if is_none_param(arrText) == True:
        if iso2Code == JAPANESE_ISO2CODE:
            try:
                tagger = JpTagger('-Owakati')
                arrText = []
                for word in tagger(textStr):
                    arrText.append(word.surface)
            except Exception as e:
                logging.error('Error : {}'.format(e.__str__()))
                pass

    if is_none_param(arrText) == True:
        if bpetokenizer_map is not None:
            if iso2Code in bpetokenizer_map:
                if bpetokenizer_map[iso2Code] is not None:
                    try:
                        arrText = bpetokenizer_map[iso2Code].encode(textStr)
                    except Exception as e:
                        logging.error('Error : {}'.format(e.__str__()))
                        pass

    if is_none_param(arrText) == True:
        if nmttokenizer_map is not None:
            if iso2Code in nmttokenizer_map:
                if nmttokenizer_map[iso2Code] is not None:
                    try:
                        arrText = nmttokenizer_map[iso2Code].tokenize(textStr)
                        if len(arrText) > 0:
                            arrText = arrText[0]
                    except Exception as e:
                        logging.error('Error : {}'.format(e.__str__()))
                        pass
    
    if arrText is not None:
        arrText = [clean_tokenize_character(word.strip()) for word in arrText if word.strip()]
    
    return arrText

def get_text_file(text_file_path):
    try:
        with open(text_file_path) as text_file:
            data = text_file.readlines()
            text_file.close()
            return data
    except Exception as e:
        pass

    return None

def get_stopword(file_path):
    stopwords = get_text_file(file_path)
    if stopwords is not None:
        stopwords = [x.strip() for x in stopwords if x.strip()]
    
    if stopwords is None:
        stopwords = []

    return set(stopwords)

def download_binary_file_from_url(url, download_path):
    local_filename = str(uuid.uuid4())
    local_file_path = None
    content_type = "image/png"
    extension = ".png"
    
    try:
        with requests.get(url, stream=True) as r:
            if 'content-type' in r.headers:
                content_type = r.headers['content-type']
                extension = mimetypes.guess_extension(content_type)
            
            local_file_path = "{}/{}{}".format(download_path, local_filename, extension)
            with open(local_file_path, 'wb') as f:
                shutil.copyfileobj(r.raw, f)
                f.close()
    except Exception as e:
        pass
    
    return (local_file_path, content_type, extension.replace(".", ""), local_filename)

def have_gpus(enable_available=False):
    if enable_available == False:
        return False
    gpus = tf.config.list_physical_devices('GPU')
    return (len(gpus) > 0)

def patch_gpus(enable_available=False):
    if have_gpus(enable_available) == True:
        mp.set_start_method('spawn')

def have_cuda(enable_available=False):
    if enable_available == False:
        return False
    return torch.cuda.is_available()

def patch_cuda(enable_omp=False):
    gpus = tf.config.list_physical_devices('GPU')
    
    os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
    if len(gpus) == 0:
        os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
        os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
    
    if enable_omp == False:
        os.environ["OMP_NUM_THREADS"] = "1"
    else:
        cpu_number = (mp.cpu_count() + 1)
        torch.set_num_threads(cpu_number)
        os.environ["OMP_NUM_THREADS"] = str(cpu_number)
        os.environ["MKL_NUM_THREADS"] = str(cpu_number)
    return enable_omp

def is_none_param(data):
    try: 
        return (data is None) or (len(data) == 0)
    except:
        return True

def build_text(text):
    if type(text) != str:
        return ''
        
    text = text.replace("\r"," ")
    text = text.replace("\n"," ")
    return text

def build_fasttext_text(text):
    if type(text) != str:
        return ''
        
    text = text.replace("\r"," ")
    text = text.replace("\n"," ")
    text = text.translate(str.maketrans('', '', string.punctuation))
    return text

def string_to_int(str):
    if (str is None) or (len(str) == 0):
        return 0

    try:
        return int(str)
    except:
        pass
    return 0

def string_to_float(str):
    if (str is None) or (len(str) == 0):
        return 0.0

    try:
        return float(str)
    except:
        pass
    return 0.0

def exist_file(file_path):
    return os.path.isfile(file_path)

def exist_dir(dir_path):
    return os.path.isdir(dir_path)

def remove_file(file_path):
    if os.path.exists(file_path):
        os.remove(file_path)

def put_translate_tracking(pools, data_obj):
    with pools.item() as client:
        return client.put_job_into(TRANSLATE_TRACKING_TOPIC, json.dumps(data_obj))

def make_dirs_if_needed(path):
    if exist_dir(path) == False:
        os.makedirs(path)

# Convert score (-inf -> 0) to (0 -> 1)
def convert_score_nmt(score):
    new_score = 0.0

    # avoid overflow encountered in exp
    if (score >= -200) and (score <= 0):
        new_score = 50/(np.log10(np.exp(-score*3)) + 50)

    return new_score

def softmax(Z):
    """
    Compute softmax values for 
    each sets of scores in V.
    each column of V is a set of score.    
    """
    e_Z = np.exp(Z)
    A = e_Z / e_Z.sum(axis = 0)
    return A

def argsort(x, reverse=True):
    return sorted(range(len(x)), key=lambda k: x[k], reverse=reverse)
