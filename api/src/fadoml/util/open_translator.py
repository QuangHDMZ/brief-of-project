import requests
import fadoml.gtranslator as gts
import fadoml.utranslator as ats
from fadoml.ptranslator.deepL import DeepLTranslator
from fadoml.ptranslator.deepL import TranslateLanguageEnum, TranslateModeType
from fadoml.dtranslator import (GoogleTranslator, MyMemoryTranslator)
import random
import json
import time
import logging
from anti_useragent import UserAgent
import sys, traceback

class OpenTranslator:
    def __init__(self, gg_file_path, debug_mode, proxies=None):
        self.proxies = proxies
        try:
            self.ua = UserAgent(platform='mac')
        except:
            pass
        self.user_agent = gts.DEFAULT_USER_AGENT
        
        self.google_service_urls = ["translate.google.com"]
        if gg_file_path is None:
            self.google_service_urls = gts.DEFAULT_SERVICE_URLS
        else:
            with open(gg_file_path) as gg_file:
                self.google_service_urls = json.load(gg_file)
        self.google_service_urls_len = len(self.google_service_urls)

        logging.info("Process gtranslator mapping")
        self.gtranslator = gts.Translator(service_urls=self.google_service_urls, user_agent = self.get_random_useragent(), proxies=self.get_random_proxy())
        self.LIBRE_LANGUAGES = {
            'auto' : 'auto',
            'en' : gts.LANGUAGES['en'],
            'ar' : gts.LANGUAGES['ar'],
            'zh' : gts.LANGUAGES['zh-cn'],
            'fr' : gts.LANGUAGES['fr'],
            'de' : gts.LANGUAGES['de'],
            'hi' : gts.LANGUAGES['hi'],
            'ga' : gts.LANGUAGES['ga'],
            'it' : gts.LANGUAGES['it'],
            'ja' : gts.LANGUAGES['ja'],
            'ko' : gts.LANGUAGES['ko'],
            'pt' : gts.LANGUAGES['pt'],
            'ru' : gts.LANGUAGES['ru'],
            'es' : gts.LANGUAGES['es']
        }
        
        ### Process to load ulion language mapping
        logging.info("Process to load ulion language mapping")
        text = "超急速充電"
        self.ULION_GOOGLE_LANGUAGES = None
        self.ULION_ALIBABA_LANGUAGES = None
        self.ULION_SOGOU_LANGUAGES = None
        self.ULION_BAIDU_LANGUAGES = None
        self.ULION_YOUDAO_LANGUAGES = None
        self.ULION_BING_LANGUAGES = None
        self.ULION_YANDEX_LANGUAGES = None
        self.ULION_ICIBA_LANGUAGES = None
        try:
            logging.info("Process google mapping")
            ats.google(text, proxies=self.get_random_proxy())
            self.ULION_GOOGLE_LANGUAGES = ats._google.language_map
            logging.info("Process bing mapping")
            ats.bing(text, proxies=self.get_random_proxy())
            self.ULION_BING_LANGUAGES = ats._bing.language_map
            logging.info("Process yandex mapping")
            ats.yandex(text, proxies=self.get_random_proxy())
            self.ULION_YANDEX_LANGUAGES = ats._yandex.language_map
            logging.info("Process iciba mapping")
            ats.iciba(text, proxies=self.get_random_proxy())
            self.ULION_ICIBA_LANGUAGES = ats._iciba.language_map
            logging.info("Process alibaba mapping")
            ats.alibaba(text, proxies=self.get_random_proxy())
            self.ULION_ALIBABA_LANGUAGES = ats._alibaba.language_map
            logging.info("Process sogou mapping")
            ats.sogou(text, proxies=self.get_random_proxy())
            self.ULION_SOGOU_LANGUAGES = ats._sogou.language_map
            logging.info("Process baidu mapping")
            ats.baidu(text, proxies=self.get_random_proxy())
            self.ULION_BAIDU_LANGUAGES = ats._baidu.language_map
            logging.info("Process youdao mapping")
            ats.youdao(text, proxies=self.get_random_proxy())
            self.ULION_YOUDAO_LANGUAGES = ats._youdao.language_map
        except:
            pass

        ### Process to load deep_translator language mapping
        logging.info("Process to load deep_translator language mapping")
        self.DEEP_GOOGLE_LANGUAGES = {v: k for k, v in GoogleTranslator.get_supported_languages(as_dict=True).items()}
        self.DEEP_MYMEMORY_LANGUAGES = {v: k for k, v in MyMemoryTranslator.get_supported_languages(as_dict=True).items()}

        ### Process testing data
        if debug_mode == 1:
            logging.info("Process to test google domain")
            #self.test_google_domain()

    def get_random_useragent(self):
        user_agent = self.user_agent
        if self.ua is not None:
            user_agent = self.ua.random
        return user_agent

    def get_random_proxy(self):
        if (self.proxies is None) or (len(self.proxies) == 0):
            return None

        if len(self.proxies) == 1:
            return self.proxies[0]

        return random.choice(self.proxies)

    def test_google_domain(self):
        text = "GoodSense Nighttime Sleep Aid, Diphenhydramine HCl 25 mg, Relieves Occasional Sleeplessness, 36 Count"
        
        arr_data = []
        for url in self.google_service_urls:
            print("Test GG domain : {}".format(url))
            started_indexed = time.time()
            text_out = self.translate_from_deep_goole_test(text, url, 'auto', 'ja')
            closed_indexed = time.time()
            arr_data.append((url, (closed_indexed - started_indexed), text_out))
        sorted(arr_data, key=lambda item: item[1])

        for (url, time_esp, text_out) in arr_data:
            print("Translate from {} => {} seconds => {}".format(url, time_esp, text_out))

    def get_random_base_google_service(self):
        number = random.randint(0, (self.google_service_urls_len - 1))
        url = "https://{}/m".format(self.google_service_urls[number])
        return url

    def get_base_google_service(self, url):
        url = "https://{}/m".format(url)
        return url

    def get_random_base_google_host(self):
        number = random.randint(0, (self.google_service_urls_len - 1))
        url = self.google_service_urls[number]
        url_host = "https://{}".format(url)
        url_api = "https://{}/_/TranslateWebserverUi/data/batchexecute".format(url)
        return (url_host, url_api)

    def translate_from_google(self, text, lang_from = 'auto', lang_to='en'):
        if gts.LANGUAGES is None:
            return None

        text = text.strip()
        if (len(text) == 0) or ((lang_from != 'auto') and (lang_from not in gts.LANGUAGES)) or (lang_to not in gts.LANGUAGES):
            return None
        
        try:
            translation = self.gtranslator.translate(text, src = lang_from, dest = lang_to, user_agent=self.get_random_useragent())
            return translation.text
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None

    def translate_from_deepl(self, text, lang_from = 'auto', lang_to='en'):
        if TranslateLanguageEnum.LanguageCodeToNameMap is None:
            return None

        text = text.strip()
        if (len(text) == 0) or ((lang_from != 'auto') and (lang_from.upper() not in TranslateLanguageEnum.LanguageCodeToNameMap)) or (lang_to.upper() not in TranslateLanguageEnum.LanguageCodeToNameMap):
            return None
        
        if lang_from != 'auto':
            lang_from = lang_from.upper()

        try:
            translation = DeepLTranslator(
                translate_str=text,
                source_lang=lang_from,
                target_lang=lang_to.upper(),
                translate_mode=TranslateModeType.SENTENCES
            ).translate()

            if 'result' in translation:
                return translation['result']
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None

    def translate_from_libretranslate(self, text, lang_from = 'auto', lang_to='en', api_key = ''):
        if self.LIBRE_LANGUAGES is None:
            return None
        
        text = text.strip()
        if (len(text) == 0) or (lang_from not in self.LIBRE_LANGUAGES) or (lang_to not in self.LIBRE_LANGUAGES):
            return None

        url = 'https://libretranslate.com'
        translate_url = url + "/translate"
        headers = {
            'user-agent': self.get_random_useragent(),
            'origin': url,
            'referer': url
        }

        payload = {
            'q': text, 
            'source': lang_from,
            'target': lang_to
        }

        if len(api_key) > 0:
            payload['api_key'] = api_key

        try:
            resp = requests.post(translate_url, data=payload, headers=headers, proxies=self.get_random_proxy(), timeout=30)
            if resp.status_code == requests.codes.ok:
                return None

            body = resp.json()
            if 'translatedText' in body:
                return body['translatedText']
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None

    def translate_from_ulion_google(self, text, lang_from = 'auto', lang_to='en'):
        if self.ULION_GOOGLE_LANGUAGES is None:
            return None

        text = text.strip()

        if self.ULION_GOOGLE_LANGUAGES is None:
            return None

        if (len(text) == 0) or ((lang_from != 'auto') and (lang_from not in self.ULION_GOOGLE_LANGUAGES)) or (lang_to not in self.ULION_GOOGLE_LANGUAGES):
            return None
        
        try:
            host_url, api_url = self.get_random_base_google_host()
            ats._google.host_url = host_url
            ats._google.api_url = api_url
            return ats.google(text, from_language=lang_from, to_language=lang_to, proxies=self.get_random_proxy())
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None
    
    def translate_from_ulion_bing(self, text, lang_from = 'auto', lang_to='en'):
        if self.ULION_BING_LANGUAGES is None:
            return None

        text = text.strip()
        if (len(text) == 0) or ((lang_from != 'auto') and (lang_from not in self.ULION_BING_LANGUAGES)) or (lang_to not in self.ULION_BING_LANGUAGES):
            return None

        try:
            return ats.bing(text, from_language=lang_from, to_language=lang_to, proxies=self.get_random_proxy())
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None

    def translate_from_ulion_yandex(self, text, lang_from = 'auto', lang_to='en'):
        if self.ULION_YANDEX_LANGUAGES is None:
            return None

        text = text.strip()
        if (len(text) == 0) or ((lang_from != 'auto') and (lang_from not in self.ULION_YANDEX_LANGUAGES)) or (lang_to not in self.ULION_YANDEX_LANGUAGES):
            return None

        try:
            return ats.yandex(text, from_language=lang_from, to_language=lang_to, proxies=self.get_random_proxy())
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None

    def translate_from_ulion_iciba(self, text, lang_from = 'auto', lang_to='en'):
        if self.ULION_ICIBA_LANGUAGES is None:
            return None

        text = text.strip()
        if (len(text) == 0) or ((lang_from != 'auto') and (lang_from not in self.ULION_ICIBA_LANGUAGES)) or (lang_to not in self.ULION_ICIBA_LANGUAGES):
            return None

        try:
            return ats.iciba(text, from_language=lang_from, to_language=lang_to, proxies=self.get_random_proxy())
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None

    def translate_from_ulion_alibaba(self, text, lang_from = 'auto', lang_to='en'):
        if self.ULION_ALIBABA_LANGUAGES is None:
            return None

        text = text.strip()
        if (len(text) == 0) or ((lang_from != 'auto') and (lang_from not in self.ULION_ALIBABA_LANGUAGES)) or (lang_to not in self.ULION_ALIBABA_LANGUAGES):
            return None

        try:
            return ats.alibaba(text, from_language=lang_from, to_language=lang_to, proxies=self.get_random_proxy())
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None

    def translate_from_ulion_sogou(self, text, lang_from = 'auto', lang_to='en'):
        if self.ULION_SOGOU_LANGUAGES is None:
            return None

        text = text.strip()

        if self.ULION_SOGOU_LANGUAGES is None:
            return None

        if (len(text) == 0) or ((lang_from != 'auto') and (lang_from not in self.ULION_SOGOU_LANGUAGES)) or (lang_to not in self.ULION_SOGOU_LANGUAGES):
            return None

        try:
            return ats.sogou(text, from_language=lang_from, to_language=lang_to, proxies=self.get_random_proxy())
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None

    def translate_from_ulion_youdao(self, text, lang_from = 'auto', lang_to='en'):
        if self.ULION_YOUDAO_LANGUAGES is None:
            return None

        text = text.strip()
        
        if self.ULION_YOUDAO_LANGUAGES is None:
            return None
        
        if (len(text) == 0) or ((lang_from != 'auto') and (lang_from not in self.ULION_YOUDAO_LANGUAGES)) or (lang_to not in self.ULION_YOUDAO_LANGUAGES):
            return None

        try:
            return ats.youdao(text, from_language=lang_from, to_language=lang_to, proxies=self.get_random_proxy())
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None

    def translate_from_ulion_baidu(self, text, lang_from = 'auto', lang_to='en'):
        if self.ULION_BAIDU_LANGUAGES is None:
            return None

        text = text.strip()

        if self.ULION_BAIDU_LANGUAGES is None:
            return None

        if (len(text) == 0) or ((lang_from != 'auto') and (lang_from not in self.ULION_BAIDU_LANGUAGES)) or (lang_to not in self.ULION_BAIDU_LANGUAGES):
            return None

        try:
            return ats.baidu(text, from_language=lang_from, to_language=lang_to, proxies=self.get_random_proxy())
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None

    def translate_from_deep_goole(self, text, lang_from = 'auto', lang_to='en'):
        if self.DEEP_GOOGLE_LANGUAGES is None:
            return None

        text = text.strip()
        if (len(text) == 0) or ((lang_from != 'auto') and (lang_from not in self.DEEP_GOOGLE_LANGUAGES)) or (lang_to not in self.DEEP_GOOGLE_LANGUAGES):
            return None

        try:
            translation = GoogleTranslator(source=lang_from, target=lang_to, proxies=self.get_random_proxy())
            return translation.translate(text=text)
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None

    def translate_from_deep_mymemory(self, text, lang_from = 'auto', lang_to='en'):
        if self.DEEP_GOOGLE_LANGUAGES is None:
            return None

        text = text.strip()
        if (len(text) == 0) or ((lang_from != 'auto') and (lang_from not in self.DEEP_GOOGLE_LANGUAGES)) or (lang_to not in self.DEEP_GOOGLE_LANGUAGES):
            return None

        try:
            return MyMemoryTranslator(source=lang_from, target=lang_to, proxies=self.get_random_proxy()).translate(text=text)
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None

    def translate_html_from_ulion_google(self, html_text, lang_to='en', threadpool = None):
        try:
            return ats.translate_html(html_text, threadpool = threadpool, translator=ats.google, to_language=lang_to, proxies=self.get_random_proxy())
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None

    def translate_html_from_ulion_bing(self, html_text, lang_to='en', threadpool = None):
        try:
            return ats.translate_html(html_text, translator=ats.bing, to_language=lang_to, proxies=self.get_random_proxy())
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None

    def translate_html_from_ulion_alibaba(self, html_text, lang_to='en', threadpool = None):
        try:
            return ats.translate_html(html_text, translator=ats.alibaba, to_language=lang_to, proxies=self.get_random_proxy())
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None

    def translate_html_from_ulion_yandex(self, html_text, lang_to='en', threadpool = None):
        try:
            return ats.translate_html(html_text, translator=ats.yandex, to_language=lang_to, proxies=self.get_random_proxy())
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None

    def translate_html_from_ulion_iciba(self, html_text, lang_to='en', threadpool = None):
        try:
            return ats.translate_html(html_text, translator=ats.iciba, to_language=lang_to, proxies=self.get_random_proxy())
        except:
            logging.error(traceback.format_tb(sys.exc_info()[2]))
            pass
        return None