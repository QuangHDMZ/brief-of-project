class TokenizerMsg:
    def __init__(self, code, lang, data):
        self.code = code
        self.data = data
        self.lang = lang
        self.extras = {}

    def set_extras(self, extras):
        self.extras = extras

    def update_extras(self, extras):
        self.extras.update(extras)

    def toJSON(self):
        return {
            'code': self.code, 
            'msg': {
                'lang': self.lang, 
                'data': self.data,
                'extras': self.extras
            }
        }

class TranslateMsg:
    def __init__(self, code, data):
        self.code = code
        self.data = data
        self.extras = {}

    def set_extras(self, extras):
        self.extras = extras

    def update_extras(self, extras):
        self.extras.update(extras)

    def toJSON(self):
        return {
            'code': self.code, 
            'msg': {
                'data': self.data,
                'extras': self.extras
            }
        }

class CommonMsg:
    def __init__(self, code, data):
        self.code = code
        self.data = data
        self.extras = {}

    def set_extras(self, extras):
        self.extras = extras

    def update_extras(self, extras):
        self.extras.update(extras)

    def toJSON(self):
        return {
            'code': self.code, 
            'msg': {
                'data': self.data,
                'extras': self.extras
            }
        }

class CategoryMsg:
    def __init__(self, code, lang, categories):
        self.code = code
        self.lang = lang
        self.categories = categories
        self.extras = {}

    def set_extras(self, extras):
        self.extras = extras

    def update_extras(self, extras):
        self.extras.update(extras)

    def toJSON(self):
        return {
            'code': self.code, 
            'msg': {
                'lang': self.lang,
                'categories': self.categories,
                'extras': self.extras
            }
        }

class HScodeMsg:
    def __init__(self, code, lang, hscodes):
        self.code = code
        self.lang = lang
        self.hscodes = hscodes
        self.extras = {}

    def set_extras(self, extras):
        self.extras = extras

    def update_extras(self, extras):
        self.extras.update(extras)

    def toJSON(self):
        return {
            'code': self.code, 
            'msg': {
                'lang': self.lang,
                'hscodes': self.hscodes,
                'extras': self.extras
            }
        }
        
class NMTTranslatorMsg:
    def __init__(self, code, text, score):
        self.code = code
        self.text = text
        self.score = score
        self.extras = {}

    def set_extras(self, extras):
        self.extras = extras

    def update_extras(self, extras):
        self.extras.update(extras)

    def toJSON(self):
        return {
            'code': self.code, 
            'msg': {
                'text': self.text,
                'score': self.score,
                'type': 0,
                'extras': self.extras
            }
        }

class ResponseMsg:
    def __init__(self, code, data):
        self.code = code
        self.data = data
        self.extras = {}

    def set_extras(self, extras):
        self.extras = extras

    def update_extras(self, extras):
        self.extras.update(extras)

    def toJSON(self):
        return {
            'code': self.code, 
            'msg': {
                'data': self.data,
                'extras': self.extras
            }
        }

class DetectBannedProductMsg:
    def __init__(self, code, lang, data):
        self.code = code
        self.lang = lang
        self.data = data
        self.extras = {}

    def set_extras(self, extras):
        self.extras = extras

    def update_extras(self, extras):
        self.extras.update(extras)

    def toJSON(self):
        return {
            'code': self.code, 
            'msg': {
                'lang': self.lang,
                'data': self.data,
                'extras': self.extras
            }
        }

class AccentTranslatorMsg:
    def __init__(self, code, text, prob):
        self.code = code
        self.text = text
        self.prob = prob
        self.extras = {}

    def set_extras(self, extras):
        self.extras = extras

    def update_extras(self, extras):
        self.extras.update(extras)

    def toJSON(self):
        return {
            'code': self.code, 
            'msg': {
                'text': self.text,
                'prob': self.prob,
                'extras': self.extras
            }
        }