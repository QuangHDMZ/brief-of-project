class ErrorMsg:
    def __init__(self, code, msg):
        self.code = code
        self.msg = msg

    def toJSON(self):
        return {'code': self.code, 'msg': self.msg}