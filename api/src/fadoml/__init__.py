import os
from sanic import Sanic, response
from CocCocTokenizer import PyTokenizer
import json
from connection_pool import ConnectionPool
from pystalk import BeanstalkClient
from sanic.log import LOGGING_CONFIG_DEFAULTS
from functools import partial
from fadoml.app.runtime.finger_print import FingerPrintModel
from fadoml.app.runtime.hscode import HscodeModel
from fadoml.app.runtime.id_card import IDCardModel
from fadoml.app.runtime.nmt_translator import NMTTranslatorModel
from fadoml.app.runtime.ocr import OCRModel
from fadoml.app.runtime.vn_accent import VnAccentModel
from fadoml.util.helper import exist_file
from fadoml.app.runtime.ban_checker import CheckerModel
from fadoml.app.runtime.category_mapping import MappingModel
from fadoml.app.runtime.captcha import CaptchaModel
from sanic.handlers import ErrorHandler
from sanic.exceptions import SanicException
from fadoml.util.constant import *
from functools import partial
from fadoml.util.helper import exist_file
from fadoml.api.helper import build_json_response
import fasttext
import logging
from fadoml.msg import error_msg
from sanic_openapi import doc, openapi2_blueprint
from onmt.translate import TranslationServer
from pyvi import ViTokenizer
from fadoml.app.util.fprint_vectorizer import FPrintVectorizer
import keras
import gcld3
from fastlangid.langid import LID
from khmernltk import word_tokenize
from fugashi import Tagger as JpTagger

__version__ = '1.0.0'
__author__ = "DevTeam"

class CustomErrorHandler(ErrorHandler):
    def default(self, request, exception):
        if not isinstance(exception, SanicException):
            logging.error(exception.message)
        return super().default(request, exception)

def load_nmt_translator(dataPath):
    config_file = "{}/nmt_translator/config.json".format(dataPath)
    server_model = TranslationServer()
    
    server_model.start(config_file)
    return server_model

def load_finger_print(dataPath):
    classifier = keras.models.load_model("{}/finger_print/17022022_classify_fprint_xception299.h5".format(dataPath))
    verifier = keras.models.load_model("{}/finger_print/08022022_verify_fprint_binary.h5".format(dataPath))
    vectorizer = FPrintVectorizer()

    fprint = {
        "classifier": classifier,
        "verifier": verifier,
        "vectorizer": vectorizer,
    }

    return fprint

def load_fastext(dataPath):
    modelFilePath = "{}/lid.176.bin".format(dataPath)
    if exist_file(modelFilePath) == False:
        return None
    return fasttext.load_model(modelFilePath)

def load_gcld3():
    return gcld3.NNetLanguageIdentifier(min_num_bytes=0, max_num_bytes=1000)

def load_fastlangid(dataPath):
    modelFilePath = "{}/lid.176.bin".format(dataPath)
    if exist_file(modelFilePath) == False:
        return None
    return LID(custom_model=modelFilePath)

def load_api_json(dataPath):
    jsonFilePath = "{}/appid/apikey.json".format(dataPath)
    data = None
    if exist_file(jsonFilePath) == True:
        with open(jsonFilePath) as json_file:
            data = json.load(json_file)
            json_file.close()
    return data

def load_category_json(dataPath):
    jsonFilePath = "{}/category.json".format(dataPath)
    data = None
    if exist_file(jsonFilePath) == True:
        with open(jsonFilePath) as json_file:
            data = json.load(json_file)
            json_file.close()
    return data

def create_beanstalk_client(b_host, b_port):
    client = BeanstalkClient(b_host, b_port)
    return client

def load_beanstalk_pool(b_host, b_port, thread_num=10):
    pool = ConnectionPool(create=partial(BeanstalkClient, b_host, b_port), max_size=thread_num)
    return pool

def load_cococ_tokenizer(haveInstance=False):
    if haveInstance == False:
        return None
    
    return PyTokenizer(load_nontone_data=True)

def load_vi_tokenizer():
    return ViTokenizer

def load_khmern_tokenizer():
    raw_text = "ខួបឆ្នាំទី២៨! ២៣ តុលា ស្មារតីផ្សះផ្សាជាតិរវាងខ្មែរនិងខ្មែរ ឈានទៅបញ្ចប់សង្រ្គាម នាំពន្លឺសន្តិភាព និងការរួបរួមជាថ្មី"
    word_tokenize(raw_text, return_tokens=True)

def load_japanese_tokenizer():
    tagger = JpTagger('-Owakati')
    text = "麩菓子は、麩を主材料とした日本の菓子。"
    tagger.parse(text)
    return tagger

def create_main_app(config=None):
    modified_config = LOGGING_CONFIG_DEFAULTS
    modified_config["loggers"]["sanic.root"]["level"] = "ERROR"
    
    app_name = __name__.replace(".", "_")
    app = Sanic(name=app_name, log_config=modified_config)
    base_path = os.path.abspath(os.path.join(__file__, os.pardir, os.pardir))
    app.ctx.app_base_path = base_path

    if config is not None:
        app.config.update(config)

    @app.route("/")
    async def index(request):
        errorMsg = error_msg.ErrorMsg(1, DEFAULT_SERVER_MSG)
        return await build_json_response(errorMsg.toJSON())

    @app.route("/favicon.ico", methods=("GET", "POST"))
    async def favicon(request):
        favicon_file_path = "{}/favicon.ico".format(request.app.config["data_path"])
        try:
            headers = {'Cache-Control': "no-cache"}
            return await response.file(os.path.abspath(favicon_file_path), headers=headers)
        except FileNotFoundError:
            return response.text("base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNkYPhfDwAChwGA60e6kgAAAABJRU5ErkJggg==", 404)

    handler = CustomErrorHandler()
    app.error_handler = handler

    from fadoml.api import common
    from fadoml.api.v1 import machine as machine_v1
    app.blueprint(common.bp)
    app.blueprint(machine_v1.bp)
    app.blueprint(openapi2_blueprint)

    return app

def create_checker_app(config=None):
    model = CheckerModel("checker")
    app = model.get_app(config)
    return app

def create_mapping_app(config=None):
    model = MappingModel("mapping")
    app = model.get_app(config)
    return app

def create_hscode_app(config=None):
    model = HscodeModel("hscode")
    app = model.get_app(config)
    return app

def create_nmt_translator_app(config=None):
    model = NMTTranslatorModel("nmt_translator")
    app = model.get_app(config)
    return app

def create_finger_print_app(config=None):
    model = FingerPrintModel("finger_print")
    app = model.get_app(config)
    return app

def create_ocr_app(config=None):
    model = OCRModel("ocr")
    app = model.get_app(config)
    return app

def create_vn_accent_app(config=None):
    model = VnAccentModel("vn_accent")
    app = model.get_app(config)
    return app

def create_captcha_app(config=None):
    model = CaptchaModel("captcha")
    app = model.get_app(config)
    return app

def create_id_card_app(config=None):
    model = IDCardModel("id_card")
    app = model.get_app(config)
    return app

__all__ = [
    'create_main_app',
    'create_checker_app',
    'create_mapping_app',
    'create_hscode_app',
    'load_cococ_tokenizer',
    'load_api_json',
    'create_id_card_app',
    'load_fastext',
    'api',
    'load_beanstalk_pool',
    'gtranslator',
    'dtranslator',
    'ptranslator',
    'utranslator',
    'app',
    'util',
    'test',
    'load_category_json',
    'load_nmt_translator',
    'create_nmt_translator_app',
    'load_vi_tokenizer',
    'load_finger_print',
    'create_finger_print_app',
    'create_ocr_app',
    'load_gcld3',
    'create_vn_accent_app',
    'load_fastlangid',
    'create_captcha_app',
    'load_khmern_tokenizer',
    'load_japanese_tokenizer'
]
