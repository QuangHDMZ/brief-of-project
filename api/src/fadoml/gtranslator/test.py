from fadoml.gtranslator.constants import LANGCODES, LANGUAGES, DEFAULT_SERVICE_URLS, DEFAULT_USER_AGENT
from fadoml.gtranslator.client import Translator
import time

if __name__ == '__main__':
    textStr = "Sau khái niệm kubernetes là gì chúng ta hãy đến với chức năng của nó. Kubernetes quản lý các docker host và cấu trúc container cluster."
    langFrom = "vi"
    langTo = "en"

    try:
        gtranslator = Translator(service_urls=DEFAULT_SERVICE_URLS, user_agent = DEFAULT_USER_AGENT)
        translation = gtranslator.translate(textStr, src = langFrom, dest = langTo)
        print(translation.text)
    except:
        pass

    time.sleep(5)