"""Free Google Translate API for Python. Translates totally free of charge."""
__all__ = 'Translator',
__version__ = '4.0.0-rc.1'


from fadoml.gtranslator.client import Translator
from fadoml.gtranslator.constants import LANGCODES, LANGUAGES, DEFAULT_USER_AGENT, DEFAULT_SERVICE_URLS  # noqa
