#!/bin/sh

ROOT_PATH=`pwd`
EXTENSION_PATH="${ROOT_PATH}/ext"
mkdir -p ${EXTENSION_PATH}
DATE_TIME=`date +%s`

VENDOR_NAME="vendors"
PYTHON="${ROOT_PATH}/${VENDOR_NAME}/bin/python3"
PIP="${ROOT_PATH}/${VENDOR_NAME}/bin/pip3"
source ${ROOT_PATH}/${VENDOR_NAME}/bin/activate

install_fadoml_pip() {
	echo "Install fado pip"

	mkdir -p ${ROOT_PATH}/tmp
	export TMPDIR="${ROOT_PATH}/tmp"

	OS_TARGET=`uname -s`
	if [ "${OS_TARGET}" == "Darwin" ]; then 
		brew install libomp
		export SKLEARN_NO_OPENMP=TRUE
	fi
	
	cd ${ROOT_PATH}
	${PYTHON} -m pip install wheel
	${PYTHON} -m pip install -r requirements.txt
}

install_fadoml() {
    echo "Building ML server"
	cd ${ROOT_PATH}
	${PYTHON} setup.py install
}

install_fastext() {
	echo "Build NMT Tokenizer"
	cd ${EXTENSION_PATH}
	git clone --recurse https://github.com/facebookresearch/fastText.git
	cd ${EXTENSION_PATH}/fastText
	${PYTHON} setup.py install
	wget "https://dl.fbaipublicfiles.com/fasttext/supervised-models/lid.176.bin"
	mv ${EXTENSION_PATH}/fastText/lid.176.bin ${ROOT_PATH}/data
}

cleanup() {
    echo "Cleanup temp"
	cd ${ROOT_PATH}
	sudo rm -rf ${EXTENSION_PATH}
	rm -rf ${ROOT_PATH}/build
	rm -rf ${ROOT_PATH}/dist
	rm -rf ${ROOT_PATH}/src/FadoML.egg-info
}

install_bazel() {
	echo "Build bazel"
	sudo apt install apt-transport-https curl gnupg
	curl -fsSL https://bazel.build/bazel-release.pub.gpg | gpg --dearmor > bazel.gpg
	sudo mv bazel.gpg /etc/apt/trusted.gpg.d/
	echo "deb [arch=amd64] https://storage.googleapis.com/bazel-apt stable jdk1.8" | sudo tee /etc/apt/sources.list.d/bazel.list
	sudo apt update && sudo apt install bazel
	sudo apt update && sudo apt install bazel-3.7.2
}

fix_tensorflow() {
	echo "Build tensorflow"
	cd ${EXTENSION_PATH}
	wget "https://github.com/tensorflow/tensorflow/archive/refs/tags/v2.5.0.tar.gz"
	tar xf v2.5.0.tar.gz
	cd ${EXTENSION_PATH}/tensorflow-2.5.0/
	./configure
	bazel build //tensorflow/tools/pip_package:build_pip_package
	${EXTENSION_PATH}/tensorflow-2.5.0/bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp/tensorflow_pkg
	${PIP} install /tmp/tensorflow_pkg/tensorflow-2.5.0-tags.whl
}

fix_keras() {
	file_util=${ROOT_PATH}/${VENDOR_NAME}/lib/python3.8/site-packages/keras/utils/generic_utils.py
	cp -rf ${file_util} ${file_util}.${DATE_TIME}
	sed '12d' ${file_util} > ${file_util}.t0
	sed '11 a from tensorflow.python.keras.utils.generic_utils import populate_dict_with_module_objects, to_snake_case' ${file_util}.t0 > ${file_util}.t1
	mv ${file_util}.t1 ${file_util}
}

install_fadoml_pip
#install_bazel
#fix_tensorflow
#install_fastext
install_fadoml
#fix_keras
cleanup