#!/bin/sh

ROOT_PATH=`pwd`
cd ${ROOT_PATH}

VENDOR_NAME="vendors"
PYTHON="${VENDOR_NAME}/bin/python3"
PIP="${VENDOR_NAME}/bin/pip3"
source ${ROOT_PATH}/${VENDOR_NAME}/bin/activate

echo "Run tracking worker ..."
${PYTHON} ${ROOT_PATH}/worker/tracking_translate_worker.py "$@"