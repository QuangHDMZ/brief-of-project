# Doc

## 1. Diagram
https://docs.google.com/document/d/1I9SrQ7F0TmaQ1ozxrS9skll790zPicsTmr8p4grQ0MI/edit

## 2. Powerpoint
https://docs.google.com/presentation/d/1-1Ll9t7BQHF8ihCzKBse5IZ00ZW6z_FJ/edit#slide=id.p1

## 1. A. Hung train
https://www.youtube.com/watch?v=AyTlz0QTMUE
# Data (Server IP:192.168.10.178)
/data_science/data_science/data/v1/fingerprint/fprint_19052022

# References
https://www.tensorflow.org/api_docs/python/tf/keras/Sequential#predict_classes
https://keras.io/api/applications/resnet/#resnet50-function
https://www.tensorflow.org/install/gpu
https://paperswithcode.com/paper/fpd-m-net-fingerprint-image-denoising-and

## Analytics
http://fingerprints.handresearch.com/dermatoglyphics/fingerprints-behavior-personality-temperaments.htm
- Thông tin về phân phối chủng vân tay và xác định tích cách (the 4 classic temperaments) theo 4 personality groups

http://fingerprints.handresearch.com/dermatoglyphics/10-facts-about-radial-loop-fingerprints.htm
- Tại fact 8 có thông tin liên quan đến mối liên kết giữa vân tay và tính cách hướng nội hướng ngoại: http://fingerprints.handresearch.com/dermatoglyphics/10-facts-about-radial-loop-fingerprints.htm#:~:text=RADIAL%20LOOP%20FACT%208%3A%20The%20Big%20Five%20personality%20dimensions%3F

http://fingerprints.handresearch.com/dermatoglyphics/fingerprints-ethnic-differences-races.htm
- Sự khác nhau giữa vân tay giữa các chủng tộc trên thế giới

http://fingerprints.handresearch.com/dermatoglyphics/fingerprints-diseases-syndromes.htm
- Thông tin về sự liên quan giữa vân tay với một số vấn đề y khoa

http://fingerprints.handresearch.com/dermatoglyphics/fingerprints-classification.htm
- Thông tin về lịch sử và cha đẻ của ngành vân tay học

http://www.dermatoglyphics.com/sof/
- Có chứa một vài thông tin hữu ích về giúp cho việc phân loại, chủng dấu vân tay:
- Chương 2: http://www.dermatoglyphics.com/sof/#CHAPTER_II
- Chương 3: http://www.dermatoglyphics.com/sof/#CHAPTER_III
- Chương 4: http://www.dermatoglyphics.com/sof/#CHAPTER_IV
- Tuy nhiên phần lớn thông tin còn lại là các nội dung không liên quan đến phân loại tính cách, tâm lý của con người dựa trên vân tay, mà chủ yếu là các vấn đề liên quan đến ứng dụng của chúng trong lập và hành pháp.
