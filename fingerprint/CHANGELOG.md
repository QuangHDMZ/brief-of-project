[07/06/2022]
- Thêm file DERMATOGLYPHIC_v2.pdf vào src/fingerprint/personality

[03/06/2022]
- Thêm file code check fp có label NONE

[30/5/2022]
- Thêm file research DEMATOGLYPHIC.pdf vào /src/fingerprint/personality

[27/5/2022]
- Processing xong mặt sau (thin + singularities) xong các tập ( Anh Hòa, Anh Trí, Anh Huy, Git)
- Hoàn thành đánh label trên các tập đã processing
- Tiến hành train trên data tổng hợp từ 4 nguồn và đã update kết quả test trên dữ liệu từng tập ở report(https://docs.google.com/spreadsheets/d/1PNk_xZ_P6M-4wXoqGoSlg71Sd6fbYx2AbDTH4G5edFI/edit#gid=1277433093)
- Thêm file research Fingerprint Type for Personality.docx vào /src/fingerprint/personality

[10/5/2022]
- Tập của Trí 
    - Loại tất cả không phải mặt sau
    - Loại hình có cả mặt trước và mặt sau
    - Còn 235 hình/ 24500 hình
- Tập của fado
    - Đã label được 228 hình/ 340 hình của fado (Còn lại là None)

- Thêm document cho Personality Researching
# LIST TO DO
- Label /data_science/data_science/data/v1/fingerprint/git/FP_img.zip
- Label /data_science/data_science/data/v1/fingerprint/AHoa/2.data_finger_mores.zip
- Label tập từ Trí

# NEXT TO DO
[11/5/2022]
- Chờ confirm tập fado đã label
- Preprocess tập của Trí 
    - Extract fingperint từ 235 hình
    - Preprocess fingerprint qua wokalib
- Preprocess lại tập Hưng, lưu trước khi chạy core và delta để làm input train
- Label tập của Trí
