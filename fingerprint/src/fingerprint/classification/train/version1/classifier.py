import pickle
import keras
from keras import models
from keras.models import Sequential
from keras.layers import Conv2D,MaxPool2D,Flatten,Dense,Activation,Dropout,BatchNormalization,MaxPooling2D
import keras.losses
import time
from fingerp.cli import current_app
import numpy as np
from keras.callbacks import CSVLogger
from fingerp.util import *
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from keras.utils import np_utils
# from tensorflow.keras.applications.resnet50 import ResNet50
import tensorflow as tf
from focal_loss import BinaryFocalLoss

EPOCHS = 1
CATE_NAME = "loop"
MODEL_NAME = "{}_epochs_{}_resnet50_feature_extraction_focal_loss".format(CATE_NAME, EPOCHS)
LR = 2e-5
BS = 32

def train():
    data_path = current_app["config"]["data_path"]
    train_path = "{}/fingerprint/preprocessed_img/{}/train".format(data_path, CATE_NAME)
    log_path = "{}/fingerprint/log/{}.log".format(data_path, MODEL_NAME)
    model_path = "{}/fingerprint/model/{}".format(data_path, MODEL_NAME)

    X = pickle.load(open("{}/X.pkl".format(train_path),'rb'))
    y = pickle.load(open("{}/y.pkl".format(train_path),'rb'))

    X = scale_and_reshape_for_keras(X)
    n_outputs = len(np.unique(y))
    y = np_utils.to_categorical(y, n_outputs)

    create_model_4(X, y, n_outputs, log_path, model_path)


def create_model_4(X, y, n_outputs, log_path, model_path):
    baseModel = tf.keras.applications.ResNet50(include_top=False, weights='imagenet', input_shape=(X.shape[1:]))

    # Lấy output của ConvNet trong baseModel
    fcHead = baseModel.layers[80].output

    # Flatten trước khi dùng FCs
    fcHead = Flatten(name='flatten')(fcHead)

    # Thêm FC
    # fcHead = Dense(512, activation='relu')(fcHead)
    # fcHead = Dropout(0.3)(fcHead)
    # fcHead = Dense(512, activation='relu')(fcHead)
    # fcHead = Dropout(0.3)(fcHead)

    # Output layer với softmax activation
    fcHead = Dense(n_outputs, activation='softmax')(fcHead)

    # Xây dựng model bằng việc nối ConvNet của Resnet50 và fcHead
    model = tf.keras.Model(inputs=baseModel.input, outputs=fcHead)

    # augmentation cho training data
    # aug_train = ImageDataGenerator(rescale=1./255, rotation_range=30, width_shift_range=0.1, height_shift_range=0.1, shear_range=0.2, 
    #                         zoom_range=0.2, horizontal_flip=True, fill_mode='nearest')

    # freeze 
    # for layer in baseModel.layers:
    #     layer.trainable = False
        
    model.compile(loss=BinaryFocalLoss(gamma=2), optimizer='adam',
                  metrics=['accuracy'])

    # model.summary()

    # Training model
    csv_logger = CSVLogger(log_path, separator=',', append=False)
    history = model.fit(X, y, use_multiprocessing=True, workers=10, validation_split=0.2, epochs=EPOCHS, batch_size=BS, verbose=1, callbacks=[csv_logger])

    # # unfreeze
    # for layer in baseModel.layers:
    #     layer.trainable = True
        
    # #Fine tuning, training model
    # csv_logger = CSVLogger(log_path, separator=',', append=False)
    # history = model.fit(X, y, use_multiprocessing=True, workers=10, validation_split=0.2, epochs=EPOCHS, batch_size=BS, verbose=1, callbacks=[csv_logger])
    
    model.save(model_path)

def create_model_3(X, y, n_outputs, log_path, model_path):
    #resnet50 model fine-tuning
    mdl = tf.keras.applications.ResNet50(include_top=False, weights='imagenet', input_shape=(X.shape[1:]))

    last_layer = mdl.layers[-1].output
    last_layer = keras.layers.Flatten()(last_layer)

    mdl = tf.keras.Model(mdl.inputs, outputs=last_layer)

    for layer in mdl.layers:
        layer.trainable = False

    # mdl.summary()

    #Create our model
    model = Sequential()
    model.add(mdl)
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.3))
    model.add(Dense(512,activation='relu'))
    model.add(Dropout(0.3))
    model.add(Dense(n_outputs,activation='softmax'))

    model.compile(loss='binary_crossentropy', optimizer='adam',
                  metrics=['accuracy'])

    # model.summary()

    #Training model
    csv_logger = CSVLogger(log_path, separator=',', append=False)
    history = model.fit(X, y, use_multiprocessing=True, workers=10, validation_split=0.2, epochs=EPOCHS, batch_size=BS, verbose=1, callbacks=[csv_logger])
    model.save(model_path)

    plot_history(history, EPOCHS)


def create_model_2(X, y, n_outputs, log_path, model_path):
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding="same", input_shape=(X.shape[1:])))
    model.add(Activation("relu"))
    model.add(BatchNormalization())
    model.add(Dropout(0.2))
    model.add(Conv2D(32, (3, 3), padding="same"))
    model.add(Activation("relu"))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(0.2))
    model.add(Conv2D(64, (3, 3), padding="same"))
    model.add(Activation("relu"))
    model.add(BatchNormalization())
    model.add(Conv2D(64, (3, 3), padding="same"))
    model.add(Activation("relu"))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(0.2))
    model.add(Flatten())
    model.add(Dense(512))
    model.add(Activation("relu"))
    model.add(Dropout(0.2))
    model.add(BatchNormalization())
    model.add(Dense(n_outputs))
    model.add(Activation("softmax"))

    # Compile model, chỉ rõ hàm loss_function nào được sử dụng, phương thức đùng để tối ưu hàm loss function.
    model.compile(optimizer='adam',
                # loss=keras.losses.BinaryCrossentropy(from_logits=True),
                loss='binary_crossentropy',
                # loss='sparse_categorical_crossentropy',
                # loss='categorical_crossentropy',
                metrics=['accuracy'])

    # Thực hiện train model với data
    csv_logger = CSVLogger(log_path, separator=',', append=False)
    history = model.fit(X, y, use_multiprocessing=True, workers=10, validation_split=0.2, batch_size=32, epochs=EPOCHS, verbose=1, callbacks=[csv_logger])
    # history = model.fit(X, y, validation_split=0.2, batch_size=32, epochs=epochs, verbose=1, callbacks=[csv_logger])
    model.save(model_path)

    plot_history(history, EPOCHS)


def create_model_1(X, y, n_outputs, log_path, model_path):
    # Model = Sequential() để nói cho keras là ta sẽ xếp các layer lên nhau để tạo model. 
    # Ví dụ input -> CONV -> POOL -> CONV -> POOL -> FLATTEN -> FC -> OUTPUT
    model = Sequential()

    # Thêm Convolutional layer với 64 kernel, kích thước kernel 3*3
    # dùng hàm relu làm activation và chỉ rõ input_shape cho layer đầu tiên
    model.add(Conv2D(64, (3,3), activation = 'relu', input_shape=(X.shape[1:])))
    model.add(MaxPool2D(pool_size=(2,2)))

    model.add(Conv2D(64, (3,3), activation = 'relu'))
    model.add(MaxPool2D(pool_size=(2,2)))

    # Flatten layer chuyển từ tensor sang vector
    model.add(Flatten())

    # Thêm Fully Connected layer với 128 nodes và dùng hàm relu
    model.add(Dense(128, activation = 'relu'))
    model.add(Dropout(0.5))

    # Output layer với 2 node và dùng softmax function để chuyển sang xác xuất.
    model.add(Dense(n_outputs, activation = 'softmax'))

    # Compile model, chỉ rõ hàm loss_function nào được sử dụng, phương thức đùng để tối ưu hàm loss function.
    model.compile(optimizer='adam',
                # loss=keras.losses.BinaryCrossentropy(from_logits=True),
                loss='binary_crossentropy',
                # loss='sparse_categorical_crossentropy',
                # loss='categorical_crossentropy',
                metrics=['accuracy'])

    # Thực hiện train model với data
    csv_logger = CSVLogger(log_path, separator=',', append=False)
    history = model.fit(X, y, use_multiprocessing=True, workers=10, validation_split=0.2, batch_size=32, epochs=EPOCHS, verbose=1, callbacks=[csv_logger])
    # history = model.fit(X, y, validation_split=0.2, batch_size=32, epochs=epochs, verbose=1, callbacks=[csv_logger])
    model.save(model_path)

    plot_history(history, EPOCHS)

def scale_and_reshape_for_keras(X):
        # Calculate easier
        X = X.astype("float32") / 255.0

        # Dữ liệu input cho mô hình convolutional neural network là 1 tensor 4 chiều (N, W, H, D)
        # X = X.reshape(X.shape[0], X.shape[1], X.shape[2] , 1)
        X = np.stack((X,)*3, axis=-1)   

        return X
   

def predict():
    data_path = current_app["config"]["data_path"]
    test_path = "{}/fingerprint/preprocessed_img/{}/test".format(data_path, CATE_NAME)
    model_path = "{}/fingerprint/model/{}".format(data_path, MODEL_NAME)

    X = pickle.load(open("{}/X.pkl".format(test_path),'rb'))
    y = pickle.load(open("{}/y.pkl".format(test_path),'rb'))

    X = scale_and_reshape_for_keras(X)

    model = keras.models.load_model(model_path)

    # # Kiểm tra trên tập test
    # results = model.evaluate(X, y)

    # print('Test loss: {:4f}'.format(results[0]))
    # print('Test accuracy: {:4f}'.format(results[1]))

    y_hat = model.predict(X)
    y_pred = np.argmax(y_hat, axis=1)

    cr = classification_report(y, y_pred)
    print(cr)
   
    cm = confusion_matrix(y, y_pred)
    print(cm)

if __name__ =='__main__':
    train()