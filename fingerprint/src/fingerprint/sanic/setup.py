from setuptools import setup

setup(
    install_requires=[
        "sanic",
        "opencv-python",
        "tensorflow"
    ],
    extras_require={
    },
)