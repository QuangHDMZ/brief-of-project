import os
import numpy as np
from app import *
import argparse
from sanic import Sanic
from sanic.response import json,text
from sanic import Blueprint
import aiofiles
from datetime import datetime
import tensorflow as tf
import numpy as np
import cv2
#constant
MIN_IMG_SIZE = 299

map_dict = {0: 'ARCH',
            1: 'RL',
            2: 'UL',
            3: 'WCDI',
            4: 'WE',
            5: 'WPL',
            6: 'WST',}

data_path = "{}/data/".format(os.getcwd())
data_upload = "{}/upload/".format(os.getcwd())
def resize(img, size, interpolation=cv2.INTER_NEAREST):
    resized = cv2.resize(img, (size, size), interpolation=interpolation)
    return resized

def vectorize(img):
    img = cv2.imread(img, cv2.IMREAD_COLOR)
    new_arr = resize(img, MIN_IMG_SIZE)
    new_arr =new_arr/255.0
    new_arr = np.array(new_arr)
    new_arr = new_arr.reshape(-1, MIN_IMG_SIZE, MIN_IMG_SIZE, 3)
    return new_arr

def load_vec_model(img):
    model =  App['config']["model"]
    vectorizer = vectorize(img)
    classifier = model
    return (vectorizer, classifier)

App = {}
def main() -> None:
    
    global App
    App = {
    "config": {
        "data_path": data_path,
        "data_upload": data_upload,
        "model": load_model(data_path+"model/29-10.h5")
    }
    }
    
    app = Sanic('demo')
    # bp = Blueprint(__name__, url_prefix="/api")
    @app.route("/fingerprint", methods=["post"])
    async def preprocessImg(request):
        
        file = request.files['image'][0]
        uploadFileTmp = App['config']["data_upload"] +  file.name
        async with aiofiles.open(uploadFileTmp, 'wb') as fwritter:
                    await fwritter.write(file.body)
        fwritter.close()
        img,model = load_vec_model(uploadFileTmp)
        probability = model.predict(img)
        prediction=probability.argmax()

        return json({"label":map_dict [prediction],
                     "probability": probability.max()*100})

    app.run(host="0.0.0.0", port=8000)
if __name__ == '__main__':
    main()