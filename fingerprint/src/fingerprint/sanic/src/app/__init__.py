import tensorflow as tf

__version__ = '1.0.0'
def load_model(path):
    model = tf.keras.models.load_model(path)
    return model

__all__ = [
    "load_model"
]
