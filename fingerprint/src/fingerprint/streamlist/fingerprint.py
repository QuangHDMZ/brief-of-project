from os import WNOWAIT
import cv2
import numpy as np
import streamlit as st
import tensorflow as tf
from tensorflow.keras.preprocessing.image import img_to_array, load_img
import time

@st.cache(allow_output_mutation=True)
def get_model():
    model = tf.keras.models.load_model('/home/hdquang/Desktop/Miczone/fingerprint/TensorFlow-Streamlit/saved_model/Xception299.h5')
    return model  
### load file
uploaded_file = st.file_uploader("Choose a image file")

map_dict = {0: 'ARCH',
            1: 'RL',
            2: 'UL',
            3: 'WCDI',
            4: 'WE',
            5: 'WPL',
            6: 'WST',}


if uploaded_file is not None:
  

    # Convert the file to an opencv image.
    file_bytes = np.asarray(bytearray(uploaded_file.read()), dtype=np.uint8)
    opencv_image = cv2.imdecode(file_bytes, 1)
    opencv_image = cv2.cvtColor(opencv_image, cv2.COLOR_BGR2RGB)
    resized = cv2.resize(opencv_image,(299,299),interpolation=cv2.INTER_NEAREST)
    
    # Now do something with the image! For example, let's display it:
  
    resized = resized/255.0
    st.image(opencv_image, channels="RGB")
    img_reshape = resized[np.newaxis,...]
    st.write(img_reshape.shape)
    Genrate_pred = st.button("Generate Prediction")    
    if Genrate_pred:
        with st.spinner('Wait for it...'):
            start = time.time()
            model = get_model()
            prediction = model.predict(img_reshape).argmax()
            end = time.time()
            st.success('Done!')
        st.title("Predicted Label for the image is {}".format(map_dict [prediction]))
        st.title("Time elapsed: ")
        st.write(end-start)