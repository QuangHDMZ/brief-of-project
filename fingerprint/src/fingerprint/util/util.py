import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sn
import numpy as np

def plot_history(history, epochs):
    # list all data in history
    print(history.history.keys())

    # summarize history for accuracy
    plt.figure(figsize = (8,4.5),dpi=100)
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.show()

    # summarize history for loss
    plt.figure(figsize = (8,4.5),dpi=100)
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.show()

def plot_confusion_matrix(matrix, labels):
    df_cm = pd.DataFrame(matrix, index = [i for i in labels],
                    columns = [i for i in labels])

    plt.figure(figsize = (10,7))
    sn.heatmap(df_cm, annot=True)
    plt.show()


def write_txt(data, file_path, separator=", "):
    with open(file_path, 'w') as file:
        for item in data:
            file.write("{}{}".format(item, separator))