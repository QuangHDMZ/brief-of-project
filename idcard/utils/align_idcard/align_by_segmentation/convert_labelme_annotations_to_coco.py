# import package
import labelme2coco

# set directory that contains labelme annotations and image files
labelme_folder = "/home/hongdta/mydata/fado/wkidcard/tri"

# set export dir
export_dir = "train_test_split"

# set train split rate
train_split_rate = 0.85

# convert labelme annotations to coco
labelme2coco.convert(labelme_folder, export_dir, train_split_rate)


# # import functions
# from labelme2coco import get_coco_from_labelme_folder, save_json

# # set labelme training data directory
# labelme_train_folder = "/home/hongdta/mydata/fado/wkidcard/tri"

# # set labelme validation data directory
# labelme_val_folder = "/home/hongdta/mydata/fado/wkidcard/tri"

# # set path for coco json to be saved
# export_dir = "train_test_split_"

# # create train coco object
# train_coco = get_coco_from_labelme_folder(labelme_train_folder)

# # export train coco json
# save_json(train_coco.json, export_dir+"train.json")

# # create val coco object
# val_coco = get_coco_from_labelme_folder(labelme_val_folder, coco_category_list=train_coco.json_categories)

# # export val coco json
# save_json(val_coco.json, export_dir+"val.json")