import numpy as np
import os

#Paths
this_path = os.path.dirname(os.path.abspath(__file__))
classes_file_path = os.path.join(this_path,"classes.txt")

def fixOneFile(fileName,num_of_class):
    # try:
    with open(os.path.join(this_path,fileName),'r+') as label_file:
        lines_in_file = label_file.read().splitlines()
        label_file.seek(0)
        true_label = fileName.split(".")[0]
        for index in range(len(lines_in_file)):

            component_in_lines = lines_in_file[index].split(" ")                
            component_in_lines[0] = str(index % num_of_class)                

            for in_lines in range(len(component_in_lines)):
                label_file.write(component_in_lines[in_lines])
                if in_lines != len(component_in_lines) - 1:
                    label_file.write(" ")
            if index != len(lines_in_file) - 1:
                    label_file.write("\n")
        label_file.truncate()
    # except:
    #     print(fileName)
        

def fixAllFile(path,classes_file_path):
    num_of_class = 0
    with open(os.path.join(this_path,classes_file_path),'r') as class_file:
        lines_in_file = class_file.read().splitlines()
        num_of_class = len(lines_in_file)
    for file_name in os.listdir(path):
        if file_name.endswith(".txt") and file_name != "classes.txt":
            fixOneFile(file_name,num_of_class)

if __name__ == "__main__":
    fixAllFile(this_path,classes_file_path)
