# Woka IDCARD

# Flow
## Diagram
flow_idcard.png

## 1. Extract avatar

## 2. OCR 

## 3. Extract fingerprint

# Data

Server ubuntu@192.168.10.178:/data_science/data_science/data/v1/idcard

# Model
Server ubuntu@192.168.10.178:/data_science/data_science/data/final_models

# References
## Hướng dẫn cách setup tham số + cấu trúc data để train yolov5

https://www.miai.vn/2021/03/02/thu-lam-bac-si-chuan-doan-x-quang-cung-yolo-v5-phan-1-2/

https://www.miai.vn/2021/03/03/thu-lam-bac-si-chuan-doan-x-quang-cung-yolo-v5-phan-2-2/

- models
    - detect 4 corners (use align idcard)
    - classify idcard
    - detect objects on front side of old idcard
    - detect objects on front side of new idcard
    - detect fingerprint on back side of idcard
    - ocr (turning vietocr)