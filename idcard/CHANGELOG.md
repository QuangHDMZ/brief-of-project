

[20/06/2022]

- train xong model align 60 epoch accuracy 0.93
- fix được trường hợp bị lỗi từ model cũ 
- ảnh nghiêng kết quả vẫn k tốt vì k có data train dạng như vậy

[10/06/2022]
-   rotate fingerprint after extracting from id card

[09/06/2022]
-   synthesis data cũ và mới (2747 images)
-   agumentation data align (13188 images) rotate 0 , 90, 180, 270, brightness, Blur
-   training model using yolov5


[08/06/2022]

- Lọc cccd(Sau,trước) từ tập data A Trí 
- hoàn thành label align( A Hòa, A Trí(2000), Slash)
- fix label data 3 tập
- synthesis data 3 tâp
- prepare code augumentation  

[07/06/2022]

-  Lý label 600/1000 data từ a Trí
-  Phú label 200/450 data từ A Hòa

[06/06/2022]
- Sau khi predict segmentation thì chưa align được
- Trong trường hợp align được thì ảnh k giống như ảnh cũ được train cho nhưng model sau như là OCR, Avatar
- 
[03/06/2022]

- đưa image sau khi crop về trục ngang ox hoặc oy

[02/06/2022]

- Thêm resize cho input VietOCR theo config (dataset->image_height =32) của base dòng 32. (https://github.com/pbcquoc/vietocr/blob/d6b75172d4a16071c2944855dbd9e88fb8d98640/config/base.yml)
- Sửa phần covert kênh màu cho binary classification id card.
- Update và visualize preprocess pipeline
- Crop id card sau khi sử dụng model segmentation (done)

[01/06/2022]

- Sửa và viết luồng biến đổi ảnh
- Research model segmentation cho IDcard để giải quyết trường hợp align lỗi
- Đánh label + train thử sử dụng thư viện Detectron2

[31/05/2022]

- Tạo luồng để debug avatar và VietOCR: /ocr/test/predict_ocr.ipynb và /avatar/predict_avatar.ipynb
- đã chạy predict 4 coner xong từ 3 tập ( A Hòa: 472 image, Slash: 594 image, A Trí: 6636 image) 
- dự kiến sẽ đánh xong khoảng 3000 image ( 3 ngày) 


[30/05/2022]

- Chạy align data từ anh Trí tập group_2 Vinh đã cluster
- Thay đổi preprocess và thay đổi model (dùng lại model pretrained của VietOCR): ocr/preprocess (chưa ghép vào API)
- Link tải model pretrained: https://drive.google.com/uc?id=1nTKlEog9YFK74kPyX0qLwCWi60_YHHk4
- Link yml của model: https://github.com/pbcquoc/vietocr/blob/master/config/vgg-seq2seq.yml


[27/05/2022]

-   Cluster data anh Trí để loại dữ liệu nhiễu (có thể xử dụng group 1 và group 2):
-   Data sau khi cluster: /data_science/data_science/data/v1/fprint/fprint/fprint_19052022/fprint_tri/Tri_cmnd_vinh_processed

Dữ liệu có thể sử dụng từ anh Trí:
1.  File pkl là file chứa feature của hình gồm key (tên file) và value (extracted features của file) 
2. Folder group_1: chứa hình group 1
2. Folder group_2: chứa hình group 2

-   Restruct repo
-   Chạy align data từ anh Hòa để kiểm trả những image k align được đi đánh label
-   Chạy align data từ anh Trí tập group_1 Vinh đã cluster

