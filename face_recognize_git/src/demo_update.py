import streamlit as st
from deepface import DeepFace
from deepface.basemodels import Facenet
from deepface.basemodels import ArcFace
from deepface.commons import functions
import os
import glob
import pickle
import codecs
import numpy as np
import pandas as pd
import mysql.connector
import argparse
from util.load_model import *
from util.disable_gpu_tf import *
from sklearn.metrics import classification_report
import faiss
import cv2
import concurrent.futures
from test_video import video_predict
from test_image import image_predict
import tensorflow as tf
from tensorflow.keras.preprocessing.image import load_img, save_img, img_to_array
from tensorflow.keras.applications.imagenet_utils import preprocess_input
from tensorflow.keras.preprocessing import image
from multiprocessing import cpu_count
from collections import Counter
import os
from random import random, randint
from mlflow import log_metric, log_param, log_artifacts, log_artifact,log_dict
from util.checkTableExists import checkTableExists
from util.evaluation_matrix import matrix_calculator
import json
from util.iresnet import get_model
import torch
# create form input
st.title('Face recoginize')
print(tf.config.list_physical_devices('GPU'))
tab1, tab2 = st.tabs(["register custom dataset", "inference"])

with tab2:
    st.header("inference")
    with st.form("my_form_inference"):
        st.write("Inside the form")
        db_name = st.text_input('database_name')
        password_db = st.text_input("password_db")
        # folder_data = st.text_input('folder_data')
        detection = st.selectbox(
            'Model face detection',
            ('opencv', 'ssd', 'mtcnn', 'retinaface', 'mediapipe'))
        recognition = st.selectbox(
            'Model face recognition',
            ("VGG-Face", "Facenet", "Facenet512", "OpenFace", "DeepFace", "DeepID", "ArcFace"))
        normalize = st.selectbox(
            "normalize_types", ("base", "Facenet", "Facenet2018", "VGGFace", "VGGFace2", "ArcFace"))
        allow_detection = st.selectbox("allow_detection", ("True", "False"))
        threshold = st.text_input('threshold (default = 0.7)')
        devices = st.selectbox("devices", ("GPU", "CPU"))
        # Every form must have a submit button.
        image_test = st.text_input("image_test")
        video_test = st.text_input("video_test")
        submitted = st.form_submit_button("Submit")
        if submitted:
            # st.write("The current path_folder is ", folder_data)
            st.write("The current db name is", db_name)
            st.write("The current pass_db name is", password_db)
            st.write("Model face detection is", detection)
            st.write("Model face recognition is", recognition)
            st.write("Normalize is", normalize)
            st.write("Allow detection", allow_detection)
            st.write("devices", devices)
            if devices == "CPU":
                disable_gpu_tf(False)
            if allow_detection == "True":
                allow_detection = True
            else:
                allow_detection = False
            with st.spinner('Wait for inference'):
                index = faiss.read_index("{}_vector.index".format(db_name))
                with open('{}_mapping.json'.format(db_name), 'r') as fp:
                    mapping = json.load(fp)
                model = load_model_facenet(model_recognition=recognition)
                input_shape = functions.find_input_shape(model)
                def feature_extractor(img_path, representations=None, allow_detection=True,return_region = False):

                    img = cv2.imread(img_path)
                    # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                    if allow_detection == True:
                        img,box = functions.preprocess_face(img=img, target_size=(
                            input_shape[1], input_shape[0]), enforce_detection=True, detector_backend=detection,return_region=True)
                    else:
                        img = cv2.resize(img, (input_shape[1], input_shape[0]))
                        img = image.img_to_array(img)
                        img = np.expand_dims(img, axis=0)
                        img /= 255
                    img = functions.normalize_input(img, normalization=normalize)
                    embedding = model.predict(img, verbose=1)[0, :]
                    representation = []
                    representation.append(img_path)
                    representation.append(embedding)
                    if representations == None and return_region == False:
                        return embedding
                    # return representation
                    if representations == None and return_region == True:
                        return embedding,box
                    representations.append(representation)


                if image_test != "":
                    embeded,box = feature_extractor(image_test, allow_detection=allow_detection,return_region=True)
                    # st.write(image_test)
                    if threshold=="":
                        result = image_predict(
                        img_path=image_test, embeded=embeded, model_register=index, mapping=mapping,threshold=0.7,box = box)
                    else:
                        result = image_predict(
                        img_path=image_test, embeded=embeded, model_register=index, mapping=mapping,threshold=float(threshold),box= box)
                    # st.write("user is: ", result)
                    st.image(result,"results")
                if video_test != "":
                    video_predict(video_path=video_test, model_detection=detection, model_recoginize=model,
                                normalization=normalize, model_register=index, mapping=mapping)
               

with tab1:
    st.header("register/evaluate custom dataset")
    with st.form("my_form"):
        st.write("Inside the form")
        db_name = st.text_input('database_name')
        password_db = st.text_input("password_db")
        folder_data = st.text_input('folder_data_register')
        folder_data_validation = st.text_input('folder_data_validation')
        detection = st.selectbox(
            'Model face detection',
            ('opencv', 'ssd', 'mtcnn', 'retinaface', 'mediapipe'))
        recognition = st.selectbox(
            'Model face recognition',
            ("VGG-Face", "Facenet", "Facenet512", "OpenFace", "DeepFace", "DeepID", "ArcFace","ArcFace_pytorch"))
        normalize = st.selectbox(
            "normalize_types", ("base", "Facenet", "Facenet2018", "VGGFace", "VGGFace2", "ArcFace"))
        allow_detection = st.selectbox("allow_detection", ("True", "False"))
        threshold = st.text_input('threshold (default = 0.7)')
        devices = st.selectbox("devices", ("GPU", "CPU"))
        # Every form must have a submit button.
        # image_test = st.text_input("image_test")
        # video_test = st.text_input("video_test")
        submitted = st.form_submit_button("Submit")
        
        if submitted:
            st.write("The current path_folder register is ", folder_data)
            st.write("The current path_folder validation is ", folder_data_validation)
            st.write("The current db name is", db_name)
            st.write("The current pass_db name is", password_db)
            st.write("Model face detection is", detection)
            st.write("Model face recognition is", recognition)
            st.write("Normalize is", normalize)
            st.write("Allow detection", allow_detection)
            st.write("devices", devices)
            if devices == "CPU":
                disable_gpu_tf(False)
            if allow_detection == "True":
                allow_detection = True
            else:
                allow_detection = False
                
            mydb = mysql.connector.connect(
                host="localhost",
                user="root",
                password="{}".format(password_db)
            )
            mycursor = mydb.cursor()
            try:
                mycursor.execute("CREATE DATABASE face_db")
            except Exception as e:
                print(e)

            classes_dir = os.listdir(folder_data)

            st.write("so users la: ", len(classes_dir))
            if recognition == "ArcFace_pytorch":
                model = get_model("r100", fp16=False)
                model.load_state_dict(torch.load("/home/admin_ds/Desktop/globeltech/face_recoginize/model (2).pt"))
                model.eval()
            else:
                model = load_model_facenet(model_recognition=recognition)
            st.write("complete load model recoginition")
            if recognition == "ArcFace_pytorch":
                input_shape = (112,112)
            else:
                input_shape = functions.find_input_shape(model)

            @torch.no_grad()
            def inference(net, img):
                feat = net(img).numpy()
                return feat

            # input_shape = (160, 160)

            def feature_extractor(img_path, representations=None, allow_detection=True):

                img = cv2.imread(img_path)

                if recognition == "ArcFace_pytorch":
                    if allow_detection == True:
                        img = functions.preprocess_face(img=img, target_size=(input_shape[1], input_shape[0]), enforce_detection=True, detector_backend=detection)
                        img = img*255
                        img = img.reshape(112,112,3)
                        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                        img = np.transpose(img, (2, 0, 1))
                        img = torch.from_numpy(img).unsqueeze(0).float()
                        img.div_(255).sub_(0.5).div_(0.5)
                    else:
                        img = cv2.resize(img,(112,112))
                        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                        img = np.transpose(img, (2, 0, 1))
                        img = torch.from_numpy(img).unsqueeze(0).float()
                        img.div_(255).sub_(0.5).div_(0.5)
                    embedding = inference(model,img)
                    embedding = embedding.reshape(512)
                else:
                # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                    if allow_detection == True:
                        img = functions.preprocess_face(img=img, target_size=(
                            input_shape[1], input_shape[0]), enforce_detection=True, detector_backend=detection)
                    else:
                        img = cv2.resize(img, (input_shape[1], input_shape[0]))
                        img = image.img_to_array(img)
                        img = np.expand_dims(img, axis=0)
                        img /= 255
                    img = functions.normalize_input(img, normalization=normalize)
                    embedding = model.predict(img, verbose=1)[0, :]
                representation = []
                representation.append(img_path)
                representation.append(embedding)
                if representations == None:
                    return embedding
                # return representation
                representations.append(representation)

            with st.spinner('Wait for feature_extractor'):
                representations = []
                futures = []
                # executor = concurrent.futures.ThreadPoolExecutor(max_workers=20)
                all_image_path = []
                list_image_path = []
                list_image_label = []
                data_types = []
                if folder_data_validation !="":
                    for classes in classes_dir:
                        try:
                            for image_path in glob.glob(folder_data + "/{}/*.*".format(classes)):
                                all_image_path.append(image_path)
                                try:
                                    feature_extractor(
                                        image_path, representations, allow_detection)
                                    list_image_path.append(image_path)
                                    list_image_label.append(classes)
                                    data_types.append("train")
                                except:
                                    pass
                            for image_path_val in glob.glob(folder_data_validation + "/{}/*.*".format(classes)):
                                all_image_path.append(image_path_val)
                                try:
                                    feature_extractor(
                                        image_path_val, representations, allow_detection)
                                    list_image_path.append(image_path)
                                    list_image_label.append(classes)
                                    data_types.append("test")
                                except:
                                    pass
                        except:
                            pass
                else:
                    for classes in classes_dir:
                        try:
                            for image_path in glob.glob(folder_data + "/{}/*.*".format(classes))[0:5]:
                                all_image_path.append(image_path)
                                try:
                                    feature_extractor(
                                        image_path, representations, allow_detection)
                                    list_image_path.append(image_path)
                                    list_image_label.append(classes)
                                    data_types.append("train")
                                except:
                                    pass
                            for image_path_val in glob.glob(folder_data + "/{}/*.*".format(classes))[5:]:
                                all_image_path.append(image_path_val)
                                try:
                                    feature_extractor(
                                        image_path_val, representations, allow_detection)
                                    list_image_path.append(image_path)
                                    list_image_label.append(classes)
                                    data_types.append("test")
                                except:
                                    pass
                        except:
                            pass

                st.write(len(list_image_path))
                st.write(len(list_image_label))

                # for future in concurrent.futures.as_completed(futures):
                #     item = future.result()
                #     representations.append(item)

                st.write("complete feature_extractor")

                embeddings = []
                for i in range(0, len(representations)):
                    embedding = representations[i][1]
                    embeddings.append(embedding)

                embeddings = np.array(embeddings, dtype='f')

                mydb = mysql.connector.connect(
                    host="localhost",
                    user="root",
                    password="{}".format(password_db),
                    database="face_db"
                )
                if checkTableExists(mydb, db_name) == True:
                    mycursor = mydb.cursor()
                    mycursor.execute("DROP TABLE {}".format(db_name))
                    mycursor.execute(
                        "CREATE TABLE {} (id_name VARCHAR(255),image_name VARCHAR(255), embedded TEXT(255),data_type VARCHAR(255))".format(db_name))
                else:
                    mycursor = mydb.cursor()
                    mycursor.execute(
                        "CREATE TABLE {} (id_name VARCHAR(255),image_name VARCHAR(255), embedded TEXT(255),data_type VARCHAR(255))".format(db_name))

                for i in range(0, len(list_image_path)):
                    path = os.path.normpath(list_image_path[i])
                    path_split = path.split(os.sep)
                    embedded_base64 = codecs.encode(pickle.dumps(
                        embeddings[i], protocol=pickle.HIGHEST_PROTOCOL), "base64").decode('latin1')
                    sql = '''INSERT INTO {} (id_name, image_name, embedded, data_type) VALUES ('{}', '{}', '{}','{}')'''.format(db_name,
                                                                                                                                path_split[-2], path_split[-1], embedded_base64, data_types[i])
                    print(sql)
                    mycursor.execute(sql)
                    mydb.commit()
                st.success('Done!')
                st.write("complete create database")
                mydb = mysql.connector.connect(
                    host="localhost",
                    user="root",
                    password="{}".format(password_db),
                    database="face_db"
                )
                mycursor = mydb.cursor()
                query = '''select * from {}'''.format(db_name)
                df = pd.read_sql(query, mydb)
                df_train = df[df.data_type == "train"]
                df_test = df[df.data_type == "test"]
                st.write("training set: ", len(df_train))
                st.write("test set: ", len(df_test))
                label_train = []
                embeddings_train = []
                for i in range(0, len(df_train)):
                    embeddings_train.append(pickle.loads(codecs.decode(
                        df_train.embedded.iloc[i].encode('latin1'), "base64")))
                    label_train.append(df_train.id_name.iloc[i])

                label_test = []
                embeddings_test = []
                for i in range(0, len(df_test)):
                    embeddings_test.append(pickle.loads(codecs.decode(
                        df_test.embedded.iloc[i].encode('latin1'), "base64")))
                    label_test.append(df_test.id_name.iloc[i])

                embeddings_train = np.array(embeddings_train)
                embeddings_test = np.array(embeddings_test)

                st.write("shape of embeded train: ", embeddings_train.shape)
                st.write("shape of embeded test: ", embeddings_test.shape)

                # FaceNet output is 128 dimensional vector
                dimensions = int(embeddings_train.shape[1])
                metric = 'euclidean'
                if metric == 'euclidean':
                    index = faiss.IndexFlatL2(dimensions)
                elif metric == 'cosine':
                    index = faiss.IndexFlatIP(dimensions)
                faiss.normalize_L2(embeddings_train)
                index.add(embeddings_train)
                # index.train(embeddings_train)
                # Log an artifact (output file)

                faiss.write_index(index, "{}_vector.index".format(db_name))
                Y_pre = []
                for embed in embeddings_test:
                    target_representation = np.array(embed, dtype='f')
                    target_representation = np.expand_dims(
                        target_representation, axis=0)
                    faiss.normalize_L2(target_representation)
                    distances, neighbors = index.search(target_representation, 4)
                    a_list = [e for i, e in enumerate(
                        label_train) if i in neighbors[0]]
                    dict_result = Counter(a_list)
                    if max(dict_result.values()) != 1:
                        Y_pre.append(max(dict_result, key=dict_result.get))
                    else:
                        Y_pre.append(label_train[neighbors[0][0]])
                    # Y_pre.append(label_train[neighbors[0][0]])

                eval_matrix = matrix_calculator(Y_label=label_test, Y_pre=Y_pre)
                st.write("accuracy: ", eval_matrix["accuracy"])
                st.write("recall: ", eval_matrix["recall"])
                st.write("precision: ", eval_matrix["precision"])
                st.write("F1: ", eval_matrix["f1_score"])
                st.write(classification_report(label_test,Y_pre))
                # print(classification_report(label_test,Y_pre))

                # log_dict(dictionary, "data.json")
                log_param("model_detection", detection)
                log_param("model_recognition", recognition)
                log_param("db_name", db_name)
                log_param("normalize", normalize)
                
                # Log a metric; metrics can be updated throughout the run
                log_metric("accuracy", eval_matrix["accuracy"])
                log_metric("recall", eval_matrix["recall"])
                log_metric("precision", eval_matrix["precision"])
                log_metric("F1", eval_matrix["f1_score"])
                log_metric("total_images", len(all_image_path))
                log_metric("num_image_detect_error", len(
                    all_image_path)-len(list_image_path))
                log_artifact("{}_vector.index".format(db_name), "model_faiss")

                mapping = {k: v for k, v in enumerate(label_train)}
                with open('{}_mapping.json'.format(db_name), 'w') as fp:
                    json.dump(mapping, fp)
                # with open('{}_mapping.json'.format(db_name), 'r') as fp:
                #     mapping = json.load(fp)
                log_dict(mapping, "mapping.json")
                # if image_test != "":
                #     embeded = feature_extractor( image_test, allow_detection=allow_detection)
                #     st.write(image_test)
                #     if threshold=="":
                #         result = image_predict(
                #         img_path=image_test, embeded=embeded, model_register=index, mapping=mapping,threshold=0.7)
                #     else:
                #         result = image_predict(
                #         img_path=image_test, embeded=embeded, model_register=index, mapping=mapping,threshold=float(threshold))
                #     st.write("user is: ", result)
                # if video_test != "":
                #     video_predict(video_path=video_test, model_detection=detection, model_recoginize=model,
                #                 normalization=normalize, model_register=index, mapping=mapping)