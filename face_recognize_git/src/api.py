from flask import Flask
from flask import request
from flask import Flask, request, jsonify
import os
from util.load_model import *
# import faiss
from deepface.commons import functions
from deepface.detectors import FaceDetector
from util.load_model import *
from util.disable_gpu_tf import *
import json
import numpy as np
import cv2
from tensorflow.keras.preprocessing import image
from deepface.commons import functions
import os
import tensorflow as tf
import time
from flask import jsonify
from util.base64image import convert_base64_to_cv_image,convert_cv_image_to_base64
import matplotlib.pyplot as plt

class ErrorMsg:
    def __init__(self, code, msg):
        self.code = code
        self.msg = msg

    def toJSON(self):
        return {'code': self.code, 'msg': self.msg}
def is_none_param(data):
    try: 
        return (data is None) or (len(data) == 0)
    except:
        return True



app = Flask(__name__)

# model = load_model_facenet("Facenet")
UPLOAD_FOLDER = "./upload"
face_detector = FaceDetector.build_model(detector_backend="retinaface")


@app.route("/face_detection", methods=["GET", "POST"])
def face_detection():
    if request.method == "POST":
        request_data = request.get_json()
        try:
          image_base64 = request_data['image']
        except:
          error_msg = ErrorMsg(1,"invalid key")
          return jsonify(error_msg.toJSON())
        img = convert_base64_to_cv_image(image_base64)
        if is_none_param(img):
          error_msg = ErrorMsg(1,"Invalid image file")
          return jsonify(error_msg.toJSON())


        results = FaceDetector.detect_faces(
                face_detector=face_detector, detector_backend="retinaface", img=img)
        # dictionary = []
        # keys = ["X","Y","W","H"]
        images_base64 = []
        if len(results)!=0:
          for result in results:
            images_base64.append(convert_cv_image_to_base64(result[0]))
              # list = ([int(i) for i in result[0]])
              # dictionary.append(dict(zip(keys,list)))
          dictFace = {"faces":images_base64}
          return json.dumps(dictFace,indent=2)
        else:
          error_msg = ErrorMsg(0,"Cant Detect Face")
          return jsonify(error_msg.toJSON())
    
      # box = {'box':box}
        # json.dumps(box, default=str)
        # json_str = json.dumps(box)
       
model_recoginize = load_model_facenet(model_recognition="Facenet")
@app.route("/face_recognize", methods=["GET", "POST"])
def face_recognize():
    if request.method == "POST":
        # if "image" not in request.files:
        #     return "there is no file1 in form!"
        request_data = request.get_json()
        try:
          face_base64 = request_data['faces']
        except:
          error_msg = ErrorMsg(1,"invalid key")
          return jsonify(error_msg.toJSON())
        target_size = (160,160)
        target_representations = []
        for face in face_base64:
          img = convert_base64_to_cv_image(face)
          if is_none_param(img):
            error_msg = ErrorMsg(1,"Invalid image file")
            return jsonify(error_msg.toJSON())
          if img.shape[0] > 0 and img.shape[1] > 0:
              factor_0 = target_size[0] / img.shape[0]
              factor_1 = target_size[1] / img.shape[1]
              factor = min(factor_0, factor_1)

              dsize = (int(img.shape[1] * factor), int(img.shape[0] * factor))
              img = cv2.resize(img, dsize)

              # Then pad the other side to the target size by adding black pixels
              diff_0 = target_size[0] - img.shape[0]
              diff_1 = target_size[1] - img.shape[1]

              img = np.pad(img, ((diff_0 // 2, diff_0 - diff_0 // 2), (diff_1 // 2, diff_1 - diff_1 // 2), (0, 0)), 'constant')
          if img.shape[0:2] != target_size:
              img = cv2.resize(img, target_size)
          img_pixels = image.img_to_array(img) #what this line doing? must?
          img_pixels = np.expand_dims(img_pixels, axis = 0)
          img_pixels /= 255 #normalize input in [0, 1]  
          target_img = functions.normalize_input(img_pixels, normalization="Facenet")
          target_representation = model_recoginize.predict(target_img)[0, :]
          target_representation = np.array(target_representation, dtype='f')
          target_representation = np.expand_dims(target_representation, axis=0)
          target_representations.append(target_representation.tolist()[0])
        
        return jsonify({"embeded":target_representations})
@app.route("/face_system", methods=["GET", "POST"])
def face_system():
    if request.method == "POST":
      request_data = request.get_json()
      try:
        image_base64 = request_data['image']
      except:
        error_msg = ErrorMsg(1,"invalid key")
        return jsonify(error_msg.toJSON())
      img = convert_base64_to_cv_image(image_base64)
      if is_none_param(img):
        error_msg = ErrorMsg(1,"Invalid image file")
        return jsonify(error_msg.toJSON())
      results = FaceDetector.detect_faces(face_detector=face_detector, detector_backend="retinaface", img=img)
      target_size = (160,160)
      target_representations = []
      for result in results:
        img = result[0]
        if img.shape[0] > 0 and img.shape[1] > 0:
            factor_0 = target_size[0] / img.shape[0]
            factor_1 = target_size[1] / img.shape[1]
            factor = min(factor_0, factor_1)

            dsize = (int(img.shape[1] * factor), int(img.shape[0] * factor))
            img = cv2.resize(img, dsize)

            # Then pad the other side to the target size by adding black pixels
            diff_0 = target_size[0] - img.shape[0]
            diff_1 = target_size[1] - img.shape[1]

            img = np.pad(img, ((diff_0 // 2, diff_0 - diff_0 // 2), (diff_1 // 2, diff_1 - diff_1 // 2), (0, 0)), 'constant')
        if img.shape[0:2] != target_size:
            img = cv2.resize(img, target_size)
        img_pixels = image.img_to_array(img) #what this line doing? must?
        img_pixels = np.expand_dims(img_pixels, axis = 0)
        img_pixels /= 255 #normalize input in [0, 1]  
        target_img = functions.normalize_input(img_pixels, normalization="Facenet")
        target_representation = model_recoginize.predict(target_img)[0, :]
        target_representation = np.array(target_representation, dtype='f')
        target_representation = np.expand_dims(target_representation, axis=0)
        target_representations.append(target_representation.tolist()[0])
    return jsonify({"embeded":target_representations})
app.run()