import faiss
from deepface.commons import functions
from deepface.detectors import FaceDetector
from util.load_model import *
from util.disable_gpu_tf import *
import json
import numpy as np
import cv2
from tensorflow.keras.preprocessing import image
import os
import tensorflow as tf

# disable_gpu_tf(cuda  = False)
# os.environ["CUDA_VISIBLE_DEVICES"]="-1"

def video_predict(video_path, model_detection, model_recoginize, normalization,model_register, mapping):

    # with open('/home/admin_ds/Desktop/globeltech/face_recoginize/mapping.json', 'r') as fp:
    #     mapping = json.load(fp)

    # restore
    # index = faiss.read_index("/home/admin_ds/Desktop/globeltech/face_recoginize/vector.index")
    # model = load_model_facenet()
    face_detector = FaceDetector.build_model(detector_backend=model_detection)

    # set text style

    fontface = cv2.FONT_HERSHEY_SIMPLEX
    fontscale = 1
    fontcolor = (0, 255, 0)
    fontcolor1 = (0, 0, 255)
    sampleNum = 10
    vidcap = cv2.VideoCapture(video_path)
    vidcap.set(cv2.CAP_PROP_FPS, 1)
    count = 0
    success = True
    i=0
    while success:
        success,capture = vidcap.read()
        target_size = (160,160)
        try:
            # disable_gpu_tf(cuda  = True)
            results = FaceDetector.detect_faces(face_detector=face_detector, detector_backend=model_detection, img=capture)
            for result in results:
                target_img = result[0]
                box = result[1]
                img = target_img
                if img.shape[0] > 0 and img.shape[1] > 0:
                    factor_0 = target_size[0] / img.shape[0]
                    factor_1 = target_size[1] / img.shape[1]
                    factor = min(factor_0, factor_1)

                    dsize = (int(img.shape[1] * factor), int(img.shape[0] * factor))
                    img = cv2.resize(img, dsize)

                    # Then pad the other side to the target size by adding black pixels
                    diff_0 = target_size[0] - img.shape[0]
                    diff_1 = target_size[1] - img.shape[1]

                    img = np.pad(img, ((diff_0 // 2, diff_0 - diff_0 // 2), (diff_1 // 2, diff_1 - diff_1 // 2), (0, 0)), 'constant')
                if img.shape[0:2] != target_size:
                    img = cv2.resize(img, target_size)
                img_pixels = image.img_to_array(img) #what this line doing? must?
                img_pixels = np.expand_dims(img_pixels, axis = 0)
                img_pixels /= 255 #normalize input in [0, 1]  
                target_img = functions.normalize_input(img_pixels, normalization=normalization)
                target_representation = model_recoginize.predict(target_img)[0, :]
                target_representation = np.array(target_representation, dtype='f')
                target_representation = np.expand_dims(target_representation, axis=0)
                faiss.normalize_L2(target_representation)
                k = 1

                distances, neighbors = model_register.search(target_representation, k)
                i = i+1
                # Vẽ hình chữ nhật quanh mặt nhận được
                cv2.rectangle(capture, (box[0], box[1]), (box[0] + box[2], box[1] + box[3]), (255, 0, 0), 2)
                if distances[0][0] <= 0.8:
                    cv2.putText(capture, "Name: " + mapping[str(neighbors[0][0])],
                                (box[0], box[1]+box[3]+30), fontface, fontscale, fontcolor, 2)
                else:
                    cv2.putText(
                        capture, "Unknown", (box[0], box[1]+box[3]+30), fontface, fontscale, fontcolor, 2)
        except Exception as e:
            print(e)
        cv2.imshow('frame', capture)
        # Check xem có bấm q hoặc trên 100 ảnh sample thì thoát
        if cv2.waitKey(100) & 0xFF == ord('q'):
            break

    vidcap.release()
    cv2.destroyAllWindows()

__all__ = [
    "video_predict"
]
