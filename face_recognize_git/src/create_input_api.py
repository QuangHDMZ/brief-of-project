from util.base64image import convert_base64_to_cv_image,convert_cv_image_to_base64
import argparse
import json

parser = argparse.ArgumentParser(description='path image')
parser.add_argument("--path_image", nargs=1, help="string")
args = parser.parse_args()

path_image = args.path_image[0]

base64 = convert_cv_image_to_base64(img = path_image,imread=True)

input = {"image":base64}

with open('input.json', 'w') as fp:
    json.dump(input, fp)