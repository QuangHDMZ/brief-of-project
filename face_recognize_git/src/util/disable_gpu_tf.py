import tensorflow as tf
import os
def disable_gpu_tf(cuda = False):
    if cuda == False:
    # physical_devices = tf.config.list_physical_devices('GPU')
    # try:
    # # Disable all GPUS
    #     tf.config.set_visible_devices([], 'GPU')
    #     visible_devices = tf.config.get_visible_devices()
    #     for device in visible_devices:
    #         assert device.device_type != 'GPU'
    # except:
    # # Invalid device or cannot modify virtual devices once initialized.
        # pass
        os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
    else:
        os.environ["CUDA_VISIBLE_DEVICES"] = "0"

__all__ = [
    "disable_gpu_tf"
]
