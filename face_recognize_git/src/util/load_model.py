from deepface.basemodels import VGGFace, OpenFace, Facenet, Facenet512, DeepID, DlibWrapper, ArcFace
import tensorflow as tf
def load_model_facenet(model_recognition):
    if model_recognition =="Facenet":
        model = Facenet.loadModel()
    elif model_recognition == "Facenet512":
        model = Facenet512.loadModel()
    elif model_recognition == "ArcFace":
        model = ArcFace.loadModel()
    elif model_recognition == "OpenFace":
        model = OpenFace.loadModel()
    elif model_recognition =="DeepID":
        model = DeepID.loadModel()
    elif model_recognition =="Dlib":
        model = DlibWrapper.loadModel()
    else:
        model = VGGFace.loadModel()
    return model

__all__ = [
    "load_model_facenet"
]

