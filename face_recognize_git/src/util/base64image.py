
import cv2
import os
import base64
import numpy as np


def convert_cv_image_to_base64(img, imread=False, ext="jpeg"):
    try:
        if imread == True:
            img = cv2.imread(img)

        _, im_arr = cv2.imencode(".{0}".format(ext.lower()), img)
        im_bytes = im_arr.tobytes()
        im_b64 = base64.b64encode(im_bytes)
        return "data:image/{0};base64,{1}".format(ext.lower(), im_b64.decode("utf-8"))
    except:
        pass
    
    return None
def convert_base64_to_cv_image(im_b64):
    arr_im_b64 = im_b64.split(",")
    if len(arr_im_b64) < 1:
        return None

    try:
        im_bytes = base64.b64decode(arr_im_b64[1])
        im_arr = np.frombuffer(im_bytes, dtype=np.uint8)
        img = cv2.imdecode(im_arr, flags=cv2.IMREAD_COLOR)
        
        (height, width, _channel) = img.shape
        return img
    except:
        pass
    
    return None
__all__ = [
    "convert_cv_image_to_base64",
    "convert_base64_to_cv_image"
]
