from sklearn.metrics import accuracy_score
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn.metrics import f1_score

def matrix_calculator(Y_label,Y_pre):
    accuracy =  accuracy_score(Y_label, Y_pre)
    recall = recall_score(Y_label, Y_pre, average="macro")
    precision = precision_score(Y_label, Y_pre, average="macro")
    f1 =  f1_score(Y_label, Y_pre, average="macro")
    result = {"accuracy":accuracy,"recall":recall,"precision":precision,"f1_score":f1}
    return result

__all__ = [
    "matrix_calculator"
]




