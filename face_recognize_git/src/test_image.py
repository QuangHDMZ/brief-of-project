import streamlit as st
from deepface import DeepFace
from deepface.basemodels import Facenet
from deepface.basemodels import ArcFace
from deepface.commons import functions
import os
import glob
import pickle
import codecs
import numpy as np
import pandas as pd
from util.load_model import *
from util.disable_gpu_tf import *
from sklearn.metrics import classification_report
import faiss
import cv2
from test_video import video_predict
import tensorflow as tf
from tensorflow.keras.preprocessing.image import load_img, save_img, img_to_array
from tensorflow.keras.applications.imagenet_utils import preprocess_input
from tensorflow.keras.preprocessing import image
from collections import Counter
import os
fontface = cv2.FONT_HERSHEY_SIMPLEX
fontscale = 1
fontcolor = (0, 255, 0)
fontcolor1 = (0, 0, 255)
sampleNum = 10

def image_predict(img_path, embeded, model_register,mapping,threshold,box): 
    img_test = cv2.imread(img_path)
    img_test = cv2.cvtColor(img_test, cv2.COLOR_BGR2RGB )
    # img_test = cv2.resize(img_test,(480,480))
    target_representation = np.array(embeded, dtype='f')
    target_representation = np.expand_dims(
    target_representation, axis=0)
    faiss.normalize_L2(target_representation)
    distances, neighbors = model_register.search(target_representation, 4)
    a_list = [e for i, e in enumerate(mapping) if i in neighbors[0]]
    dict_result = Counter(a_list)
    print(distances[0][0])
    if distances[0][0] <= threshold:
        if max(dict_result.values()) != 1:
            result =  max(dict_result, key=dict_result.get)
        else:
            result =  (mapping[str(neighbors[0][0])])
    else:
        result =  "unknown"
    cv2.rectangle(img_test, (box[0], box[1]), (box[0] +
                box[2], box[1] + box[3]), (255, 0, 0), 2)

    cv2.putText(img_test, "Name: " + result,
                    (box[0], box[1]+box[3]+30), fontface, fontscale, fontcolor, 2)
    return img_test
__all__ = [
    "image_predict"
]
