import numpy as np
import cv2
import argparse
import os
import glob
from deepface import DeepFace
from deepface.basemodels import Facenet
from deepface.commons import functions
from util.load_model import *
import pickle
import mysql.connector
import codecs

parser = argparse.ArgumentParser(description='user name')
parser.add_argument("--user_name", nargs=1, help="string")
args = parser.parse_args()
user_name = args.user_name[0]

if os.path.isdir("./test_data/{}".format(user_name)) == False:
    os.makedirs("./test_data/{}".format(user_name))
sampleNum = 0
print(user_name)
cam = cv2.VideoCapture(0)
while(True):

    ret, img = cam.read()

    # Lật ảnh cho đỡ bị ngược
    img = cv2.flip(img, 1)

    # Kẻ khung giữa màn hình để người dùng đưa mặt vào khu vực này
    centerH = img.shape[0] // 2
    centerW = img.shape[1] // 2
    sizeboxW = 300
    sizeboxH = 400
    img_save = img.copy()
    cv2.rectangle(img, (centerW - sizeboxW // 2, centerH - sizeboxH // 2),
                  (centerW + sizeboxW // 2, centerH + sizeboxH // 2), (255, 255, 255), 5)
    cv2.imshow('frame', img)
    # Check xem có bấm q hoặc trên 100 ảnh sample thì thoát
    cv2.imwrite(
        "./test_data/{}/image_{}.jpg".format(user_name, sampleNum), img_save)
    sampleNum = sampleNum + 1
    if cv2.waitKey(100) & 0xFF == ord('q'):
        break
    elif sampleNum > 100:
        break

cam.release()
cv2.destroyAllWindows()

model = load_model_facenet()


def feature_extractor(img_path, representations):
    img = functions.preprocess_face(img=img_path, target_size=(
        160, 160), enforce_detection=True, detector_backend="retinaface")
    img = functions.normalize_input(img, normalization='Facenet')
    embedding = model.predict(img, verbose=1)[0, :]
    representation = []
    representation.append(img_path)
    representation.append(embedding)
    representations.append(representation)


representations = []
list_image_path_new = []
for image in glob.glob("./test_data/{}/*.jpg".format(user_name)):
    list_image_path_new.append(image)
    feature_extractor(image, representations)


embeddings = []
for i in range(0, len(representations)):
    embedding = representations[i][1]
    embeddings.append(embedding)

embeddings_new = np.array(embeddings, dtype='f')

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="Anhthu281120@",
    database="face_db"
)
mycursor = mydb.cursor()

for i in range(0, len(list_image_path_new)):
    path = os.path.normpath(list_image_path_new[i])
    path_split = path.split(os.sep)
    embedded_base64 = codecs.encode(pickle.dumps(
        embeddings_new[i], protocol=pickle.HIGHEST_PROTOCOL), "base64").decode('latin1')
    sql = '''INSERT INTO infor_user (id_name, image_name, embedded) VALUES ('{}', '{}', '{}')'''.format(
        path_split[-2], path_split[-1], embedded_base64)
    print(sql)
    mycursor.execute(sql)
    mydb.commit()
print("done")
