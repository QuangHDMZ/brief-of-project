import pandas as pd
import pickle
import codecs
import numpy as np
import mysql.connector
import faiss
import json
import argparse


parser = argparse.ArgumentParser(description="face")
parser.add_argument("--db_name", nargs=1, help="string", default="infor_user")
args = parser.parse_args()
db_name = args.db_name[0]

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="Anhthu281120@",
    database="face_db"
)

query = '''select * from {}'''.format(db_name)

df = pd.read_sql(query, mydb)

label = []
embeddings = []
for i in range(0, len(df)):
    embeddings.append(pickle.loads(codecs.decode(
        df.embedded.iloc[i].encode('latin1'), "base64")))
    label.append(df.id_name[i])

embeddings = np.array(embeddings)

dimensions = 128  # FaceNet output is 128 dimensional vector

metric = 'euclidean'  # euclidean, cosine

if metric == 'euclidean':
    index = faiss.IndexFlatL2(dimensions)
elif metric == 'cosine':
    index = faiss.IndexFlatIP(dimensions)
faiss.normalize_L2(embeddings)

index.add(embeddings)

mapping = {k: v for k, v in enumerate(label)}


with open('mapping.json', 'w') as fp:
    json.dump(mapping, fp)


# save
faiss.write_index(index, "{}_vector.index".format(db_name))
