import faiss
from deepface.commons import functions
from deepface.detectors import FaceDetector
from util.load_model import *
from util.disable_gpu_tf import *
import json
import numpy as np
import cv2
import os
import tensorflow as tf

# config = tf.compat.v1.ConfigProto(
#     gpu_options=tf.compat.v1.GPUOptions(allow_growth=True))
# sess = tf.compat.v1.Session(config=config)
# gpus = tf.config.experimental.list_physical_devices('GPU')
# for gpu in gpus: 
#     tf.config.experimental.set_memory_growth(gpu, True)
# print(tf.config.list_physical_devices('GPU'))
# disable_gpu_tf(cuda  = False)
# os.environ["CUDA_VISIBLE_DEVICES"]="-1"
# restore
index = faiss.read_index("/home/admin_ds/Desktop/globeltech/face_recoginize/vector.index")
# with tf.device('/cpu:0'):
model = load_model_facenet()

with open('/home/admin_ds/Desktop/globeltech/face_recoginize/mapping.json', 'r') as fp:
    mapping = json.load(fp)

# set text style
fontface = cv2.FONT_HERSHEY_SIMPLEX
fontscale = 1
fontcolor = (0, 255, 0)
fontcolor1 = (0, 0, 255)
sampleNum = 10
cam = cv2.VideoCapture(0)
i = 0
while(True):
    ret, img = cam.read()

    # Lật ảnh cho đỡ bị ngược
    img = cv2.flip(img, 1)
    # img = cv2.resize(img,(460,259))
    # Kẻ khung giữa màn hình để người dùng đưa mặt vào khu vực này
    # centerH = img.shape[0] // 2;
    # centerW = img.shape[1] // 2;
    # sizeboxW = 300;
    # sizeboxH = 400;
    # img_rec = img.copy()
    # cv2.rectangle(img_rec, (centerW - sizeboxW // 2, centerH - sizeboxH // 2),
    #               (centerW + sizeboxW // 2, centerH + sizeboxH // 2), (255, 255, 255), 5)
    target_img, box = functions.preprocess_face(img=img, target_size=(
        160, 160), enforce_detection=False, detector_backend= "retinaface", return_region=True)
    # face_detector = FaceDetector.build_model(detector_backend="ssd")
    # results = FaceDetector.detect_faces(face_detector=face_detector, detector_backend="ssd", img=img)
    # for result in results:
    #     target_img = result[0]
    #     target_img = cv2.resize(target_img,(160,160))      
    #     box = result[1]
    target_img = functions.normalize_input(target_img, normalization='Facenet')
    # with tf.device('/cpu:0'):
    target_representation = model.predict(target_img)[0, :]
    target_representation = np.array(target_representation, dtype='f')
    target_representation = np.expand_dims(target_representation, axis=0)
    faiss.normalize_L2(target_representation)
    k = 1
    distances, neighbors = index.search(target_representation, k)
    i = i+1
    # Vẽ hình chữ nhật quanh mặt nhận được
    cv2.rectangle(img, (box[0], box[1]), (box[0] +
                box[2], box[1] + box[3]), (255, 0, 0), 2)
    if distances[0][0] <= 0.8:
        cv2.putText(img, "Name: " + mapping[str(neighbors[0][0])],
                    (box[0], box[1]+box[3]+30), fontface, fontscale, fontcolor, 2)
    else:
        cv2.putText(
            img, "Unknown", (box[0], box[1]+box[3]+30), fontface, fontscale, fontcolor, 2)
    cv2.imshow('frame', img)
    # Check xem có bấm q hoặc trên 100 ảnh sample thì thoát
    if cv2.waitKey(100) & 0xFF == ord('q'):
        break
    elif sampleNum > 100:
        break

cam.release()
cv2.destroyAllWindows()
