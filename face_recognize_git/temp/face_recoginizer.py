import faiss
import argparse
from deepface import DeepFace
from deepface.basemodels import Facenet
from deepface.commons import functions
from util.load_model import *
import json
import numpy as np


parser = argparse.ArgumentParser(description='path image test')
parser.add_argument("--image_test",nargs=1,help="string")
args = parser.parse_args()
image_test  = args.image_test[0]


#restore
index = faiss.read_index("vector.index")
model = load_model_facenet()


target_img = functions.preprocess_face(img=image_test, target_size=(160, 160),enforce_detection=True,detector_backend="retinaface",return_region = False )
target_img = functions.normalize_input(target_img, normalization = 'Facenet')
target_representation = model.predict(target_img)[0,:]

target_representation = np.array(target_representation, dtype='f')
target_representation = np.expand_dims(target_representation, axis=0)
faiss.normalize_L2(target_representation)

k = 1
distances, neighbors = index.search(target_representation, k)



with open('mapping.json', 'r') as fp:
    mapping = json.load(fp)

if distances[0][0] <= 0.8:
    print("face is: ",mapping[str(neighbors[0][0])])
else:
    print("face is: Unknown")