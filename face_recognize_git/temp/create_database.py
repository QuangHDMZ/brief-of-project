from deepface import DeepFace
from deepface.basemodels import Facenet
from deepface.basemodels import ArcFace

from deepface.commons import functions
import os
import glob
import pickle
import codecs
import numpy as np
import pandas as pd
import mysql.connector
import argparse
from util.load_model import *
from util.disable_gpu_tf import *
import cv2
import concurrent.futures
from multiprocessing import cpu_count

import mlflow.keras

mlflow.keras.autolog()

parser = argparse.ArgumentParser(description='path folder data')
parser.add_argument("--path_folder", nargs=1, help="string")
parser.add_argument("--db_name", nargs=1, help="string", default="infor_user")
args = parser.parse_args()

folder_data = args.path_folder[0]
db_name = args.db_name[0]
print(db_name)

# get classes in folder
classes_dir = os.listdir(folder_data)

print("so users la: ", len(classes_dir))

list_image_path = []
list_image_label = []
for classes in classes_dir:
    for image_path in glob.glob(folder_data + "/{}/*.*".format(classes))[0:10]:
        list_image_path.append(image_path)
        list_image_label.append(classes)

print(len(list_image_path))
print(len(list_image_label))

model = load_model_facenet()
print("done")



def feature_extractor(img_path,representations):
    img = cv2.imread(img_path)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = functions.preprocess_face(img=img, target_size=(
        112, 112), enforce_detection=False, detector_backend="retinaface")
    img = functions.normalize_input(img, normalization='ArcFace')
    embedding = model.predict(img, verbose=1)[0, :]
    representation = []
    representation.append(img_path)
    representation.append(embedding)
    # return representation
    representations.append(representation)


representations = []
futures = []
# executor = concurrent.futures.ThreadPoolExecutor(max_workers=20)
for image_path in list_image_path:
    feature_extractor(image_path,representations)
    
# for future in concurrent.futures.as_completed(futures):
#     item = future.result()
#     representations.append(item)

print("complete feature_extractor")

embeddings = []
for i in range(0, len(representations)):
    embedding = representations[i][1]
    embeddings.append(embedding)

embeddings = np.array(embeddings, dtype='f')

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="Anhthu281120@",
    database="face_db"
)


def checkTableExists(dbcon, tablename):
    dbcur = dbcon.cursor()
    dbcur.execute("""
        SELECT COUNT(*)
        FROM information_schema.tables
        WHERE table_name = '{0}'
        """.format(tablename.replace('\'', '\'\'')))
    if dbcur.fetchone()[0] == 1:
        dbcur.close()
        return True

    dbcur.close()
    return False


if checkTableExists(mydb, db_name) == True:
    mycursor = mydb.cursor()
    mycursor.execute("DROP TABLE {}".format(db_name))
    mycursor.execute(
        "CREATE TABLE {} (id_name VARCHAR(255),image_name VARCHAR(255), embedded TEXT(255))".format(db_name))
else:
    mycursor = mydb.cursor()
    mycursor.execute(
        "CREATE TABLE {} (id_name VARCHAR(255),image_name VARCHAR(255), embedded TEXT(255))".format(db_name))

for i in range(0, len(list_image_path)):
    path = os.path.normpath(list_image_path[i])
    path_split = path.split(os.sep)
    embedded_base64 = codecs.encode(pickle.dumps(
        embeddings[i], protocol=pickle.HIGHEST_PROTOCOL), "base64").decode('latin1')
    sql = '''INSERT INTO {} (id_name, image_name, embedded) VALUES ('{}', '{}', '{}')'''.format(db_name,
                                                                                                path_split[-2], path_split[-1], embedded_base64)
    print(sql)
    mycursor.execute(sql)
    mydb.commit()

print("done")
