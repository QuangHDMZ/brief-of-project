import keras
from transformers import AutoTokenizer, AutoModelForSequenceClassification, Trainer, TrainingArguments, BertTokenizer, BertForSequenceClassification
from transformers import DistilBertTokenizer, DistilBertForSequenceClassification
import pickle
import json
import pandas
#load model
model_bin1 = keras.models.load_model("path")
model_bin2 = keras.models.load_model("path")
model_bin3 = keras.models.load_model("path")
model_bin4 = DistilBertForSequenceClassification.from_pretrained('path',num_labels = 374)

#load tokennizer

with open(
    "path", "rb") as handle:
    tokenizer_bin1 = pickle.load(handle)
with open(
    "path", "rb") as handle:
    tokenizer_bin2 = pickle.load(handle)
with open(
    "path", "rb") as handle:
    tokenizer_bin3 = pickle.load(handle)
    
tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-uncased')

#load label map

label_map_group2 = pd.read_csv("path",dtype='str')

label_map_group4 = pd.read_csv("path",dtype='str')

with open("/path") as json_file:
    label_map_group1 = json.load(json_file)
    
with open("path") as json_file:
    label_map_group3 = json.load(json_file)

#load info hscode
infohscode = json.load(open("path")) 




