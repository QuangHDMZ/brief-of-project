# Group 2,4

pred = model.predict(data)


def create_output2_4(pred, top_n, label_map_group, infohscode):
    temp = np.argpartition(-pred[0], top_n)
    pred_idx = temp[:top_n]
    child_hscode_list = label_map_group.iloc[pred_idx].child_hscode
    test = pred[0]
    temp = np.partition(-test, top_n)
    result = -temp[:top_n]
    top_acc = result * 100
    return dict(
        zip(zip(top_acc, child_hscode_list), [infohscode[x] for x in child_hscode_list])
    )


# group_2


def create_output1(pred, top_n, label_map_group, infohscode):

    dx = (-p[0]).argsort()[:top_n]
    data = np.round(pred[0][dx] * 100, 5)
    dict_you_want = {your_key: label_map_group1[str(your_key)] for your_key in dx}

    return dict(
        zip(
            zip(data, dict_you_want.values()),
            [infohscode[x[:-2]] for x in dict_you_want.values()],
        )
    )


# group_3
import torch

pred = model(**test_encodings)


def create_output3(pred, top_n, label_map_group, infohscode):
    predictions = torch.nn.functional.softmax(pred.logits, dim=-1)
    values, indices = predictions.data.topk(top_n)
    dict_you_want = {
        your_key: label_map_group[str(your_key)] for your_key in indices[0].numpy()
    }
    return dict(
        zip(
            zip(data, dict_you_want.values()),
            [infohscode[x[:-2]] for x in dict_you_want.values()],
        )
    )

