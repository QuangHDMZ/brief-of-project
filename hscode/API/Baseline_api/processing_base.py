# Group 1,3
import string
import re
import nltk

nltk.download("stopwords")
nltk.download("punkt")
nltk.download("wordnet")
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.preprocessing.text import Tokenizer

stop_words = set(stopwords.words("english"))
lemmatizer = WordNetLemmatizer()
MAX_SEQUENCE_LENGTH_1 = 262
MAX_SEQUENCE_LENGTH_2 = 463
MAX_SEQUENCE_LENGTH_3 = 257
MAX_SEQUENCE_LENGTH_4 = 487


def preprocessing_group1_3(sentence):
    
    tokens = word_tokenize(sentence)
    # convert to lower case
    tokens = [w.lower() for w in tokens]
    # remove punctuation from each word
    table = str.maketrans("", "", string.punctuation)
    stripped = [w.translate(table) for w in tokens]
    # remove remaining tokens that are not alphabetic
    words = [word for word in stripped if word.isdigit() == False]
    # filter out stop words
    words = [w for w in words if not w in stop_words]
    
    return " ".join(words)


def preprocessing_group2_4(sentence):
    
    tokens = word_tokenize(sentence)
    # filter out stop words
    words = [w for w in tokens if not w in stop_words]
    
    return " ".join(words)


def encoding_sentence1_2_4(new_data, tokenizer, MAX_SEQUENCE_LENGTH):
    
    sequences = tokenizer.texts_to_sequences([new_data])
    data = pad_sequences(sequences, padding="post", maxlen=MAX_SEQUENCE_LENGTH)
    
    return data


def encoding_sentence3(new_data, tokenizer, MAX_SEQUENCE_LENGTH):
    
    data = tokenizer(
        new_data,
        truncation=True,
        padding=True,
        max_length=MAX_SEQUENCE_LENGTH,
        return_tensors="pt",
    )

    return data

