from setuptools import setup

setup(
    install_requires=[
        "sanic",
        "tensorflow",
        "keras",
        "transformers",
        "torch"
    ],
    extras_require={
    },
)