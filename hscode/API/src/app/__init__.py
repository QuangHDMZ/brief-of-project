import tensorflow as tf
import keras
from transformers import AutoTokenizer, AutoModelForSequenceClassification, Trainer, TrainingArguments, BertTokenizer, BertForSequenceClassification
from transformers import DistilBertTokenizer, DistilBertForSequenceClassification
import pickle
import json
import pandas as pd


__version__ = '1.0.0'

def load_model_bin_1_2_4(path):
    model = tf.keras.models.load_model(path)
    return model

def load_model_bin_3(path):
    model = DistilBertForSequenceClassification.from_pretrained(path,num_labels = 374)
    return model


def load_tokenizer_bin_1_2_4(path,bin):
    if bin==4:
        tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-uncased')
        return tokenizer
    else: 
        with open(
        path, "rb") as handle:
        tokenizer= pickle.load(handle)
        return tokenizer




def load_tokenizer_bin_3():
    tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-uncased')
    return tokenizer

def load_label_map_1_3(path):
    with open(path) as json_file:
        label_map = json.load(json_file)
    return  label_map
        
def load_label_map_2_4(path):
    label_map = pd.read_csv(path,dtype='str')
    return  label_map

def load_infohscode(path):
    infohscode = json.load(open(path)) 
    return infohscode



__all__ = [
    "load_model_bin_1_2_4",
    "load_model_bin_3",
    "load_tokenizer_bin_1_2_4",
    "load_tokenizer_bin_3",
    "load_label_map_1_3",
    "load_label_map_2_4",
    "load_infohscode"
]
