from app import *
# Group 1,3
import string
import re
import nltk

nltk.download("stopwords")
nltk.download("punkt")
nltk.download("wordnet")
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.preprocessing.text import Tokenizer
import torch
from sanic import Sanic
from sanic.response import json,text
from sanic import Blueprint
import os
import numpy as np


stop_words = set(stopwords.words("english"))
lemmatizer = WordNetLemmatizer()

MAX_SEQUENCE_LENGTH_1 = 262
MAX_SEQUENCE_LENGTH_2 = 463
MAX_SEQUENCE_LENGTH_3 = 257
MAX_SEQUENCE_LENGTH_4 = 487


def preprocessing_group1_3(sentence):
    
    tokens = word_tokenize(sentence)
    # convert to lower case
    tokens = [w.lower() for w in tokens]
    # remove punctuation from each word
    table = str.maketrans("", "", string.punctuation)
    stripped = [w.translate(table) for w in tokens]
    # remove remaining tokens that are not alphabetic
    words = [word for word in stripped if word.isdigit() == False]
    # filter out stop words
    words = [w for w in words if not w in stop_words]
    
    return " ".join(words)


def preprocessing_group2_4(sentence):
    
    tokens = word_tokenize(sentence)
    # filter out stop words
    words = [w for w in tokens if not w in stop_words]
    
    return " ".join(words)


def encoding_sentence1_2_4(new_data, tokenizer, MAX_SEQUENCE_LENGTH):
    
    sequences = tokenizer.texts_to_sequences([new_data])
    data = pad_sequences(sequences, padding="post", maxlen=MAX_SEQUENCE_LENGTH)
    
    return data


def encoding_sentence3(new_data, tokenizer, MAX_SEQUENCE_LENGTH):
    
    data = tokenizer(
        new_data,
        truncation=True,
        padding=True,
        max_length=MAX_SEQUENCE_LENGTH,
        return_tensors="pt",
    )

    return data


def create_output2_4(pred, top_n, label_map_group, infohscode):
    temp = np.argpartition(-pred[0], top_n)
    pred_idx = temp[:top_n]
    child_hscode_list = label_map_group.iloc[pred_idx].child_hscode
    test = pred[0]
    temp = np.partition(-test, top_n)
    result = -temp[:top_n]
    top_acc = result * 100
    return dict(
        zip(zip(top_acc, child_hscode_list), [infohscode[x] for x in child_hscode_list])
    )

# group_2

def create_output1(pred, top_n, label_map_group, infohscode):
    if bin==1:
        dx = (-pred[0]).argsort()[:top_n]
        data = np.round(pred[0][dx] * 100, 5)
        dict_you_want = {your_key: label_map_group[str(your_key)] for your_key in dx}

        return dict(
            zip(
                zip(data, dict_you_want.values()),
                [infohscode[x[:-2]] for x in dict_you_want.values()],
            )
        )
    elif (bin==2) or (bin==4):
        temp = np.argpartition(-pred[0], top_n)
        pred_idx = temp[:top_n]
        child_hscode_list = label_map_group.iloc[pred_idx].child_hscode
        test = pred[0]
        temp = np.partition(-test, top_n)
        result = -temp[:top_n]
        top_acc = result * 100
        return dict(
            zip(zip(top_acc, child_hscode_list), [infohscode[x] for x in child_hscode_list])
        )
    else:
        predictions = torch.nn.functional.softmax(pred.logits, dim=-1)
        values, indices = predictions.data.topk(top_n)
        dict_you_want = {
            your_key: label_map_group[str(your_key)] for your_key in indices[0].numpy()
        }
        data = values[0].numpy()
        data = np.round(data*100,decimals=5)
        return dict(
            zip(
                zip(data, dict_you_want.values()),[infohscode[x[:-2]] for x in dict_you_want.values()],
        )
        )
        


# group_3

def create_output3(pred, top_n, label_map_group, infohscode):
    predictions = torch.nn.functional.softmax(pred.logits, dim=-1)
    values, indices = predictions.data.topk(top_n)
    dict_you_want = {
        your_key: label_map_group[str(your_key)] for your_key in indices[0].numpy()
    }
    data = values[0].numpy()
    data = np.round(data*100,decimals=5)
    return dict(
        zip(
            zip(data, dict_you_want.values()),
            [infohscode[x[:-2]] for x in dict_you_want.values()],
        )
    )

data_path = "{}/data/".format(os.getcwd())


App = {}
def main() -> None:
    
    global App
    App = {
    "config": {
        "data_path": data_path,
        "model_bin1": load_model_bin_1_2_4(data_path+"model/lstm_testquang1.h5"),
        # "model_bin2": load_model_bin_1_2_4(data_path+"model/29-10.h5"),
        "model_bin3": load_model_bin_3(data_path+"model/bert_group3"),
        # "model_bin4": load_model_bin_1_2_4(data_path+"model/29-10.h5"),
        "tokenizer_bin1": load_tokenizer_bin_1_2_4(data_path+"tokenizer/tokenizer_bin1.pickle"),
        # "tokenizer_bin2": load_model_bin_3(data_path+"model/bert_group3"),
        "tokenizer_bin3": load_tokenizer_bin_3(),
        # "tokenizer_bin4": load_model_bin_3(data_path+"model/bert_group3"),
        "label_map_bin1": load_label_map_1_3(data_path+"label_map/label_map_group1.json"),
        # "label_map_bin2": load_label_map_2_4(data_path+"label_map/label_map_group2.json"),
        "label_map_bin3": load_label_map_1_3(data_path+"label_map/label_map_group3.json"),
        # "label_map_bin4": load_label_map_2_4(data_path+"label_map/label_map_group4.json"),
        "infohscode": load_infohscode(data_path+"label_map/child_hscode.json")
        
    }
    }

    
    app = Sanic('demo')
    # bp = Blueprint(__name__, url_prefix="/api")
    @app.route("/hscode", methods=["get"])
    async def preprocessImg(request):
        textStr = request.args.get("text")

        text = preprocessing_group1_3(textStr)
        text_bin1 = encoding_sentence1_2_4(text,App["config"]["tokenizer_bin1"],MAX_SEQUENCE_LENGTH_1)
        text_bin3 = encoding_sentence3(text,App["config"]["tokenizer_bin3"],MAX_SEQUENCE_LENGTH_3)
        predict_bin_1 = App["config"]["model_bin1"].predict(text_bin1)
        predict_bin_3 = App["config"]["model_bin3"](**text_bin3)
        result_bin1 = create_output1(predict_bin_1,2,App["config"]["label_map_bin1"],App["config"]["infohscode"])
        result_bin3 = create_output3(predict_bin_3,2,App["config"]["label_map_bin3"],App["config"]["infohscode"])
        result = {"Bin_1":result_bin1,
                  "Bin_3":result_bin3}

        return json(result)

    app.run(host="0.0.0.0", port=8000)
if __name__ == '__main__':
    main()