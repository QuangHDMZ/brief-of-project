from dataclasses import dataclass
from nltk.corpus.reader.framenet import PrettyDict
import torch
import pandas as pd
import numpy as np
import tensorflow as tf

# Thu vien transformer cho Classification
from transformers import AutoTokenizer, AutoModelForSequenceClassification, Trainer, TrainingArguments, BertTokenizer, BertForSequenceClassification

# Xu ly label
from sklearn.preprocessing import LabelEncoder
from processing import preprocessing_text
from transformers import DistilBertTokenizer, DistilBertForSequenceClassification


tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-uncased')
model = DistilBertForSequenceClassification.from_pretrained('/home/hdquang/Desktop/Miczone/hscodeapi/bert_group3',num_labels = 374)

# encoding du lieu
a = "Fuji X Secrets: 142 Ways to Make the Most of Your Fujifilm X Series Camera"

sentence = preprocessing_text(a)

test_encodings = tokenizer(sentence, truncation=True, padding=True, max_length=257,return_tensors="pt")

outputs = model(**test_encodings)
# print(outputs.last_hidden_state.shape)
import torch

predictions = torch.nn.functional.softmax(outputs.logits, dim=-1)
values,indices = predictions.data.topk(5)

import json
with open("/home/hdquang/Desktop/Miczone/hscodeapi/label_map_group3.json") as json_file:
    label_map_group3 = json.load(json_file)
    
    
dict_you_want = {your_key: label_map_group3[str(your_key)] for your_key in indices[0].numpy() }

data = values[0].numpy()
data = np.round(data*100,decimals=5)
zip_iterator = zip(data, dict_you_want.values())

a_dictionary = dict(zip_iterator)

print(a_dictionary)

