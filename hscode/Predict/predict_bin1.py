import pandas as pd
import numpy as np
import tensorflow as tf
import keras
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
import pickle
import string
import re
import nltk
import json
from processing import preprocessing_text

model = keras.models.load_model(
    "/home/hdquang/Desktop/Miczone/hscodeapi/lstm_testquang1.h5"
)

MAX_SEQUENCE_LENGTH = 262  # max length of each entry (sentence), including padding

a = "JULAN Stainless Steel NFC Ring Multifunctional NFC Magic Smart Ring for Android Windows Mobile Phone"

sentence = preprocessing_text(a)

# loading tokenize
with open(
    "/home/hdquang/Desktop/Miczone/hscodeapi/tokenizer_bin1.pickle", "rb"
) as handle:
    tokenizer = pickle.load(handle)

tokenized_seq = tokenizer.texts_to_sequences(sentence)
padded_seq = pad_sequences(tokenized_seq, padding="post", maxlen=MAX_SEQUENCE_LENGTH)


predict = model.predict(padded_seq)


with open("/home/hdquang/Desktop/Miczone/hscodeapi/label_map_group1.json") as json_file:
    label_map_group1 = json.load(json_file)

dx = (-predict[0]).argsort()[:5]


data = np.round(predict[0][dx]*100,5)

dict_you_want = {your_key: label_map_group1[str(your_key)] for your_key in dx}


zip_iterator = zip(data, dict_you_want.values())

a_dictionary = dict(zip_iterator)

print(a_dictionary)

# print("hscode is:  ", label_map_group1[str(predict.argmax())])
# print(data)