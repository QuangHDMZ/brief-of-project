[28/06/2022]
- Viết preprocess cho 2.3: xử lý dữ liệu sau khi join (case 6)
[27/06/2022]
- Viết preprocess cho 2.3: xử lý dữ liệu sau khi join (case 5)
[24/06/2022] 
- Xuất hiện lỗi trong việc merge file data Amazon và mapping file (chỉ có 5 triệu dòng so với lúc trước ~ 11 triệu) (đang xử lý)
[23/06/2022]
- Viết preprocess cho 2.3: xử lý dữ liệu sau khi join (case 3)
[22/06/2022]
- Viết preprocess cho 2.3: xử lý dữ liệu sau khi join (case 3)
[21/06/2022]
- Viết preprocess cho 2.3: xử lý dữ liệu sau khi join (case 2) - tính similarity (cont.)
[20/06/2022]
- Viết preprocess cho 2.3: xử lý dữ liệu sau khi join (case 2) - tính similarity
[17/06/2022]
- Viết preprocess cho 2.3: xử lý dữ liệu sau khi join (case 2) - nhúng 'name' thành tensor
[15, 16/06/2022]
- Format lại và bổ sung mô tả cho 2.2
[14/06/2022]
- Viết preprocess cho 2.3: xử lý dữ liệu sau khi join (case 1)
[13/06/2022]
- Viết preprocess cho 2.3: xử lý dữ liệu sau khi join (trường hợp name)
[10/06/2022]
- Viết preprocess cho 2.2: join và phân tích dữ liệu
[09/06/2022]
- Viết preprocess cho 2.1.2 file 2: kiểm tra tính đồng nhất giữa child_hscode và parent_hscode, xử lý các trường hợp gặp phải
[08/06/2022]
- Viết preprocess cho 2.1.2 file 2: kiểm tra format child_hscode và parent_hscode, xử lý các trường hợp gặp phải 
[07/06/2022]
- Viết preprocess cho 2.1.2 file 1: kiểm tra phân bố độ dài trường name, xử lý các trường hợp gặp phải. (cont.)
[04/06/2022]
- Viết preprocess cho 2.1.2 file 1: kiểm tra ngôn ngữ trường name và xử lý (cont.), kiểm tra phân bố độ dài trường name, xử lý các trường hợp gặp phải.
[02/06/2022]
- Viết preprocess cho 2.1.2 file 1: kiểm tra ngôn ngữ trường name và xử lý
[01/06/2022]
- Viết preprocess cho 2.1.1 file 2: xử lý cơ bản
[31/05/2022]
- Viết preprocess cho 2.1.1 file 1: xử lý cơ bản
