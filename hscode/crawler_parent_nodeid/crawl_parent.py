from selenium import webdriver
from time import sleep
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
import io
import pandas as pd
from selenium.webdriver.common.by import By 
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import csv
from selenium.common.exceptions import InvalidSelectorException


#https://www.amazon.com/b?node=13398111

list_parent = ['4531',
 '7161079011',
 '13398111',
 '16310191',
 '381760011',
 '166835011',
 '9022394011',
 '8996314011',
 '2258019011',
 '8622799011',
 '6066126011',
 '706813011',
 '4991425011',
 '6518738011',
 '4522',
 '3741271']
name = '2258019011'
list_parent_link = ['https://www.amazon.com/b/?node=166836011&ref_=Oct_d_odnav_d_166835011_0&pd_rd_w=1Ld53&pf_rd_p=0f6f8a08-29ea-497e-8cb4-0ccf91422740&pf_rd_r=9EWKGWT2GGAE8ARCT01E&pd_rd_r=d119dd9e-f5f9-4941-a976-e8c41344a74f&pd_rd_wg=m7X7J',
'https://www.amazon.com/b/?node=1272297011&ref_=Oct_d_odnav_d_166835011_1&pd_rd_w=1Ld53&pf_rd_p=0f6f8a08-29ea-497e-8cb4-0ccf91422740&pf_rd_r=9EWKGWT2GGAE8ARCT01E&pd_rd_r=d119dd9e-f5f9-4941-a976-e8c41344a74f&pd_rd_wg=m7X7J',
'https://www.amazon.com/b/?node=8457150011&ref_=Oct_d_odnav_d_166835011_2&pd_rd_w=1Ld53&pf_rd_p=0f6f8a08-29ea-497e-8cb4-0ccf91422740&pf_rd_r=9EWKGWT2GGAE8ARCT01E&pd_rd_r=d119dd9e-f5f9-4941-a976-e8c41344a74f&pd_rd_wg=m7X7J']
list_bbn = [166836011,1272297011,8457150011]


# data = pd.read_csv("node_id_of_child_hscode_small_250.csv", usecols = ["stt","node_id"])
# print("Nhap number: ")
# number = int(input())
# list_node_id = data['node_id']
#number_node_id  = len(list_node_id)

number_parent_id  = len(list_parent_link)
number =0
browser = webdriver.Firefox()

for i in range(0,number_parent_id):
    try:
        #browser.get("https://www.amazon.com/b?node={}".format(list_parent_id[i+number]))
        browser.get("{}".format(list_parent_link[i+number]))
        
        #browser.get("https://www.amazon.com/s?k=Artificial+Plants+%26+Flowers&i=garden&rh=n%3A14087351&_encoding=UTF8&c=ts&qid=1627106306&ts_id=14087351&ref=sr_pg_1")
        sleep(5)
        for j in range(0,100):
            sleep(5)
            Titles = []
            product_id = []
            original_product_id_final = []
            page_source = BeautifulSoup(browser.page_source,"html.parser")
            strings_title= page_source.find_all('span',class_="a-size-base-plus a-color-base a-text-normal")
            
            if len(strings_title) == 0 :
                strings_title= page_source.find_all('span',class_="a-size-base a-color-base a-text-normal")
                
            if len(strings_title) == 0 :
                strings_title= page_source.find_all('span',class_="a-size-medium a-color-base a-text-normal")
                
            
                

            
            

            original_product_id = page_source.find_all('a',class_='a-link-normal s-no-outline')
            

        
            for tag1 in strings_title:
                if len(tag1.text.strip()) <= 1:
                    Titles.append(0)
                else:
                    Titles.append(tag1.text.strip())
            
            for tag2 in original_product_id:
                product_id.append(tag2['href'])
            


            for ID in product_id:
                vitri = ID.find('dp/')

                if vitri is not None:
                    original_product_id_final.append(ID[vitri+3:vitri+13])
                else:
                    original_product_id_final.append(0)

            

            print(len(Titles))
            print(len(original_product_id_final))


            len_min = min(len(Titles),len(original_product_id_final))
            Titles = Titles[:len_min]
            original_product_id_final  = original_product_id_final[:len_min]

            try:

                df = pd.DataFrame({ 'Node id'    : '{}'.format(list_bbn[i+number]),
                                    'Product_name': Titles,
                                   'original_product_id' : original_product_id_final,

                                   })
                df.to_csv('{}.csv'.format(name),index=False,mode = 'a')
            except Exception as e:
                print(e)
                pass
            browser.execute_script('window.scrollTo(0,document.body.scrollHeight);')
            try:
                Next_button = browser.find_element_by_xpath('//*[@class="a-last"]')
                #class="s-pagination-item s-pagination-next s-pagination-button s-pagination-separator"
                Next_button.click()
            except Exception as e:
                Next_button1 = browser.find_element_by_xpath('//*[@class="class="s-pagination-item s-pagination-next s-pagination-button s-pagination-separator"]')
                Next_button1.click()
    except SyntaxError as e1:
        continue
    except TypeError as e2:
        continue
    except InvalidSelectorException as e:
        continue
           
