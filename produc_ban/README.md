# Instruction

Version 1:
 - Chia train dev test theo tỉ lệ từng class: split_train_dev_test.ipynb
 - Chạy process image save thành file .pkl để train
 - thực nghiệm trên nhiều model.

Version 2:
 - Chia train dev test theo tỉ lệ từng class: split_train_dev_test.ipynb
 - Chạy process resize ratio: resize_ratio.ipynb
 - training model resnet50